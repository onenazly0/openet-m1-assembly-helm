helm repo remove zpdopeacr
az acr helm repo add -n zpdopeacr

helm upgrade -i chf -n ecs zpdopeacr/m1-ecs-assembly  --version 2.1.4  \
--set-file chf.housekeeping.m1CustomCleanUp.customM1CleanUpScript=m1-chf-custom-cleanup.sh  \
--set-file chf.housekeeping.customSBAHousekeepingCronjob=SBA.Housekeeping.cronjob.custom \
--set-file ocrt.customRsyncClientSshSecret=writeRsyncOut_id_rsa  \
--set-file notif.customRsyncClientSshSecret=writeRsyncOut_id_rsa \
--set-file rsyncd.customRsyncClientSshSecret=id_rsa \
--set-file chf.customChfCdrStageYaml=CHF.ChfCdrStage.yaml \
--set-file chf.customLoggingConfigXml=logback.xml \
--set-file ocrt.customLoggingConfigXml=logback-ocrt.xml \
--set-file ocrt.customOfferCatalogRuntimeSystemConfigYaml=OfferCatalogRuntimeSystemConfig.yaml \
--set-file chf.customAuditCDRConfigXml=auditCDRConfig.yaml \
--set-file chf.customLocationDetectionStageYaml=CHF.LocationDetectionStage.yaml \
--set-file chf.customGetOrCreateSessionStageYaml=CHF.GetOrCreateSessionStage.yaml \
--set-file chf.customChargingManagerYaml=CHF.ChargingManager.yaml \
--set-file chf.customSBASessionManagerYaml=CHF.SBASessionManager.yaml \
--set-file drb.tracingYaml=Tracing.yaml \
--set-file ro.customRsyncClientSshSecret=id_rsa \
--set-file ro.customRsyncConfigJson=rsync-config_ro.json \
--set-file notif.customRsyncConfigJson=rsync-config_notif.json \
--set-file sba-profile-manager.customSBAProfileManagerYaml=CHF.SBAProfileManager.yaml \
-f values-ecs-staging-chf-runtime-2.1.4-zpdopeaks02.yaml


helm upgrade -i provisioning zpdopeacr/m1-provisioning-assembly \
--set-file rsyncd.customRsyncClientSshSecret=id_rsa \
--set-file provisioning.customRsyncClientSshSecret=writeRsyncOut_id_rsa \
--set-file provisioning.customRsyncConfigJson=rsync-config_provisioning.json \
-f values-provisioning-2.1.1-zpdopeaks02.yaml -n provisioning

helm install certmanager-openet-ca zstopeacr/certmanager-ca -n ecs -f values-cert-manager-ca.yaml

helm upgrade -i voltdb-operator zpdopeacr/voltdb -n ecs -f values-volttdb-operator.yaml

helm  upgrade -i  volt-cluster-pm zpdopeacr/voltdb -n ecs -f values-volttdb-pm-zpdopeaks02.yaml --debug

helm upgrade -i volt-cluster-sm-ro zpdopeacr/voltdb -n ecs -f  values-volttdb-rosm-zpdopeaks02.yaml

helm upgrade -i volt-cluster-sm-chf zpdopeacr/voltdb -n ecs -f  values-volttdb-chfsm.yaml

helm upgrade -i consul zpdopeacr/consul -n consul -f values-consul.yaml
