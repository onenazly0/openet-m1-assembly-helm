helm repo remove zstopeacr
az acr helm repo add -n zstopeacr

helm upgrade -i chf -n ecs zstopeacr/m1-ecs-assembly  --version 2.1.4  \
--set-file chf.housekeeping.m1CustomCleanUp.customM1CleanUpScript=m1-chf-custom-cleanup.sh  \
--set-file chf.housekeeping.customSBAHousekeepingCronjob=SBA.Housekeeping.cronjob.custom \
--set-file ocrt.customRsyncClientSshSecret=writeRsyncOut_id_rsa  \
--set-file notif.customRsyncClientSshSecret=writeRsyncOut_id_rsa \
--set-file rsyncd.customRsyncClientSshSecret=id_rsa \
--set-file chf.customChfCdrStageYaml=CHF.ChfCdrStage.yaml \
--set-file chf.customLoggingConfigXml=logback.xml \
--set-file ocrt.customLoggingConfigXml=logback-ocrt.xml \
--set-file ocrt.customOfferCatalogRuntimeSystemConfigYaml=OfferCatalogRuntimeSystemConfig.yaml \
--set-file chf.customAuditCDRConfigXml=auditCDRConfig.yaml \
--set-file chf.customAuditCDRConfigXml=auditCDRConfig.yaml \
--set-file chf.customLocationDetectionStageYaml=CHF.LocationDetectionStage.yaml \
--set-file chf.customChargingManagerYaml=CHF.ChargingManager.yaml \
--set-file chf.customSBASessionManagerYaml=CHF.SBASessionManager.yaml \
--set-file chf.customGetOrCreateSessionStageYaml=CHF.GetOrCreateSessionStage.yaml \
--set-file drb.tracingYaml=Tracing.yaml \
--set-file ro.customRsyncClientSshSecret=id_rsa \
--set-file ro.customRsyncConfigJson=rsync-config_ro.json \
--set-file notif.customRsyncConfigJson=rsync-config_notif.json \
--set-file sba-profile-manager.customSBAProfileManagerYaml=CHF.SBAProfileManager.yaml \
-f values-ecs-staging-chf-runtime-2.1.4-zstopeaks01.yaml


helm upgrade -i provisioning zstopeacr/m1-provisioning-assembly \
--set-file rsyncd.customRsyncClientSshSecret=id_rsa \
--set-file provisioning.customRsyncClientSshSecret=writeRsyncOut_id_rsa \
--set-file provisioning.customRsyncConfigJson=rsync-config_provisioning.json \
-f values-provisioning-2.1.1-zstopeaks01.yaml -n provisioning

helm install certmanager-openet-ca zstopeacr/certmanager-ca -n ecs -f values-cert-manager-ca.yaml

helm install -n provisioning certmanager-openet-ca-prov zstopeacr/certmanager-ca  --set-file ca.crt=./uat2openet.cer --set-file ca.key=./uat2.openet.m1.local.key  --debug
