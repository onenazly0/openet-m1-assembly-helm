#!/bin/sh
DEFAULT_IFS=$IFS
IFS=$'\n'

#CONFIG_FILE=/tmp/config/rsync.json
#TMP_CONFIG_FILE=/tmp/original.json
#HOST_NAME=`cat /tmp/hosts_name`
#if [ ! -f $CONFIG_FILE ]; then
#    echo "No Configuration file present at $CONFIG_FILE, exiting..."
#    exit 1
#fi

function cleanup {

   #CleanUp handling for CHF logs zip file
    find /opt/deploy/SBA/chf/logs -type f \( -iname "*.*.log.gz"   \) -mmin +60 -exec echo "Delete file {}" \; -exec rm {} \;
    

   #CleanUp handling for CHF tcpdump zip file
    find /opt/deploy/SBA/chf/tcpdump -type f \( -iname "*.pcap*.gz"  \) -mmin +10 -exec echo "Delete file {}" \; -exec rm {} \;

   #CleanUp handling for CHF cdrs file
    find /opt/deploy/SBA/rw/output -type f \( -iname "*.rwf"  \) -mmin +10 -exec echo "Delete file {}" \; -exec rm {} \;

}

echo "Running cleanup $(date)"

cleanup

