helm repo remove zstopeacr
az acr helm repo add -n zstopeacr

helm upgrade -i chf -n ecs zstopeacr/m1-ecs-assembly  --version 2.1.4  \
--set-file chf.housekeeping.m1CustomCleanUp.customM1CleanUpScript=m1-chf-custom-cleanup.sh  \
--set-file chf.housekeeping.customSBAHousekeepingCronjob=SBA.Housekeeping.cronjob.custom \
--set-file ocrt.customRsyncClientSshSecret=writeRsyncOut_id_rsa  \
--set-file notif.customRsyncClientSshSecret=writeRsyncOut_id_rsa \
--set-file rsyncd.customRsyncClientSshSecret=id_rsa \
--set-file chf.customChfCdrStageYaml=CHF.ChfCdrStage.yaml \
--set-file chf.customLoggingConfigXml=logback.xml \
--set-file ocrt.customLoggingConfigXml=logback-ocrt.xml \
--set-file ocrt.customOfferCatalogRuntimeSystemConfigYaml=OfferCatalogRuntimeSystemConfig.yaml \
--set-file chf.customAuditCDRConfigXml=auditCDRConfig.yaml \
--set-file chf.customSBASessionManagerYaml=CHF.SBASessionManager.yaml \
--set-file chf.customGetOrCreateSessionStageYaml=CHF.GetOrCreateSessionStage.yaml \
--set-file chf.customLocationDetectionStageYaml=CHF.LocationDetectionStage.yaml \
--set-file chf.customChargingManagerYaml=CHF.ChargingManager.yaml \
--set-file chf.customChargingDPStageYaml=CHF.ChargingDPStage.yaml \
--set-file drb.tracingYaml=Tracing.yaml \
--set-file ro.customRsyncClientSshSecret=id_rsa \
--set-file ro.customRsyncConfigJson=rsync-config_ro.json \
--set-file notif.customRsyncConfigJson=rsync-config_notif.json \
-f values-ecs-staging-chf-runtime-2.1.4-zstopeaks02.yaml

helm upgrade -i provisioning zstopeacr/m1-provisioning-assembly \
--set-file rsyncd.customRsyncClientSshSecret=id_rsa \
--set-file provisioning.customRsyncClientSshSecret=writeRsyncOut_id_rsa \
--set-file provisioning.customRsyncConfigJson=rsync-config_provisioning.json \
-f values-provisioning-2.1.1-zstopeaks02.yaml -n provisioning

helm upgrade -i certmanager-openet-ca-ecs zstopeacr/certmanager-ca -n ecs -f values-cert-manager-ca-zstopeaks02.yaml

helm install -n provisioning certmanager-openet-ca-prov zstopeacr/certmanager-ca  --set-file ca.crt=./uat2openet.cer --set-file ca.key=./uat2.openet.m1.local.key  --debug

helm upgrade -i oc-gui -n oc-gui https://artifactory.openet.com:443/artifactory/helm-release/oc-gui/offercatalog-gui-assembly/offercatalog-gui-assembly-1.11.7.tgz -f values-oc-gui-zstopeaks02.yaml --debug
