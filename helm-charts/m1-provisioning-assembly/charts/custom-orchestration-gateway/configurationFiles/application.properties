camel.springboot.name={{ .Values.camel.service.name }}

camel.springboot.use-breadcrumb=true
camel.springboot.use-mdc-logging=true

server.port={{ .Values.service.containerPort }}
server.address=0.0.0.0
management.address=0.0.0.0
management.port={{ add .Values.service.containerPort 2 }}
endpoints.enabled = true
endpoints.health.enabled = true

# Camel Thread Pool
camel.threadpool.pool-size={{ .Values.camel.threadpool.poolSize }}
camel.threadpool.max-pool-size={{ .Values.camel.threadpool.maxPoolSize }}
camel.threadpool.max-queue-size={{ .Values.camel.threadpool.maxQueueSize }}
camel.threadpool.keep-alive-time={{ .Values.camel.threadpool.keepAliveTime }}

# Server properties
server.tomcat.connection-timeout={{ .Values.server.connectionTimeout }}
server.tomcat.threads.max={{ .Values.server.threads.max }}
server.tomcat.threads.min-spare={{ .Values.server.threads.min }}

# expose actuator endpoint via HTTP
management.endpoints.web.exposure.include=health,loggers

# show verbose health details (/actuator/health) so you can see Camel information also
management.endpoint.health.show-details=always

# enable JMX which allows to also control health check
camel.springboot.jmx-enabled = true

# enable supervised route controller which will startup routes in safe manner
camel.springboot.route-controller-supervise-enabled = true

# attempt up till 10 times to start a route (and exhaust if still failing)
# when a route is exhausted then its taken out as being supervised and
# will not take part of health-check either (UNKNOWN state)
camel.springboot.route-controller-back-off-max-attempts = 10

# enable health check (is automatic enabled if discovered on classpath)
# global flag to enable/disable
camel.health.enabled = true
# context check is default included but we can turn it on|off
camel.health.context-enabled = true
# routes check is default included but we can turn it on|off
camel.health.routes-enabled = true
# registry check is default included but we can turn it on|off
camel.health.registry-enabled = true

# Scan for Camel XML rest files in /rest_xml directory
camel.springboot.xml-rests = file:/opt/openet/config/rest_xml/*.xml

# Scan for Camel XML route files in /route_xml/ directory
camel.springboot.xml-routes = file:/opt/openet/config/route_xml/*.xml

# Logging config path
logging.config=file:/opt/openet/config/logging/logback.xml


# Statistics config
statistics.directory=/opt/deploy/SBA/statistics

# Statistic List <Stat Name>,<Stat Interval in seconds>, <enabled>, <type>
{{ range $index, $statistic := .Values.statistics }}
statistics.statisticsList[{{ $index }}] = {{ $statistic.name }},{{ $statistic.interval }},{{ $statistic.enabled }},{{ $statistic.type -}}
{{ end }}

# Alarms config
alarms.directory=/opt/deploy/SBA/alarms
alarms.writer.enabled=true

camel.component.consul.url = http://${HOST_IP:localhost}:${CONSUL_AGENT_PORT:8500}
camel.component.consul.service-registry.deregister-after = {{ .Values.camel.component.consul.serviceRegistry.deregisterAfter }}
camel.component.consul.service-registry.check-interval = {{ .Values.camel.component.consul.serviceRegistry.checkInterval }}
camel.component.consul.cluster.service.acl-token = ${CONSUL_ACL_TOKEN:}
camel.component.consul.service-registry.acl-token = ${CONSUL_ACL_TOKEN:}
camel.component.consul.service-registry.tag = {{ .Values.camel.component.consul.serviceRegistry.tag }}
camel.component.consul.service-registry.service-host = ${POD_IP}
camel.component.consul.service-registry.service-name = {{ .Values.camel.component.consul.serviceRegistry.serviceName }}
camel.component.consul.service-registry.namespace = ${NAMESPACE:}
camel.component.consul.service-registry.meta = {{ .Values.camel.component.consul.serviceRegistry.meta }}

{{ range $i, $service := .Values.camel.component.consul.discovery }}
camel.component.consul.discovery.{{ $service.name }}.service-name = {{ $service.namespace }}.{{ $service.name -}}
{{ end }}

# Custom property list
{{ range $i, $property := .Values.camel.component.customProperties }}
camel.component.customProperty.{{ $property.name }} = {{ $property.value -}}
{{ end }}

