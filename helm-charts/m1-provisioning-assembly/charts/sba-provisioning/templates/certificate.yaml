{{/*
The secret below generates a random password for the usage of keystore & truststore.
To prevent the regeneration of the password, Helm pre-install hook is used.
It is installed regardless the status of the TLS enablement so that the user is able to turn on/off TLS using Helm upgrade.
*/}}
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: {{ template "sba-provisioning.fullname" . }}-store-password
  annotations:
    "helm.sh/hook": "pre-install"
    "helm.sh/hook-delete-policy": "before-hook-creation"
data:
  password: {{ randAlphaNum 20 | b64enc | quote }}

{{ if eq (include "sba-provisioning.is-tls-enabled" .) "true" }}

---

{{/*
The purpose of this secret is to ensure the secret generated by cert-manager to be removed on uninstall.
*/}}
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: {{ template "sba-provisioning.fullname" . }}-tls-secret
  annotations:
    "helm.sh/hook": "post-delete"
    "helm.sh/hook-delete-policy": "before-hook-creation, hook-succeeded"
data:

---

apiVersion: cert-manager.io/v1alpha3
kind: Certificate
metadata:
  name: {{ template "sba-provisioning.fullname" . }}-certificate
  labels:
    app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
    app.kubernetes.io/instance: {{ .Release.Name | quote }}
    helm.sh/chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
spec:
  secretName: {{ template "sba-provisioning.fullname" . }}-tls-secret
  duration: {{ .Values.tls.certificate.duration }}
  renewBefore: {{ .Values.tls.certificate.renewBefore }}
  commonName: {{ .Release.Name }}.{{ .Release.Namespace }}
  dnsNames:
    # Pod DNS name
    - "*.{{ .Release.Namespace }}.pod"
    {{- if .Values.global.kubernetes.clusterDomain }}
    - "*.{{ .Release.Namespace }}.pod.{{ .Values.global.kubernetes.clusterDomain }}"
    {{- end }}
    {{- if .Values.tls.certificate.dnsNames }}
    # Additionally declared DNS names
      {{- range .Values.tls.certificate.dnsNames }}
    - "{{ . }}"
      {{- end }}
    {{- end }}
    # The service name, for when accessed through service
    - {{ template "sba-provisioning.fullname" . }}.{{ .Release.Namespace }}.svc
    {{- if .Values.global.kubernetes.clusterDomain }}
    - "{{ template "sba-provisioning.fullname" . }}.{{ .Release.Namespace }}.svc.{{ .Values.global.kubernetes.clusterDomain }}"
    {{- end }}
  {{- if .Values.tls.certificate.ipAddresses }}
  ipAddresses:
    # Additionally declared IP addresses
    {{- range .Values.tls.certificate.ipAddresses }}
    - "{{ . }}"
    {{- end }}
  {{- end }}
  subject:
    organizations:
      - Openet
  isCA: false
  privateKey:
    rotationPolicy: Always
  keystores:
    jks:
      create: true
      passwordSecretRef:
        name: {{ template "sba-provisioning.fullname" . }}-store-password
        key: password
  issuerRef:
    name: {{ .Values.tls.certificate.issuer }}
    kind: ClusterIssuer
    group: cert-manager.io

{{end}}

