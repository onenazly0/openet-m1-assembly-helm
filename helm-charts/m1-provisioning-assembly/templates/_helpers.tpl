{{- define "registry_secret" -}}
{{-  $auths :=  printf "%s:%s" .Values.global.imagePullSecret.username .Values.global.imagePullSecret.password -}}
{"auths":{
 {{.Values.global.imagePullSecret.repo | quote}}:{"username":{{.Values.global.imagePullSecret.username | quote}},"password":{{.Values.global.imagePullSecret.password | quote}},"auth": {{ $auths | b64enc | quote }} }}}
{{- end -}}

