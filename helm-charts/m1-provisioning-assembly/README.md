# Provisioning  Assembly Guide  

## Introduction 

The Provisioning Assembly packages provisioning related services.

The Provisioning Assembly will be deployed in its own namespace (e.g. provisioning) and will interact with services in ECS (in e.g. ecs namespace) 

The chart defaults enables the deployment of most services.

Service | Deployed by default  | Description
--- | --- | --- | --- | ---
`SBA Provisioning`     | No          | Rest Provisioning
`Provisioning `        | Yes         | Soap Provisioning
`Gloo`                 | Yes         | Ingress controller
`Rsync`                | No          | Rsyncd



## What can be deployed?

Any single service or combination of services can be deployed

## Preparing values.yaml

The `values.yaml` is used to override chart defaults

Typically the following values may be modified :

*  values determining the services to be deployed
*  values overriding the amount of resources allocated to the services
*  values overriding the docker image registry and registry secrets
*  values overriding nodePorts/ingress configuration
*  values adding ACL tokens for Consul which are required when Consul deployed with ACL ON
*  global values to configure Jaeger tracing

## Sample values files for usecases are :

[values] (https://bitbucket.openet.com/projects/SRPS/repos/provisioning-assembly/browse/values/values.yaml)

## Sample values files for image registry overrides and imagePullSecrets:

This sample values overrides image registry and imagePullSecrets.
It could be merged into the 'main' values.yaml or can be passed as additional values argument in 'helm deploy' command

[values-image-registry.yaml (https://bitbucket.openet.com/projects/SRPS/repos/provisioning-assembly/browse/values/values-image-registry.yaml)

## Sample values files for Consul ACL:

This sample values adds Consul ACL token override for all services 
It could be merged into the 'main' values.yaml or can be passed as additional values argument in 'helm deploy' command

[values-consul-acl.yaml (https://bitbucket.openet.com/projects/SRPS/repos/provisioning-assembly/browse/values/values-consul-acl.yaml)

## General Configuration

Parameter | Description | Default
--- | --- | ---
`global.ecsNamespace` 		| namespace where ECS Applications & Volt are deployed	| `ecs`
`provisioning.context.chf_internf_client.global.namespace` | namespace where ECS is deployed for forge provisioning  | `ecs`
`createRegistrySecret.enabled` | create image registry secret 				| `true`
`createVirtualService.enabled` | create virtual service       				| `true`
`gloo-kubernetes.enabled`      | deploy gloo                  				| `true`
`sba-provisioning.enabled`     | deploy sba-provisioning                    | `true`
`provisioning.enabled`         | deploy provisioning                        | `true`
`rsycd.enabled`    		       | deploy rsync                               | `true`
`global.tracing.type`          | 'NoOp' to disable Jaeger tracing otherwise 'jaeger' 	| `NoOp`


# Deploying the Provisioning Assembly

## Prerequisites:

* Consul is deployed in the cluster
* Cert-Manager is deployed in the cluster (if turning on TLS for Gloo)
* values file(s) is(are) prepared to override default values (e.g. turn on/off deployment of services, overriding docker registry, override resources)
* ECS can be deployed before or after Provisioning

## Note on rsync

If deploying (forge) provisioning then rsyncd is required 

## Deploying the helm chart:

### Get the latest information about charts from the chart repositories.

```
$ helm repo update
```

### Install Provisioning Assembly with SBA-Provisioning

The command deploys the provisioning-assembly chart to a 'provisiong' namespace

The deployment can override the default configuration. In this example the SubscriberLifeCycle related configuration is overridden iwth local versions   

```
helm install provisioning https://software.openet.com/artifactory/partners-helm/provisioning-assembly/provisioning-assembly-X.X.X.tgz \
--values=values.yaml \
--namespace=provisioning \
--set sba-provisioning.enabled=true \
--set provisioning.enabled=false \
--set global.ecsNamespace=ecs \
#--set-file sba-provisioning.customSubscriberLifeCycleManager=SubscriberLifecycleManager.xml \
#--set-file sba-provisioning.customProvisioningSubscriberLifecycle=Provisioning.SubscriberLifecycle.yaml
```

### Install Provisioning Assembly with Provisioning

```
helm install provisioning https://software.openet.com/artifactory/partners-helm/provisioning-assembly/provisioning-assembly-X.X.X.tgz \
--values=values.yaml \
--namespace=provisioning \
--set sba-provisioning.enabled=false \
--set provisioning.enabled=true \
--set global.ecsNamespace=ecs \
--set provisioning.context.chf_internf_client.global.namespace=ecs 
```

### UnInstall Provisioning

```
helm uninstall provisioning --namespace=provisioning
```


