#!/bin/sh

PROTOCOL="http"
if [ "$KIBANA_HTTPS_ENABLED" = "true" ]
then
  PROTOCOL="https"
fi
echo "HTTP Protocol is $PROTOCOL"
health_check_response=$(curl --write-out '%{http_code}' --silent --output /dev/null --insecure -u "$KIBANA_USERNAME":"$KIBANA_PASSWORD" $PROTOCOL://$MONITORING_URL:$KIBANA_PORT/api/status)

echo "health_check_response $health_check_response"

while [ "$health_check_response" = "503" ]
do
    health_check_response=$(curl --write-out '%{http_code}' --silent --output /dev/null --insecure -u "$KIBANA_USERNAME":"$KIBANA_PASSWORD" $PROTOCOL://$MONITORING_URL:$KIBANA_PORT/api/status)
    sleep 5
    echo "Kibana unavailable, retrying"
done

if [ "$health_check_response" = "401" ]
then
  echo "Provided credentials are incorrect"
  exit 1
fi

if [ "$health_check_response" = "200" ]
then
  echo "Importing index patterns"; curl --insecure -X POST -u "$KIBANA_USERNAME":"$KIBANA_PASSWORD" $PROTOCOL://$MONITORING_URL:$KIBANA_PORT/api/saved_objects/_import?overwrite=true -H "kbn-xsrf: true" --form file=@/index-patterns-kibana/index_patterns.ndjson;
  for f in kibana-exports/*.ndjson;
    do echo "Importing ${f} dashboard"; curl --insecure -X POST -u "$KIBANA_USERNAME":"$KIBANA_PASSWORD" $PROTOCOL://$MONITORING_URL:$KIBANA_PORT/api/saved_objects/_import?overwrite=true -H "kbn-xsrf: true" --form file=@/"${f}";
    done;

  exit 0
fi

exit 1
