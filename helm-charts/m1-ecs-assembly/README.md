# ECS Assembly Helm Chart 

## Introduction

The ECS Assembly helm chart packages all the services that might be required when deploying ECS in a 4G or 5G network / use case.

All the services in the helm chart have an 'enabled' value which allows turning on/off deployment of that service

## Prerequisites

Consul is deployed in the cluster.
ADS is deployed in the cluster (version at least 1.0.5)
values.yaml is prepared with overrides including what services should be deployed or not

## Configuration

Parameter | Description | Default
--- | --- | ---
`*.enabled` | enable/disable deployment of services | `true or false`
`dnsDomain` | Cluster Domain name | `svc.cluster.local`
`enableHttps` | If `true`, SSL is turned ON between the services. Currently turning SSL ON between services is not supported | `false`
`voltdb-sm.service.name` | The voltdb-sm service name to match configuration in other services. Always set to `session-manager-db` | `session-manager-db`
`sba-profile-manager.voltdb-pm.service.name` | The voltdb-pm service name to match configuration in other services. Always set to `profile-manager-db` | `profile-manager-db`
`*.nfType` | Used with NRF. For ECS services always set to `CHF` | `CHF`
`global.imagePullSecret.repo` | docker registry images will be pulled from | `software.openet.com:5000`
`global.imagePullSecret.username` | docker registry username | `-`
`global.imagePullSecret.password` | docker registry password | `-`
chf.elasticAppender.elasticHost | URL of ADS | `http://ads-elasticsearch-es-http.ads.svc.cluster.local:9200`
chf.elasticAppender.authEnabled | If `true` Elastic Appender will include HTTP Basic Authentication when sending data to ADS | `true`
chf.elasticAppender.auth.username | Username for ADS HTTP Basic Authentication | `openet`
chf.elasticAppender.auth.password | Password for ADS HTTP Basic Authentication | `Openet01`


### Profile Export Stream Configuration

Values that affect whether or not the Profile database will try to stream data to UMS/ADS elasticsearch cluster

Parameter | Description | Default
--- | --- | ---
`sba-profile-manager.voltdb-pm.cluster.clusterSpec.image.repository` | Choose between images to enable streaming. (voltdb/voltdb-profile-manager or voltdb/voltdb-profile-manager-stream) The default image does not stream. Versions are always the same regardless of the image chosen | `voltdb/voltdb-profile-manager`

See sample values files for 4g and 5g in the 'samples' directory

## Deploying

### Add the Helm repo if you do not already have it:

```
$ helm repo add openet-private https://software.openet.com/artifactory/partners-helm --username <partner-xxx> --password <xxxxx>
```

### Get the latest information about charts from the chart repositories.

```
$ helm repo update
```
replace 'https://artifactory.openet.com/artifactory/helm-release-local' with 'openet-private'


### Deploy ECS

```
Access to ADS is using account username "openet" with password "Openet01". This is set as default value in CHF Helm chart.

helm install ecs https://artifactory.openet.com/artifactory/helm-release-local/ecs-assembly-X.X.X.tgz \
--values=values.yaml \
--namespace=ecs \
--set rsyncd.rsync.source.username=$username \
--set rsyncd.rsync.source.hostname=$ipaddr \
--set-file rsyncd.customRsyncClientSshSecret=$HOME/.ssh/id_rsa \
--set rsyncd.rsync.source.path=$configpath/config-data  \
```


