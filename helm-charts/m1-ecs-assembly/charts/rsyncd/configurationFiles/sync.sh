#!/bin/bash
DEFAULT_IFS=$IFS
IFS=$'\n'

CONFIG_FILE=/tmp/config/rsync.json
TMP_CONFIG_FILE=/tmp/original.json
if [ ! -f $CONFIG_FILE ]; then
    echo "No Configuration file present at $CONFIG_FILE, exiting..."
    exit 1
fi

function updateTmpFile() {
    cp $CONFIG_FILE $TMP_CONFIG_FILE
}
updateTmpFile

function sync() {

    #Parse our json, extracting sourcePath and destinationPath into a single TSV separated line
    for i in $(cat $CONFIG_FILE | jq -r '.[] | [.sourcePath, .destinationPath] | @tsv'); do
        IFS=$DEFAULT_IFS
        #Split our TSV line into 2
        read -ra my_array <<< "$i"

        #Run rsync
        echo "Syncing from ${my_array[0]} to ${my_array[1]}"
        mkdir -p $(dirname ${my_array[1]})
        RSYNC_CMD=`echo rsync -v $RSYNC_COMMAND_ARGS ${my_array[0]} ${my_array[1]}` 
        echo $RSYNC_CMD
        eval $RSYNC_CMD

        if [ $? -ne 0 ]; then
            IFS=$'\n'
            echo "Could not sync ${my_array[0]} to ${my_array[1]}"
            if [ ! -z "$INIT_STEPS_ONLY" ]; then
                exit 2
            else
                return 2
            fi
        else
            IFS=$'\n'
        fi
        echo "Success"
    done

    if [ -z "$INIT_STEPS_ONLY" ]; then
        #invoke on the reload API
        curl -X POST -H "Content-type: application/json" http://localhost:9090/ops/update_config
    fi
    
    # After all the above has worked mark the config file as handled
    updateTmpFile
}

#Running as an init container
if [ ! -z "$INIT_STEPS_ONLY" ]; then
    sync
    exit 0
fi


function watch() {
    #Running as a normal container, so never exit
    WATCH_DIR=/tmp/config
    echo "Watching ${WATCH_DIR} for deletions"

    while true; do
        diff -q $CONFIG_FILE $TMP_CONFIG_FILE 2>&1 > /dev/null
        if [ $? -eq 1 ]; then
            echo "Running sync $(date)"
            sync
        else
            echo "No deletions detected $(date)"
        fi
        sleep 10
    done
}

watch
