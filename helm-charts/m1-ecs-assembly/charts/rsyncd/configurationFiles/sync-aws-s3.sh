#!/bin/bash

function handleError() {
    if [ ! -z "$WAIT_ON_ERROR" ]; then
        while true; do
            aws s3 ls $profileOptionStr ${AWS_S3_SYNC_BUCKET}
            sleep 10
        done
    fi
    exit 1
}

# AWS S3 sync script
aws --version

if [[ {{ .Values.awsSync.aws.accessViaAnnotations }} == true ]]; then
    echo "Using bucket access credentials via Annotations"
    profileOptionStr=""
else
    profile={{ .Values.awsSync.aws.config.iamAccessProfile }}
    
    if [[ "$profile" == "disabled" ]]; then
        profile="default"
    fi
    profileOptionStr=" --profile $profile"

    echo "Using bucket access credentials from profile[$profile]"
fi

aws s3 ls $profileOptionStr ${AWS_S3_SYNC_BUCKET}
if [ $? -ne 0 ]; then
    echo "Failed to list bucket contents."
    handleError
fi

echo "AWS List Success"
echo "AWS_S3_SYNC_BUCKET = [${AWS_S3_SYNC_BUCKET}]"
echo "aws s3 $profileOptionStr cp --recursive s3://${AWS_S3_SYNC_BUCKET} /source_data/"
aws s3 $profileOptionStr cp --recursive --dryrun s3://${AWS_S3_SYNC_BUCKET} /source_data/
aws s3 $profileOptionStr cp --recursive --only-show-errors s3://${AWS_S3_SYNC_BUCKET} /source_data/
awsReturnCode=$?
if [ $awsReturnCode -ne 0 ]; then
    echo "Failed to sync s3 bucket [${AWS_S3_SYNC_BUCKET}]. AWS return code: [$awsReturnCode]"
    handleError
fi


