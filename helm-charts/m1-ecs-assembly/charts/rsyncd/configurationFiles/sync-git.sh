#!/bin/bash
set -e

echo "StrictHostKeyChecking=no" > /root/.ssh/config
git_source_directory="/tmp/git"
mkdir -p $git_source_directory
cd $git_source_directory
git init
git remote add origin ${GIT_URL}
git fetch --depth 1 origin ${GIT_BRANCH}
git checkout FETCH_HEAD
if [ ! -z ${GIT_RELATIVE_PATH} ]; then
  cp -rf ${GIT_RELATIVE_PATH}/* /source_data
else
  cp -rf * /source_data
fi
ls -latr /source_data/



