# Profile Manager
The SBA Profile Manager chart packages the Profile Manager service with Voltdb database.

## Profile Manager OAPI specification
`api.yaml` is an important artifact on a new release, as It contains API definitions for the following: Subscribers & Groups,Subscriptions, 
 Notification Addresses, Purses, Balances & Reservations .

## Configuration

The following table lists the configurable parameters for the Profile Manager service.
The VoltDB cluster & operator configurable parameters can be found in the voltdb subchart README (sba-profile-manager/charts/voltdb/README.md)
There is a choice available of which image to use for voltdb-pm, one of which can stream profile manager documents to UMS/ADS.

### General Configuration

Parameter | Description | Default
--- | --- | ---
`voltdb-pm.enabled` | enable volt subchart | `false`. set to true for Standalone PM deployment
`app-filebeat-config.enabled` | enable file configmap subchart | `false`. set to true for Standalone PM deployment
`createUpstream.enabled` | create gloo upstream | `true`. set to false for Standalone PM deployment
`dnsDomain` | Cluster Domain name | `svc.cluster.local`
`replicaCount` | The number of Profile Manager pod instances | `1`
`nfType` | Used for routing from Ambassador. Configure the same value in Ambassador | `CHF`
`pathPrefix` | Used to add annotation to service for ambassador  | `profile-manager/v1`
`image.registry` | Docker registry | `artifactory.openet.com:5000`
`image.repository` | Docker repository | `/sba-microservice/profile-manager-server`
`image.tag` | Docker image tag | ``
`image.pullPolicy` | Docker image pull policy | `Always`
`installFilebeat` | Install the filebeat sidecar container if `true` | `true`
`restServer.config.verticleInstances` | Number of application threads (each thread listens on new port)  | `4`
`serviceAccountName` | Application pod service account name  | `default`
`securityContext.runAsUser` |  Application pod runAsUser  | `k8s env default`
`securityContext.runAsGroup` |  Application pod runAsGroup  | `k8s env default`
`securityContext.fsGroup` |  Application pod fsGroup  | `k8s env default`
`service.resources.requests.cpu` | Application pod CPU Request  | `2`
`service.resources.requests.memory` | Application pod Memory Request  | `2G`
`service.resources.limits.cpu` | Application pod CPU Limit  | `4`
`service.resources.limits.memory` | Application pod Memory Limit  | `6G`
`global.image.pullSecretOverride` | inform volt cleanup pod what secret to use to pull image | `openet-software `
`global.serviceAccountName ` | inform volt pods what serviceaccount to pull images | `openet-sa`
`global.imagePullSecrets` | inform volt cleanup pod what secret to use to pull image | `openet-software`
`jvm.options` | Optional JVM settings for the SBA microservice container | `-`

### Configuration Files

When the following values are set with a file location the file is used to override the respective functionality with custom configuration

__NOTE__ The ''Changes Require Restart'' column indicates whether changes to the file require the service process to be restarted or whether the changes are dynamically reloaded once updated on the filesystem

Parameter | Description | Default Value | Changes Require Restart
--- | --- | --- | ---
`customSBAProfileManagerYaml` | profile Manager configuration | uses file built into the chart | No
`customStatisticsConfigYaml` | Statistics Config | uses file built into the chart | No
`customAlarmsConfigYaml`: | Alarms Config | uses file built into the chart | Yes
`customLoggingConfigXml`: | Log and Log Lavel Config | uses file built into the chart that can be controlled and extended by value definitions  | No
`customRsyncConfigJson` | Rsync client configuration defining source and destination locations to be transferred | uses file built into the chart | No
`customRsyncClientPostSyncScript` | Script to provide custom operation on the microservice after the rsync operation is complete | uses file built into the chart | No

### Log Levels

Parameter | Description | Default
--- | --- | ---
`logging.logLevel` | Sets the Log Level in the log file. Applied dynamically and changes log output without restart | `INFO`
`logging.extension` | Custom log file settings that are added in-place in the logback file. Yaml format. Default empty | ` `

### Rsync Configuration 

Parameter | Description | Default
--- | --- | ---
`rsync.name` | Name for rsync sidecar container | `rsync-client`
`rsync.image.registry` | Docker registry | `artifactory.openet.com:5000`
`rsync.image.repository` | Docker repository | `rsync/rsync-client`
`rsync.image.tag` | Rsync docker image tag | `1.0.0-SNAPSHOT`
`rsync.image.pullPolicy` | Docker image pull policy | `Always`
`rsync.commandArgs` | Extra rsync command switches used by the client | ` -r --delete --password-file=/tmp/rsync/password `
`rsync.syncCheckPeriodSecs` | How often the client checks if a sync operation should be run, in seconds | `10`
`rsync.configVersion` | To be incremented to provoke a synchronisation operation to transfer latest files  | `0`
`rsync.livenessProbe.healthFile` | The location of the file used in the liveness check | `/mnt/health/liveness`
`rsync.readinessProbe.healthFile` | The location of the file used in the readiness check | `/mnt/health/readiness`
`rsyncd.service.name` | The rsync service name to be used in rsync operations | `rsyncd-svc`
`rsyncd.service.port.number` | The rsync port number to use | `873`
`rsyncd.service.port.type` | The service type used in URL formulation | `rsync`

### Profile Export Stream Configuration

The profile manager volt database can be configured to stream documents to ADS (or an alternative elasticsearch cluster), resulting in a copy of all documents being available there for reporting or reference purposes. Streaming export is a built in feature of voltdb that we are leveraging and have customised to suit our profile manager documents. 

    A pre-requisite for this to work is for provisioning and profile manager docEncoding to be set to JSON, so the documents are in a format that elasticsearch can parse.
    
Values that affect whether or not the Profile database will try to stream data to ADS elasticsearch cluster

Parameter | Description | Default
--- | --- | ---
`voltdb-pm.cluster.clusterSpec.image.repository` | Choose between images to enable streaming. (voltdb/voltdb-profile-manager or voltdb/voltdb-profile-manager-stream) The default image does not stream. Versions are always the same regardless of the image chosen | `voltdb/voltdb-profile-manager`
`voltdb-pm.cluster.config.deployment.export.configurations[0].target` | The streaming target name, this is a hardcoded value and should always be `elasticsearch` | `elasticsearch`
`voltdb-pm.cluster.config.deployment.export.configurations[0].enabled` | A boolean value to enable/disable streaming to elasticsearch. This is a hardcoded value and should always be set to `true` | `true`
`voltdb-pm.cluster.config.deployment.export.configurations[0].type` | Identifies the type of export connector to use. Profile export has its own export connector, which voltdb allows as type `custom`. This is a hardcoded value and should always be set to `custom` | `custom`
`voltdb-pm.cluster.config.deployment.export.configurations[0].exportconnectorclass` | Connected to the previous parameter, this identifies the java class that handles the custom export. This is a hardcoded value and should always be set to `com.openet.exportclient.ProfileExportClient` | `com.openet.exportclient.ProfileExportClient`
`voltdb-pm.cluster.config.deployment.export.configurations[0].properties.endpoint` | This is the connection string for the elasticsearch endpoint that voltdb will stream the profile records to. It is made up of `http<s>://<host>:<port>/%t/document`. The latter part of the endpoint should always be the same, as this identifies the index to create. To connect to ADS the value is as follows `http://ads-elasticsearch-es-http.ads.svc.cluster.local:9200/%t/document` | `http://ads-elasticsearch-es-http.ads.svc.cluster.local:9200/%t/document`
`voltdb-pm.cluster.config.deployment.export.configurations[0].properties.user` | This is the user name for http authorisation where authorisation is turned on. It is turned on in ADS by default, with the user name `elastic` being the hardcoded value. This value should not be present if connecting to a different elasticsearch cluster without http authorisation. | `elastic`
`voltdb-pm.cluster.config.deployment.export.configurations[0].properties.password` | This is the password for http authorisation where authorisation is turned on. It is turned on in ADS by default. ADS will autogenerate a new password each time it starts up, this password must be set prior to the voltdb profile manager database starting up. See below for info on how to get the password. This value should not be present if connecting to a different elasticsearch cluster without http authorisation. As the password changes, the default value is `changeme` | `changeme`
`voltdb-pm.cluster.config.deployment.export.configurations[0].properties.caCertificatePath` | TLS/SSL is not currently turned on for ADS. If it is turned on in the future, the supported method for connecting via TLS/SSL is with a caCert. The caCert used to secure ADS must be put on the file system for voltdb profile manager. This parameter should define an absolute path to the caCert if set. | not set

*Note* to extract the password for ADS:

    adsPassword=$(kubectl -n ads get secret ads-elasticsearch-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo)

### voltClient Configuration 

Parameter | Description | Default
--- | --- | ---
`voltClient.username` | Allow setting of username for VoltClient. If used it must match username of voltdb-pm | ` `
`voltClient.password` | Allow setting of password for VoltClient. If used it must match password of voltdb-pm | ` `
`voltClient.clientCount` | Allow setting of number of volt clients ` `
`voltClient.voltDBTimeout` | Allow setting of number of volt client timeout | ` `

### Overload Configuration

Values that affect the overload operation of the microservice

Parameter | Description | Default
--- | --- | ---
`overloadProtection.restServer.containerCpuUsageMetric.enabled` | Controls enable of overload operatin based on CPU usage level metric | `true`
`overloadProtection.restServer.containerCpuUsageMetric.target` | The CPU usage level metric where overload responses start to be returned, across all CPUs | `320`
`overloadProtection.requestHistogramConfig` | Optional histrogram specific config values. Default empty | ``
`overloadProtection.throttlingConfig` | Optional throttling specific config values. Default empty | ``
`overloadProtection.maximumConcurrentRequestsPerVerticle` | Optional overload restriction based on requests per verticle. Default empty | ``
`overloadProtection.processingPrioritizationEnabled` | Optional configuration for overload prioritization. Default empty | ``

### Service Discovery Configuration

Values that affect the service discovery (consul) operation of the microservice

Parameter | Description | Default
--- | --- | ---
`serviceDiscovery.serviceRegistration.deregisterAfterPeriod` |  Time period in seconds that the service should be deregistered | `true`
`serviceDiscovery.serviceRegistration.services` | Services may be specified | `[]`
`serviceDiscovery.consulClient.hostName` | The consul client hostname to be used | the calcluated host ip

Note: The Voltdb container no longer runs as root user. The image defines its own internal user with uid=1001 and gid=1001.

## Installing the Chart

The Profile Manager chart can be installed as follows :

Prepare a values file (to override docker registry locations or for example to adjust size of volt cluster or adjust CPU/Memory allocated to the PM pod)

Additionally values need to be overridden for Standalone Deployment

(See sample values file in the 'samples' directory of this chart for some examples of overrides for both Profile Manager service and voltdb)


### Add the Helm repo if you do not already have it:

```
$ helm repo add openet-private https://software.openet.com/artifactory/partners-helm --username <partner-xxx> --password <xxxxx>
```

### Get the latest information about charts from the chart repositories.

```
$ helm repo update
```

### Install with RBAC charts:

* create the openet-account
* create the pm namespace with rbac resources and volt CRDs
* switch context to openet-account
* deploy PM with --skip-crds

```
helm upgrade --install openet-rbac-common https://artifactory.openet.com/artifactory/helm-snapshot/openet-kubernetes-rbac/openet-kubernetes-rbac-0.1.0-SNAPSHOT.tgz --set common.enabled=true

helm upgrade --install openet-rbac-pm https://artifactory.openet.com/artifactory/helm-snapshot/openet-kubernetes-rbac/openet-kubernetes-rbac-0.1.0-SNAPSHOT.tgz --set voltdb.enabled=true --set voltdb.namespace=pm --set ecs.enabled=true --set ecs.namespace=pm

TOKEN=$(kubectl -n openet-system describe secrets "$(kubectl -n openet-system describe serviceaccount openet-account | grep -i Tokens | awk '{print $2}')" | grep token: | awk '{print $2}')
kubectl config set-credentials openet-account --token=$TOKEN
kubectl config set-context openet-account --cluster=<CLUSTER> --user=openet-account
kubectl config use-context openet-account

helm install pm openet-private/sba-profile-manager --version=x.x.x --namespace=xxx [--values myvalues.yaml] [--set parameter ..] --values=samples/values-pm-standalone.yaml --skip-crds 
```

### Install without RBAC charts:

```
$ helm install pm openet-private/sba-profile-manager --version=x.x.x --namespace=xxx [--values myvalues.yaml] [--set parameter ..] --values=samples/values-pm-standalone.yaml --values=samples/values-rbac-off.yaml
```


