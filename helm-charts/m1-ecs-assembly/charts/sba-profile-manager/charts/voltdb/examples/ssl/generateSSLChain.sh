#!/usr/bin/env bash
set -xeuo pipefail
cd "$( dirname "$0" )"

# ref: https://docs.voltdb.com/UsingVoltDB/SecuritySSL.php

# Cleanup old keystore and truststore if found
rm -rf myvoltdb.*

# Generate key
keytool -genkey -keystore myvoltdb.keystore -storetype pkcs12 \
          -storepass myvoltdb -keypass myvoltdb -alias mydb \
          -keyalg rsa -validity 3650 -keysize 2048 \
          -dname CN=voltdb.com

# Generate certificate request
keytool -certreq -keystore myvoltdb.keystore \
          -storepass myvoltdb -keypass myvoltdb -alias mydb  \
          -keyalg rsa -file myvoltdb.csr

# Generate certificate
keytool -gencert -keystore myvoltdb.keystore \
          -storepass myvoltdb -keypass myvoltdb -alias mydb  \
          -infile myvoltdb.csr -outfile myvoltdb.cert -validity 3650

# Import certificate to keystore
keytool -import -keystore myvoltdb.keystore  \
          -storepass myvoltdb -keypass myvoltdb -alias mydb  \
          -file myvoltdb.cert

# Import certificate to truststore
keytool -import -keystore myvoltdb.truststore \
          -storepass myvoltdb -keypass myvoltdb -alias mydb  \
          -file myvoltdb.cert -noprompt

# Convert x509 binary to PEM format
openssl x509 -inform der -in myvoltdb.cert -out myvoltdb.pem

# TODO: Switch to secure TLS 1.3 - TLS_CHACHA20_POLY1305_SHA256 or TLS_AES_256_GCM_SHA384
# TODO: Use valid certificiate chain with Root, Intermediate and server chain

# openssl genpkey -algorithm rsa -aes-256-cbc -pass pass:testpass -pkeyopt rsa_keygen_bits:4096 -out RootCA.key

# openssl req -new -x509 -days 1826 \
  # -sha384 \
  # -key RootCA.key \
  # -out RootCA.crt \
  # -passin pass:testpass \
  # -subj "/C=US/ST=Massachusetts/L=Bedford/O=VoltDB Inc./OU=Cloud Native Transformation/CN=voltdb.com/emailAddress=info@voltdb.com"

# openssl genpkey -algorithm rsa -aes-256-cbc -pass pass:testpass -pkeyopt rsa_keygen_bits:4096 -out IntermediateCA.key

# openssl req -new \
  # -sha384 \
  # -key IntermediateCA.key \
  # -out IntermediateCA.csr \
  # -passin pass:testpass \
  # -subj "/C=US/ST=Massachusetts/L=Bedford/O=VoltDB Inc./OU=Cloud Native Transformation/CN=voltdb.com/emailAddress=info@voltdb.com"

# openssl x509 -req -days 1000 \
  # -passin pass:testpass \
  # -in IntermediateCA.csr \
  # -CA RootCA.crt \
  # -CAkey RootCA.key \
  # -CAcreateserial \
  # -out IntermediateCA.crt

# openssl genpkey -algorithm rsa -aes-256-cbc -pass pass:testpass -pkeyopt rsa_keygen_bits:4096 -out Server.key

# openssl req -new \
  # -sha384 \
  # -key Server.key \
  # -out Server.csr \
  # -passin pass:testpass \
  # -subj "/C=US/ST=Massachusetts/L=Bedford/O=VoltDB Inc./OU=IT Department/CN=voltdb.com/emailAddress=info@voltdb.com"

# openssl x509 -req -days 1000 -passin pass:testpass -in Server.csr -CA IntermediateCA.crt -CAkey IntermediateCA.key -set_serial 0101 -out Server.crt -sha384

# keytool -import -trustcacerts -keystore voltdb.keystore \
  # -storepass myvoltdb -noprompt -alias voltdbroot -file RootCA.crt
# keytool -import -trustcacerts -keystore voltdb.keystore \
  # -storepass myvoltdb -noprompt -alias voltdbintermediate -file IntermediateCA.crt
# keytool -import -trustcacerts -keystore voltdb.keystore \
  # -storepass myvoltdb -noprompt -alias voltdbserver -file Server.crt

# keytool -import -trustcacerts -keystore voltdb.truststore \
   # -storepass myvoltdb -noprompt -alias voltdbroot -file RootCA.crt
# keytool -import -trustcacerts -keystore voltdb.truststore \
   # -storepass myvoltdb -noprompt -alias voltdbintermediate -file IntermediateCA.crt
# keytool -import -trustcacerts -keystore voltdb.truststore \
   # -storepass myvoltdb -noprompt -alias voltdbserver -file Server.crt
