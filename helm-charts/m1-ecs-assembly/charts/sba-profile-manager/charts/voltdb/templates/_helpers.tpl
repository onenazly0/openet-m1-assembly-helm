{{/* vim: set filetype=mustache: */}}
{{/* Expand the name of the chart. This is suffixed with -cluster-classes, which means subtract 16 from longest 63 available */}}
{{- define "voltdb-operator.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 47 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "voltdb-operator.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 47 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 47 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 47 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/* Fullname suffixed with operator */}}
{{- define "voltdb-operator.operator.fullname" -}}
{{- printf "%s-operator" (include "voltdb-operator.fullname" .) -}}
{{- end }}

{{/* Fullname suffixed with cluster */}}
{{- define "voltdb-operator.cluster.fullname" -}}
{{- printf "%s-cluster" (include "voltdb-operator.fullname" .) -}}
{{- end }}

{{/* Create chart name and version as used by the chart label. */}}
{{- define "voltdb-operator.chartref" -}}
{{- replace "+" "_" .Chart.Version | printf "%s-%s" .Chart.Name -}}
{{- end }}

{{/* Generate basic labels */}}
{{- define "voltdb-operator.labels" }}
chart: {{ template "voltdb-operator.chartref" . }}
release: {{ $.Release.Name | quote }}
heritage: {{ $.Release.Service | quote }}
{{- if .Values.commonLabels}}
{{ toYaml .Values.commonLabels }}
{{- end }}
{{- end }}

{{/* Create the name of voltdb-operator service account to use */}}
{{- define "voltdb-operator.operator.serviceAccountName" -}}
{{- if .Values.operator.serviceAccount.create -}}
    {{ default (include "voltdb-operator.operator.fullname" .) .Values.operator.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.operator.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/* Create the name of cluster service account to use */}}
{{- define "voltdb-operator.cluster.serviceAccountName" -}}
{{- if .Values.cluster.serviceAccount.create -}}
    {{ default (include "voltdb-operator.cluster.fullname" .) .Values.cluster.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.cluster.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/* VoltDB mountPath */}}
{{- define "voltdb-operator.cluster.mountPath" -}}
{{- if .Values.cluster.clusterSpec.persistentVolume.hostPath.enabled -}}
{{- if .Values.cluster.clusterSpec.persistentVolume.hostPath.path -}}
{{- .Values.cluster.clusterSpec.persistentVolume.hostPath.path -}}
{{ else -}}
{{- printf "/data/voltdb"}}
{{- end -}}
{{- else -}}
{{- printf "/pvc/voltdb"}}
{{- end -}}
{{- end -}}
