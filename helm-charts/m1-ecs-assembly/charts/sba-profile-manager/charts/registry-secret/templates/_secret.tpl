{{/*
  Creates the secret only if the username is provided
*/}}
{{- define "registry-secret.dockerSecret" -}}
{{- if .username }}
apiVersion: v1
kind: Secret
metadata:
  name: {{ required "The secret name is required" .secretName }}
  annotations:
    "helm.sh/hook": pre-install,pre-upgrade
    "helm.sh/hook-weight": "0"
    "helm.sh/hook-delete-policy": before-hook-creation
  labels:
    app.kubernetes.io/managed-by: {{ .root.Release.Service | quote }}
    app.kubernetes.io/instance: {{ .root.Release.Name | quote }}
    helm.sh/chart: "{{ .root.Chart.Name }}-{{ .root.Chart.Version }}"
type: kubernetes.io/dockerconfigjson
data:
  .dockerconfigjson: {{ include "registry-secret.dockerConfigJson" . | b64enc }}
---
{{- end -}}
{{- end -}}
