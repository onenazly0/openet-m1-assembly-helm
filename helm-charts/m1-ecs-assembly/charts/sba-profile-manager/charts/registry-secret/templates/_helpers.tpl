{{/*
  Renders the docker config in JSON for the registry authentication.
*/}}
{{- define "registry-secret.dockerConfigJson" -}}
{{-  $auths :=  printf "%s:%s" .username .password -}}
{
  "auths":{
    {{ required "The registry URL is required" .repo | quote }}: {
      "username":{{ .username | quote }},
      "password":{{ .password | quote }},
      "auth": {{ $auths | b64enc | quote }}
    }
  }
}
{{- end -}}

{{/*
  Counts the number registry secret(s) to be created.
*/}}
{{- define "registry-secret.activeSecretsCount" -}}
{{- $activeSecret := 0 }}
{{- range $key, $value := .credentials }}
  {{- if $value.username }}
    {{- $activeSecret = add1 $activeSecret }}
  {{- end }}
{{- end -}}
{{ $activeSecret }}
{{- end -}}

{{/*
  Renders the list of imagePullSecrets from global scope.
*/}}
{{- define "registry-secret.imagePullSecrets" -}}
{{- if .global.imagePullSecrets -}}
imagePullSecrets: {{- toYaml .global.imagePullSecrets | nindent 2 }}
{{- else }}
{{- fail "global.imagePullSecrets should not be empty" }}
{{- end }}
{{- end -}}

{{/*
  Renders the list of created registry secrets.
*/}}
{{- define "registry-secret.printCreatedSecrets" -}}
{{- range $key, $value := .credentials }}
  {{- if $value.username }}
  - {{ $value.secretName }}
  {{- end }}
{{- end -}}
{{- end -}}