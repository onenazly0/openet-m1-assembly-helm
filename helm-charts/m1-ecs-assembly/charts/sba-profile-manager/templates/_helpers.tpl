{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "sba-profile-manager.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "sba-profile-manager.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}


{{- define "registry_secret" -}}
{{-  $auths :=  printf "%s:%s" .Values.global.imagePullSecret.username .Values.global.imagePullSecret.password -}}
{"auths":{
 {{.Values.global.imagePullSecret.repo | quote}}:{"username":{{.Values.global.imagePullSecret.username | quote}},"password":{{.Values.global.imagePullSecret.password | quote}},"auth": {{ $auths | b64enc | quote }} }}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sba-profile-manager.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Get the service protocol (HTTP/HTTPS)
*/}}
{{- define "sba-profile-manager.service-protocol" -}}
{{- if eq (include "sba-profile-manager.is-tls-enabled" .) "true" }}
{{- printf "https" }}
{{- else }}
{{- printf "http" }}
{{- end -}}
{{- end -}}
 
{{/*
Get the pod's target port name
*/}}
{{- define "sba-profile-manager.sba-ms-port-name" -}}
sba-ms-{{ template "sba-profile-manager.service-protocol" . }}
{{- end -}}
 
{{/*
Check if TLS is enabled by checking both global and microservice Helm values. If microservice is configured, it takes precedence.
*/}}
{{- define "sba-profile-manager.is-tls-enabled" -}}
{{ ternary .Values.tls.enabled .Values.global.tls.enabled (hasKey .Values.tls "enabled") }}
{{- end -}}

{{/* append 'dnsPostfix' to rsyncd services when defined */}}
{{- define "appendDnsPostfix" -}}
{{- if $.Values.rsyncd.dnsPostfix }}.{{ $.Values.rsyncd.dnsPostfix }}{{ end -}}
{{- end -}}

