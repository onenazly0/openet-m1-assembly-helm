apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "chf.name" . }}
  labels:
    app: {{ template "chf.name" . }}
    chart: {{ template "chf.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ template "chf.name" . }}
      release: {{ .Release.Name }}
  template:
    metadata:
      labels:
{{- if and .Values.nfServiceType .Values.global.nfType }}
        nf-type: {{ .Values.global.nfType }}
        nf-instance-id: {{ required "\n\n\n\nValues.global.nfInstanceId is mandatory and should contain UUIDv4!!!\n\n\n\n" .Values.global.nfInstanceId }}
{{- end }}
        app: {{ template "chf.name" . }}
        release: {{ .Release.Name }}
      annotations:
{{- if and .Values.nfServiceType .Values.global.nfType }}
        nf-service-type: {{ .Values.nfServiceType }}
        service-component-name: {{ .Chart.Name }}
{{- with .Values.serviceInfo }}
        nf-service-template: |
{{ toYaml . | indent 10 }}
{{- end }}
{{- end }}
        rollme: "{{ .Values.restartVersion }}" 
    spec:
{{- if or .Values.serviceAccount.name .Values.global.serviceAccountName }}
      serviceAccountName: {{ coalesce .Values.serviceAccount.name .Values.global.serviceAccountName }}
{{- end }}
      securityContext:
        {{- toYaml .Values.securityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.runtimeContainer.image.registry }}/{{ .Values.runtimeContainer.image.repository }}:{{ .Values.runtimeContainer.image.tag }}"
          workingDir: /opt/deploy/SBA
          args: ["/bin/bash", "-x", "-c", "sed -i 's#sudo tcpdump -w $TCPDUMP_DIAG_DIR/sba.pcap -i any $TCPDUMP_FILTER#sudo tcpdump -w $TCPDUMP_DEST_DIR/$(date +%Y%m%d_%H%M%S).pcap -i any $TCPDUMP_FILTER -C 200 -z gzip #g' bin/StartService.sh ; bin/StartService.sh"]
          imagePullPolicy: {{ .Values.runtimeContainer.image.pullPolicy }}
          env:
          - name: TCPDUMP_CAPTURE
            value: "{{ .Values.tcpdumpCapture }}"
          - name: TCPDUMP_FILTER
            value: "{{ .Values.tcpdumpFilter }}"
          - name: TCPDUMP_DEST_DIR
            value: "{{ .Values.tcpdumpDestDir }}"
          - name: HOST_IP
            valueFrom:
              fieldRef:
                fieldPath: status.hostIP
          - name: POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          - name: POD_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          {{- if eq (include "chf.is-tls-enabled" .) "true" }}
          - name: SERVER_TLS_KEYSTORE_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ template "chf.fullname" . }}-store-password
                key: password
          {{- end }}
          {{- if or .Values.global.tls.truststore.file .Values.global.tls.truststore.secretName }}
          - name: SERVER_TLS_TRUSTSTORE_PASSWORD
            valueFrom:
              secretKeyRef:
                {{- if .Values.global.tls.truststore.file }}
                name: {{ template "chf.name" . }}-truststore
                {{- else }}
                name: {{ .Values.global.tls.truststore.secretName }}
                {{- end }}
                key: password
          {{- end }}
{{if eq .Values.elasticAppender.authEnabled "true"}}
          - name: ES_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ template "chf.name" . }}-elastic-auth
                key: elasticPass
{{ end }}                
          envFrom:
          - configMapRef:
              name: {{ template "chf.name" . }}
          volumeMounts:
            # Mounts '/opt' contents (deploy/SBA folder)
            - name: sba-init-container-volume
              mountPath: /opt

            - name: logback-config-volume
              mountPath: /opt/deploy/SBA/config/logging
              
            - name: statistics-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/stats

            - name: alarm-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/alarms

            - name: sessionmanager-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/chf/sessionManager
              
            - name: stages-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/chf/stages

            - name: chargingmanager-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/chf/chargingManager

            - name: timerclient-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/chf/timerClient/
              
            - name: recordwriter-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/chf/recordWriter

            - name: restserver-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/restServer

            - name: callbackclient-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/callback
              
            # OverloadProtectionConfig
            - name: overload-protection-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/overloadProtection

            - name: servicediscoveryconsulclient-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/serviceDiscoveryConsulClient

            - name: tracing-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/tracing

            {{- /* M1-Custom: m1CustomCleanUp */}}
            {{if eq .Values.housekeeping.m1CustomCleanUp.enabled true}}
            - name: m1-custom-cleanup-volume-log
              mountPath: "/opt/deploy/SBA/chf/logs"
           {{ if .Values.tcpdumpDestDir }}
            - name: tcpdump-volume
              mountPath: {{ .Values.tcpdumpDestDir }}
           {{ end }}
            {{ end }}
            {{- /* M1-Custom: End */}}

{{ if .Values.voltClient.useSSL }}
            - mountPath: /etc/volt/ssl
              name: volt-client-secret
{{ end }}

            # SSL
{{- if or .Values.global.tls.truststore.file .Values.global.tls.truststore.secretName }}
            - name: tls-external-truststore
              mountPath: /opt/deploy/SBA/truststore
{{- end }}
{{ if eq (include "chf.is-tls-enabled" .) "true" }}
            - mountPath: /opt/deploy/SBA/certificates
              name: tls-secret
            - mountPath: /opt/deploy/SBA/config/java-options/trust-store-options
              subPath: trust-store-options
              name: trust-store-options-volume
{{ end }}
{{ if .Values.global.pmNamespace }}
            - name: pm-clients-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/pmClient
{{ end }}
            - name: kafka-keytab-config-volume
              mountPath: /opt/deploy/SBA/config/deployment-config/chf/kafka-keytab

{{ range $kafkaSecretsMounts := .Values.kafkaSecretsMounts }}
            - mountPath: {{ $kafkaSecretsMounts.mountPath }}
              name: {{ $kafkaSecretsMounts.name }}
{{ end }}
{{ range $key, $value := .Values.volumeMounts }}
            - mountPath: {{ $value }}
              name: {{ $key }}
{{ end }}

{{if eq .Values.installRsync "true"}}
            - name: rsync-data-volume
              mountPath: /opt/deploy/SBA/config/product-config

{{ if eq (include "chf.IsSubscriberTracingEnabled" .) "true" }}
            - name: rsync-reference-data-subscriber-tracing-data-volume
              mountPath: "/opt/deploy/SBA/refData/subscriberTracing"
{{ end }}
{{ end }}

{{- range $key, $value := .Values.jvm.options }}
            - mountPath: /opt/deploy/SBA/config/java-options/{{ $key }}
              subPath: {{ $key }}
              name: {{ $key }}-volume
{{ end }}

          ports:
            - name: {{ template "chf.sba-ms-port-name" . }}
              containerPort: {{ .Values.service.port }}
              protocol: TCP
          livenessProbe:
            httpGet:
              httpHeaders:
              - name: 3gpp-sbi-message-priority
                value: "0"
              path: "{{ .Values.service.livenessProbe.path }}"
              port: {{ .Values.service.port }}
              scheme: {{ include "chf.service-protocol" . | upper }}
            initialDelaySeconds: {{ .Values.service.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.service.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.service.livenessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.service.livenessProbe.successThreshold }}
            failureThreshold: {{ .Values.service.livenessProbe.failureThreshold }}
          readinessProbe:
            httpGet:
              httpHeaders:
              - name: 3gpp-sbi-message-priority
                value: "0"
              path: "{{ .Values.service.readinessProbe.path }}"
              port: {{ .Values.service.port }}
              scheme: {{ include "chf.service-protocol" . | upper }}
            initialDelaySeconds: {{ .Values.service.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.service.readinessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.service.readinessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.service.readinessProbe.successThreshold }}
            failureThreshold: {{ .Values.service.readinessProbe.failureThreshold }}
          resources:
{{ toYaml .Values.service.resources | indent 12 }}


{{if eq .Values.filebeat.enabled true}}
        - name: {{ .Values.filebeat.name }}
          image: "{{ .Values.filebeat.image.registry }}{{ .Values.filebeat.image.repository }}:{{ .Values.filebeat.image.tag }}"
          imagePullPolicy: {{ .Values.filebeat.image.pullPolicy }}
          envFrom:
          - configMapRef:
              name: {{ template "chf.name" . }}-filebeat
          env:
          - name: NODE_IP
            valueFrom:
              fieldRef:
                fieldPath: status.hostIP
          - name: NODE_NAME
            valueFrom:
              fieldRef:
                fieldPath: spec.nodeName
          - name: POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          - name: POD_NAME
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          - name: K8_LABEL_RELEASE
            valueFrom:
              fieldRef:
                fieldPath: metadata.labels['release']
          - name: K8_LABEL_APP
            valueFrom:
              fieldRef:
                fieldPath: metadata.labels['app']
{{- if .Values.filebeat.extraEnvs }}
{{ toYaml .Values.filebeat.extraEnvs | indent 10 }}
{{- end }}
          volumeMounts:

            - name: filebeat-app-config
              mountPath: /usr/share/filebeat/filebeat.yml
              subPath: filebeat.yml

{{ range $key, $value := .Values.volumeMounts }}
            - mountPath: {{ $value }}
              name: {{ $key }}
              readOnly: true
{{ end }}
          resources:
{{ toYaml .Values.filebeat.resources | indent 12 }}

{{end}}

{{if eq .Values.grokExporter.enabled true}}
        - name: {{ .Values.grokExporter.name }}
          image: "{{ .Values.grokExporter.image.registry }}/{{ .Values.grokExporter.image.repository }}:{{ .Values.grokExporter.image.tag }}"
          imagePullPolicy: {{ .Values.grokExporter.image.pullPolicy }}
          args: ["/configuration/openet_stats.yaml"]
          ports:
          - name: {{ .Values.grokExporter.name }}
            containerPort: {{ .Values.grokExporter.port }}
            protocol: TCP
          volumeMounts:
            - name: {{ template "chf.name" . }}-grok-exporter-config-volume
              mountPath: /configuration
            - name: fw-stats
              mountPath: /statistics
          resources:
{{ toYaml (.Values.grokExporter.resources) | indent 12 }}
{{ end }}

{{if eq .Values.housekeeping.enabled true}}
        - name: {{ .Values.housekeeping.name }}
          image: "{{ .Values.housekeeping.image.registry }}/{{ .Values.housekeeping.image.repository }}:{{ .Values.housekeeping.image.tag }}"
          imagePullPolicy: {{ .Values.housekeeping.image.pullPolicy }}
          volumeMounts:
            - name: housekeeping-logrotate-config-volume
              mountPath: /home/openet/logrotate/configFiles/
            - name: housekeeping-cronjob-config-volume
              mountPath: /home/openet/logrotate/cronjobs/cronjob
              subPath: cronjob
{{ range $key, $value := .Values.housekeeping.volumeMounts }}
            - mountPath: {{ $value }}
              name: {{ $key }}
              readOnly: false
{{ end }}

            {{- /* M1-Custom: m1CustomCleanUp */}}
       {{if eq .Values.housekeeping.m1CustomCleanUp.enabled true}}
            - name: m1-custom-cleanup-volume-log
              mountPath: "/opt/deploy/SBA/chf/logs"

           {{ if .Values.tcpdumpDestDir }}
            - name: tcpdump-volume
              mountPath: {{ .Values.tcpdumpDestDir }}
           {{ end }}

            - name: {{ template "chf.name" . }}-housekeeping-customscript-volume
              mountPath: /home/openet/logrotate/scripts/cleanup-script.sh
              subPath: cleanup.sh
              readOnly: true
          
          {{- /* M1-Custom: Apply only in CHF*/}}
          {{ range $key, $value := .Values.volumeMounts }}
           {{if or (eq $key "fw-cdr") (eq $key "fw-cdr-out")}}
            - mountPath: {{ $value }}
              name: {{ $key }}
           {{ end }}
          {{ end }}
          {{- /* M1-Custom: End apply only in CHF*/}}

      {{ end }}
            {{- /* M1-Custom: End*/}}
          resources:
{{ toYaml .Values.housekeeping.resources | indent 12 }}
{{ end }}

{{if eq .Values.installRsync "true"}}

        - name: {{ .Values.rsync.name }}
          image: {{ .Values.rsync.image.registry }}/{{ .Values.rsync.image.repository }}:{{ .Values.rsync.image.tag }}
          imagePullPolicy: {{ .Values.rsync.image.pullPolicy }}
          env:
          - name: RSYNC_COMMAND_ARGS
            value: {{ .Values.rsync.commandArgs }}  
          - name: RSYNC_CLIENT_LIVENESS_FILE
            value: {{ .Values.rsync.livenessProbe.healthFile }}  
          - name: RSYNC_CLIENT_READINESS_FILE
            value: {{ .Values.rsync.readinessProbe.healthFile }}  
          - name: RSYNC_CLIENT_SYNC_CHECK_PERIOD_SECS
            value: {{ .Values.rsync.syncCheckPeriodSecs | quote }}
          - name: POST_SYNC_SCRIPT
            value: "/usr/bin/post-sync.sh"
          - name: RSYNC_USERNAME
            valueFrom:
              secretKeyRef:
                name: rsyncd-credentials
                key: username
          livenessProbe:
            exec:
              command:                                       
              - cat
              - {{ .Values.rsync.livenessProbe.healthFile }}
            initialDelaySeconds: {{ .Values.rsync.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.rsync.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.rsync.livenessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.rsync.livenessProbe.successThreshold }}
            failureThreshold: {{ .Values.rsync.livenessProbe.failureThreshold }}
          readinessProbe:
            exec:
              command:
              - cat
              - {{ .Values.rsync.readinessProbe.healthFile }}
            initialDelaySeconds: {{ .Values.rsync.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.rsync.readinessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.rsync.readinessProbe.timeoutSeconds }}
            successThreshold: {{ .Values.rsync.readinessProbe.successThreshold }}
            failureThreshold: {{ .Values.rsync.readinessProbe.failureThreshold }}
          resources:
{{ toYaml .Values.rsync.resources | indent 12 }}
          volumeMounts:
            - name: rsync-config-volume
              mountPath: /tmp/config
            - name: rsyncd-credentials
              mountPath: "/tmp/rsync"
              readOnly: true
            - name: rsync-data-volume
              mountPath: /opt/deploy/SBA/config/product-config
{{ if eq (include "chf.IsSubscriberTracingEnabled" .) "true" }}
            - name: rsync-reference-data-subscriber-tracing-data-volume
              mountPath: "/opt/deploy/SBA/refData/subscriberTracing"
{{ end }}
            - name: rsync-health-volume
              mountPath: "/mnt/health"
            - name: post-sync-script-volume
              mountPath: "/usr/bin/post-sync.sh"
              readOnly: true
              subPath: post-sync.sh
{{end}}
      initContainers:
        - name: sba-init-container
          image: {{ .Values.initContainer.image.registry }}/{{ .Values.initContainer.image.repository }}:{{ .Values.initContainer.image.tag }}
          # use copier to copy '/opt' contents (deploy folder) into the sba-shared-volume
          command: [ '/copier', "/opt", '/sba-shared-volume' ]
          imagePullPolicy: {{ .Values.initContainer.image.pullPolicy }}
          volumeMounts:
            - name: sba-init-container-volume
              mountPath: /sba-shared-volume
{{if eq .Values.installRsync "true"}}
        - name: rsync-init
          image: {{ .Values.rsync.image.registry }}/{{ .Values.rsync.image.repository }}:{{ .Values.rsync.image.tag }}
          imagePullPolicy: {{ .Values.rsync.image.pullPolicy }}
          env:
            - name: INIT_STEPS_ONLY
              value: "true"
            - name: RSYNC_COMMAND_ARGS
              value: {{ .Values.rsync.commandArgs }}
            - name: RSYNC_USERNAME
              valueFrom:
                secretKeyRef:
                  name: rsyncd-credentials
                  key: username
          volumeMounts:
            - name: rsync-config-volume
              mountPath: /tmp/config
            - name: rsync-sync-data-volume
              mountPath: "/source_data"
            - name: rsyncd-credentials
              mountPath: "/tmp/rsync"
              readOnly: true
            - name: rsync-data-volume
              mountPath: /opt/deploy/SBA/config/product-config
{{ if eq (include "chf.IsSubscriberTracingEnabled" .) "true" }}
            - name: rsync-reference-data-subscriber-tracing-data-volume
              mountPath: "/opt/deploy/SBA/refData/subscriberTracing"
{{ end }}
          resources:
{{ toYaml .Values.rsync.resources | indent 12 }}
{{end}}

      volumes:
        - name: sba-init-container-volume
          emptyDir: {}

        - name: rsync-sync-data-volume
          emptyDir: {}

{{if eq .Values.filebeat.enabled true}}
        - name: filebeat-app-config
          configMap:
            name: {{ .Values.filebeat.appFilebeatConfigMap }}
{{ end }}
        - name: server-config-volume
          configMap:
            name: {{ template "chf.name" . }}-server
{{if eq .Values.grokExporter.enabled true}}
        - name: {{ template "chf.name" . }}-grok-exporter-config-volume
          configMap:
            name: {{ template "chf.name" . }}-grok-exporter
{{end}}
{{ if eq .Values.housekeeping.enabled true }}
        - name: housekeeping-logrotate-config-volume
          configMap:
            name: {{ template "chf.name" . }}-housekeeping-logrotate
        - name: housekeeping-cronjob-config-volume
          configMap:
{{- if and .Values.housekeeping (not .Values.global.housekeeping) }}
            name: {{ template "chf.name" . }}-housekeeping-cronjob
{{- else }}
            housekeeping-global-cronjob
{{- end }}
{{ range $key, $value := .Values.housekeeping.volumeMounts }}
{{- if not (hasKey $.Values.volumeMounts $key) }}
        - name: {{ $key }}
          emptyDir: {}
{{- end }}
{{ end }}
{{ end }}

       {{- /* M1-Custom: m1CustomCleanUp */}}
       {{if eq .Values.housekeeping.m1CustomCleanUp.enabled true}}
        - name: m1-custom-cleanup-volume-log
          emptyDir: {}
        {{ if .Values.tcpdumpDestDir }}
        - name: tcpdump-volume
          emptyDir: {}
        {{ end }}
      
        - name: {{ template "chf.name" . }}-housekeeping-customscript-volume
          configMap:
            defaultMode: 0755
            name: {{ template "chf.name" . }}-custom-cleanup-script
       {{ end }}
       {{- /* M1-Custom: End */}}

        - name: logback-config-volume
          configMap:
            name: {{ template "chf.name" . }}-logback
            
        - name: sessionmanager-config-volume
          configMap:
            name: {{ template "chf.name" . }}-sessionmanager

        - name: stages-config-volume
          configMap:
            name: {{ template "chf.name" . }}-stages

        - name: statistics-config-volume
          configMap:
            name: {{ template "chf.name" . }}-statistics

        - name: alarm-config-volume
          configMap:
            name: {{ template "chf.name" . }}-alarms

        - name: chargingmanager-config-volume
          configMap:
            name:  {{ template "chf.name" . }}-chargingmanager

        - name: timerclient-config-volume
          configMap:
            name: {{ template "chf.name" . }}-timerclient
            
        - name: recordwriter-config-volume
          configMap:
            name: {{ template "chf.name" . }}-recordwriter
            
        - name: restserver-config-volume
          configMap:
            name: {{ template "chf.name" . }}-restserver

        - name: callbackclient-config-volume
          configMap:
            name: {{ template "chf.name" . }}-callbackclient

        - name: overload-protection-config-volume
          configMap:
            name: {{ template "chf.name" . }}-overload-protection

        - name: kafka-keytab-config-volume
          configMap:
            name: {{ template "chf.name" . }}-kafka-keytab

        - name: servicediscoveryconsulclient-config-volume
          configMap:
            name: {{ template "chf.name" . }}-servicediscoveryconsulclient

{{ if .Values.voltClient.useSSL }}
        - name: volt-client-secret
          secret:
            secretName: volt-certs-certificate-secret
{{ end }}

{{- if or .Values.global.tls.truststore.file .Values.global.tls.truststore.secretName }}
        - name: tls-external-truststore
          secret:
            {{- if .Values.global.tls.truststore.file }}
            secretName: {{ template "chf.name" . }}-truststore
            {{- else }}
            secretName: {{ .Values.global.tls.truststore.secretName }}
            {{- end }}
{{- end }}
{{ if eq (include "chf.is-tls-enabled" .) "true" }}
        - name: tls-secret
          secret:
            secretName: {{ template "chf.fullname" . }}-tls-secret
        - name: trust-store-options-volume
          configMap:
            name: chf-trust-store-options
{{ end }}
        - name: tracing-config-volume
          configMap:
{{- if or .Values.tracing (not .Values.global.tracing.configmap) }}
            name: {{ template "chf.name" . }}-tracing
{{- else }}
            name: {{ .Values.global.tracing.configmap }}
{{- end }}

{{ range $kafkaSecrets := .Values.kafkaSecrets }}
        - name: {{ $kafkaSecrets.name }}
          secret:
            secretName: {{ $kafkaSecrets.secret.secretName }}
{{ end }}
{{- range $key, $value := .Values.jvm.options }}
        - name: {{ $key }}-volume
          configMap:
            name: {{ template "chf.name" $ }}-{{ $key }}
{{ end }}

{{if .Values.global.pmNamespace }}
        - name: pm-clients-volume
          configMap:
            name: {{ template "chf.name" . }}-pm-clients
{{ end }}

{{if eq .Values.installRsync "true"}}
        - name: rsync-config-volume
          configMap:
            name: {{ template "chf.name" . }}-rsync-configuration 
        - name: rsyncd-credentials
          secret:
            secretName: rsyncd-credentials
            defaultMode: 0660
        - name: rsync-data-volume
          emptyDir: {}
{{ if eq (include "chf.IsSubscriberTracingEnabled" .) "true" }}
        - name: rsync-reference-data-subscriber-tracing-data-volume
          emptyDir: {}
{{ end }}
        - name: rsync-health-volume
          emptyDir: {}
        - name: post-sync-script-volume
          configMap:
            defaultMode: 0755
            name: {{ template "chf.name" . }}-post-sync-script

{{end}}
{{ range $key, $value := .Values.volumeMounts }}
        - name: {{ $key }}
          emptyDir: {}
{{ end }}
    {{- with .Values.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
{{- range $key, $value := .Values.affinity }}
    {{- with . }}
{{ toYaml $key | trim | indent 8 }}:
{{ toYaml $value | trim | indent 10 }}
    {{- end }}
{{- end }}
{{- if kindIs "invalid" .Values.affinity.podAntiAffinity }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                    - {{ template "chf.name" . }}
              topologyKey: kubernetes.io/hostname
{{- end}}
    {{- with .Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
