{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "chf.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- define "container.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "chf.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chf.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- define "container.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Get the service protocol (HTTP/HTTPS)
*/}}
{{- define "chf.service-protocol" -}}
{{- if eq (include "chf.is-tls-enabled" .) "true" }}
{{- printf "https" }}
{{- else }}
{{- printf "http" }}
{{- end -}}
{{- end -}}

{{/*
Get the pod's target port name
*/}}
{{- define "chf.sba-ms-port-name" -}}
sba-ms-{{ template "chf.service-protocol" . }}
{{- end -}}

{{/*
Check if TLS is enabled by checking both global and microservice Helm values. If microservice is configured, it takes precedence.
*/}}
{{- define "chf.is-tls-enabled" -}}
{{ ternary .Values.tls.enabled .Values.global.tls.enabled (hasKey .Values.tls "enabled") }}
{{- end -}}

{{/* append 'dnsPostfix' to rsyncd services when defined */}}
{{- define "appendDnsPostfix" -}}
{{- if $.Values.rsyncd.dnsPostfix }}.{{ $.Values.rsyncd.dnsPostfix }}{{ end -}}
{{- end -}}

{{/*
Checks if subscriber tracing enabled either on global or service level.
*/}}
{{- define "chf.IsSubscriberTracingEnabled" -}}
{{- if .Values.tracing -}}
    {{- if .Values.tracing.config -}}
        {{- if .Values.tracing.config.subscriberTracing -}}
            {{ .Values.tracing.config.subscriberTracing -}}
        {{- else -}}
            {{- printf "false" }}
        {{- end -}}
    {{- else -}}
            {{- printf "false" }}
    {{- end -}}
{{- else if .Values.global.tracing -}}
    {{- if .Values.global.tracing.config -}}
        {{- if .Values.global.tracing.config.subscriberTracing -}}
            {{ .Values.global.tracing.config.subscriberTracing -}}
        {{- else -}}
            {{- printf "false" -}}
        {{- end -}}
    {{- else -}}
        {{- printf "false" -}}
    {{- end -}}
{{- else -}}
    {{- printf "false" -}}
{{- end -}}
{{- end -}}
