#!/bin/sh
DEFAULT_IFS=$IFS
IFS=$'\n'

#CONFIG_FILE=/tmp/config/rsync.json
#TMP_CONFIG_FILE=/tmp/original.json
#HOST_NAME=`cat /tmp/hosts_name`
#if [ ! -f $CONFIG_FILE ]; then
#    echo "No Configuration file present at $CONFIG_FILE, exiting..."
#    exit 1
#fi

function cleanup {

   #special handling for CHF/OCRT logs and pcap
   if [ -z "$CLEANUP_MINS" ]; then
    echo "No M1 Special handling"
   else
    find $CLEANUP_DIR -type f \( -iname "*.log" ! -iname "*-json.log" -or -iname "*.pcap*"  ! -iname "*.pcap*.gz"  \) -mmin $CLEANUP_MINS -exec echo "Delete file {}" \; -exec rm {} \; 
   fi
   
   #Delete zip file
   #find $CLEANUP_DIR -type f \( -iname "*.log.gz"  -or -iname "*.pcap*.gz"   \) -mtime $CLEANUP_DAYS -exec echo "Delete file {}" \; -exec rm {} \; 
   CMD=`echo "find $CLEANUP_DIR -type f \( $CLEANUP_FIND_FILTERS  \) -mtime $CLEANUP_DAYS -exec echo "Delete file {}" \; -exec rm {} \;"`
   eval $CMD

}

echo "Running cleanup $(date)"
cleanup
