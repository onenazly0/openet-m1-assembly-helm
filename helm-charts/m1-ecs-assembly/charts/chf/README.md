SBA CHF Helm Chart Packaging

## Overview

This module builds the helm chart that allows the CHF to be deployed to a kubernetes cluster

### Nchf_ConvergedCharging OAPI specification
 `api.yaml` is an important artifact on a new release, as it contains API definitions of ConvergedCharging Services .

## Configuration

The following helm values are used to alter a given deployment in the cluster

### General Configuration

Parameter | Description | Default
--- | --- | ---
`replicaCount` | The number of CHF pod instances | `1`
`nfServiceType` | Used for registering the service in consul | `unset`
`restartVersion` | To be incremented to provoke a rolling redeploy of the CHF service pods | `0`
`image.registry` | Docker registry | `artifactory.openet.com:5000`
`image.repository` | Docker repository | `/chf/chf-server`
`image.tag` | Docker image tag | the project version
`image.pullPolicy` | Docker image pull policy | `Always`
`installFilebeat` | Install the filebeat sidecar container if `true` | `true`
`installRsync` | Install the rsync init and sidecar containers if `true` | `true`

### Configuration Files

When the following values are set with a file location the file is used to override the respective functionality with custom configuration

__NOTE__ The ''Changes Require Restart'' column indicates whether changes to the file require the service process to be restarted or whether the changes are dynamically reloaded once updated on the filesystem

Parameter | Description | Default Value | Changes Require Restart
--- | --- | --- | ---
`customSBASessionManagerYaml` | SBA Session Manager configuration | uses file built into the chart | No
`customOffercatalogRuntimeApiClientYaml` | Offer Catalog Runtime Api Client configuration | uses file built into the chart  | No
`customLocationDetectionStageYaml` | Stage configuration for Location Detection | uses file built into the chart | No
`customStatisticsConfigYaml` | Statistics Config | uses file built into the chart | No
`customAlarmsConfigYaml`: | Alarms Config | uses file built into the chart | Yes
`customLoggingConfigXml`: | Log and Log Lavel Config | uses file built into the chart that can be controlled and extended by value definitions  | No
`customChargingManagerYaml` | Charging Manager configuration | uses file built into the chart | No
`customTimerClientYaml` | Timer Client configuration | uses file built into the chart | No
`customAuditWriteStageYaml` | Stage configuration for Audit Writer  | uses file built into the chart | No
`customChargingDPStageYaml` | Stage configuration for ChargingDP | uses file built into the chart | No
`customChfCdrStageYaml` | Chf Cdr Stage configuration | uses file built into the chart | No
`customRecordWriterComponentAuditYaml` | Record Writer Component Audit configuration | uses file built into the chart | No
`customRecordWriterComponentChfCdrYaml` | Record Writer Component ChfCdr configuration | uses file built into the chart | No
`customRecordWriterComponentNotifyEdrYaml` | Record Writer Component Operation for Notification Edr | uses file built into the chart | No
`customApplyDeltaUsageStageYaml` | Apply Delta Usage Stage configuration | uses file built into the chart | No
`customAuthorisationStageYaml` | Authorisation Stage configuration  | uses file built into the chart | No
`customGetOrCreateSessionStageYaml` | Stage configuration for  GetOrCreateSession | uses file built into the chart | No
`customOfferPreparationStageYaml` | Stage configuration for OfferPreparation | uses file built into the chart | No
`customRsyncConfigJson` | Rsync client configuration defining source and destination locations to be transferred | uses file built into the chart | No
`customRsyncClientPostSyncScript` | Script to provide custom operation on the microservice after the rsync operation is complete | uses file built into the chart | No
`customAuditCDRConfigXml` | Configuration defining the Audit Operation and CDR format | uses file built into the chart | No
`customChfCDRConfigXml` | Configuration defining the CHF CDR format | uses file built into the chart | No
`customNotifyEdrConfigXml` | Configuration defining the EDR Notification format | uses file built into the chart | No
`customProfileManagerYaml` | Profile Manager endpoint configuration | uses file built into the chart | No

### Log Levels
Parameter | Description | Default
--- | --- | ---
`logging.logLevel` | Sets the Log Level in the log file. Applied dynamically and changes log output without restart | `INFO`
`logging.extension` | Custom log file settings that are added in-place in the logback file. Yaml format. Default empty | ``

### Rsync Configuration 

Parameter | Description | Default
--- | --- | ---
`rsync.name` | Name for rsync sidecar container | `rsync-client`
`rsync.image.registry` | Docker registry | `artifactory.openet.com:5000`
`rsync.image.repository` | Docker repository | `rsync/rsync-client`
`rsync.image.tag` | Rsync docker image tag | `1.1.0`
`rsync.image.pullPolicy` | Docker image pull policy | `Always`
`rsync.commandArgs` | Extra rsync command switches used by the client | ` -r --delete --password-file=/tmp/rsync/password `
`rsync.syncCheckPeriodSecs` | How often the client checks if a sync operation should be run, in seconds | `10`
`rsync.configVersion` | To be incremented to provoke a synchronisation operation to transfer latest files  | `0`
`rsync.livenessProbe.healthFile` | The location of the file used in the liveness check | `/mnt/health/liveness`
`rsync.readinessProbe.healthFile` | The location of the file used in the readiness check | `/mnt/health/readiness`
`rsyncd.service.name` | The rsync service name to be used in rsync operations | `rsyncd-svc`
`rsyncd.clusterDomain` | Cluster Domain name | `svc.cluster.local`

### voltClient Configuration 

Parameter | Description | Default
--- | --- | ---
`voltClient.username` | Allow setting of username for VoltClient. If used it must match username of voltdb-pm | ` `
`voltClient.password` | Allow setting of password for VoltClient. If used it must match password of voltdb-pm | ` `
`voltClient.clientCount` | Allow setting of number of volt clients ` `
`voltClient.voltDBTimeout` | Allow setting of number of volt client timeout | ` `

### Elasticsearch Appender Configuration

CHF by default stream CDR/EDR to ADS elasticsearch. In case of the ADS Elasticsearch not available, CDR/EDR will be written into filesystem via RollingFile appender.

ADS Elasticsearch by default applies HTTP Basic authentication using username "openet" with password "Openet01". This is set as default value in CHF Helm chart.

*Note* There are currently 2 versions of appenders available to send to elasticsearch. The existing appender, called "Elasticsearch" in the log4j2 xml file, is being replaced with a new appender called "OpenetElastic". They share most of the same parameters, some are only applicable to the new appender, and will be ignored by the old one, and vice versa. The "Elasticsearch" appender will be removed in an upcoming release. 

Parameter | Description | Default
--- | --- | ---
`elasticAppender.elasticHost` | Elasticsearch URL of ADS host| `http://ads-elasticsearch-es-http.ads.svc.cluster.local:9200`
`elasticAppender.authEnabled` | Use HTTP Basic Authentication when connecting the ADS Elasticseach Host if `true` | `true`
`elasticAppender.auth.username` | Username for HTTP Basic Authentication | `openet`
`elasticAppender.auth.password` | Password for HTTP Basic Authentication | `Openet01`
`elasticAppender.notifyEDRIndexName` | Elasticsearch index name (or index alias) for EDR Notification | `sitea.usage.fct.notify-edr.0`
`elasticAppender.chfCDRIndexName` | Elasticsearch index name (or index alias) for CHF CDR | `sitea.usage.fct.chf-cdr.0`
`elasticAppender.auditCDRIndexName` | Elasticsearch index name (or index alias) for CCS CDR | `ccs-cdr`
`elasticAppender.connTimeout` | Number of milliseconds before ConnectException is thrown while attempting to connect to ADS Elasticseach Host. | `1000`
`elasticAppender.deliveryInterval` | Number of milliseconds between deliveries | `1000`
`elasticAppender.batchSize` | Approximate maximum number of logs delivered in one batch | `1000`
`elasticAppender.itemSizeInBytes` | Initial size of single buffer instance (Object pooling for item processing for Layout Formatting and the Appender) | `1024`
`elasticAppender.initialPoolSize` | Number of pooled elements created at startup (Object pooling for item processing for Layout Formatting and the Appender) | `100`
`elasticAppender.ioThreads` | *OpenetElastic only* Allows the user to explicitly override the default value which will be determined by a call to Runtime.getRuntime().availableProcessors(); *Note* getAvailableProcessors can be unreliable on containers, so this number should be set to match the number of verticles. |  `8`
`elasticAppender.concurrentHttpRequests` | *OpenetElastic only* Allows the user to explicitly override the default value which is 1, this means at most 1 bulk request will be in flight while the next is being accumulated. | `1`

Log4j2 Elasticsearch Appender website: https://github.com/rfoltyns/log4j2-elasticsearch

OpenetElastic Appender udocs: [udocs](https://bitbucket.openet.com/projects/COMP/repos/sba-record-writer-impl/browse/udocs/openet_elastic_plugin.md)

## Deployment

`# CHF Microservice in kubernetes

The CHF Microservice is deployed as part of the overall helm chart ecs-assembly-1.x.x.tgz into kubernetes using Helm refer to README under ecs-assembly helm 
