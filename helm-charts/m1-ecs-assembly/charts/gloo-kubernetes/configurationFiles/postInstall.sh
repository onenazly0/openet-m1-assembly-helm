#!/bin/sh

kubectl patch --type=merge deployment -n {{ .Release.Namespace }} gateway-proxy -p "$(cat /etc/postinstall/proxy-deployment-patch.yaml)"
kubectl patch --type=merge svc -n {{ .Release.Namespace }} gateway-proxy -p "$(cat /etc/postinstall/service-patch.yaml)"
kubectl delete us -n {{ .Release.Namespace }} -l 'discovered_by in (consulplugin,kubernetesplugin)'
