{{- define "registry_secret" -}}
{{-  $auths :=  printf "%s:%s" .Values.global.imagePullSecret.username .Values.global.imagePullSecret.password -}}
{"auths":{ {{.Values.global.imagePullSecret.repo | quote}}:{"username":{{.Values.global.imagePullSecret.username | quote}},"password":{{.Values.global.imagePullSecret.password | quote}},"auth": {{ $auths | b64enc | quote }} }}}
{{- end -}}

{{/*
Populate the sslConfig of the VirtualService. Default the TLS secret to the name configured in the certificate property.
*/}}
{{- define "virtualService.sslConfig" -}}
{{- $defaultTplValues := dict "certificate" .certificate "Release" .root.Release "Template" .root.Template }}
{{- $defaultSSLConfig := include "virtualService.sslConfig.tpl" $defaultTplValues | fromYaml }}
{{- $sslConfig := .sslConfig | default dict -}}
{{- $finalSslConfig := merge $sslConfig $defaultSSLConfig }}
sslConfig:
{{ toYaml $finalSslConfig | indent 2 -}}
{{- end -}}


{{/*
Check if TLS is enabled by checking both global and ingress Helm values. If ingress is configured, it takes precedence.
*/}}

{{- define "gloo-kubernetes.is-https-enabled" -}}
{{ ternary .Values.global.ingress.protocols.https.enabled .Values.global.tls.enabled (hasKey .Values.global.ingress.protocols.https "enabled") }}
{{- end -}}

{{- define "gloo-kubernetes.is-http-enabled" -}}
{{ ternary .Values.global.ingress.protocols.http.enabled (not .Values.global.tls.enabled) (hasKey .Values.global.ingress.protocols.http "enabled") }}
{{- end -}}
