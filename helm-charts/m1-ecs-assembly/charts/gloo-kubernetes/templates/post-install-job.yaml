---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  name: "gloo-init"
  annotations:
    # This is what defines this resource as a hook. Without this line, the
    # job is considered part of the release.
    "helm.sh/hook": post-install,post-upgrade
    "helm.sh/hook-weight": "0"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
rules:
- apiGroups: [""]
  resources: ["services"]
  verbs: ["get", "patch"]
- apiGroups: ["apps"]
  resources: ["deployments"]
  verbs: ["get", "patch"]
- apiGroups: ["gloo.solo.io"]
  resources: ["settings", "upstreams"]
  verbs: ["get", "patch", "delete", "list"]

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  name: "gloo-init"
  annotations:
    # This is what defines this resource as a hook. Without this line, the
    # job is considered part of the release.
    "helm.sh/hook": post-install,post-upgrade
    "helm.sh/hook-weight": "1"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
subjects:
  - kind: ServiceAccount
    name: "gloo-init"
roleRef:
  kind: Role
  name: "gloo-init"
  apiGroup: rbac.authorization.k8s.io
  
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: "gloo-init"
  labels:
    app: "gloo-init"
    chart: {{ .Chart.Name }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
  annotations:
    # This is what defines this resource as a hook. Without this line, the
    # job is considered part of the release.
    "helm.sh/hook": post-install,post-upgrade
    "helm.sh/hook-weight": "2"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ .Release.Name }}-post-install-resources"
  annotations:
    # This is what defines this resource as a hook. Without this line, the
    # job is considered part of the release.
    "helm.sh/hook": post-install,post-upgrade
    "helm.sh/hook-weight": "3"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
data:
  proxy-deployment-patch.yaml: |-
{{ (tpl (.Files.Get "configurationFiles/proxy-deployment-patch.yaml") . ) | indent 4 }}
  settings-patch.yaml: |-
{{ (tpl (.Files.Get "configurationFiles/settings-patch.yaml") . ) | indent 4 }}
  service-patch.yaml: |-
{{ (tpl (.Files.Get "configurationFiles/service-patch.yaml") . ) | indent 4 }}
  postInstall.sh: |-
{{ (tpl (.Files.Get "configurationFiles/postInstall.sh") . ) | indent 4 }}

---

{{- $image := .Values.postInstall.image }}
{{- if .Values.global.image  }}
{{- $image = merge .Values.postInstall.image .Values.global.image }}
{{- end }}

apiVersion: batch/v1
kind: Job
metadata:
  name: "{{ .Release.Name }}"
  labels:
    app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
    app.kubernetes.io/instance: {{ .Release.Name | quote }}
    app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
    helm.sh/chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
  annotations:
    # This is what defines this resource as a hook. Without this line, the
    # job is considered part of the release.
    "helm.sh/hook": post-install,post-upgrade
    "helm.sh/hook-weight": "4"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
spec:
  activeDeadlineSeconds: {{ .Values.postInstall.activeDeadlineSeconds }}  
  backoffLimit: {{ .Values.postInstall.backoffLimit }}
  template:
    metadata:
      name: "{{ .Release.Name }}"
      labels:
        app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
        app.kubernetes.io/instance: {{ .Release.Name | quote }}
        helm.sh/chart: "{{ .Chart.Name }}-{{ .Chart.Version }}"
    spec:
{{- if $image.pullSecret }}
      imagePullSecrets:
      - name: {{ $image.pullSecret }}
{{- end }}
      serviceAccountName: "gloo-init"
      restartPolicy: Never
      containers:
      - name: post-install-job
        # Do not use merge values with global as the Gloo registry does not contain this image
        image: {{ .Values.postInstall.image.registry }}/{{ .Values.postInstall.image.name }}:{{ .Values.postInstall.image.tag }}
        volumeMounts:
        - name: post-install-resources-volume
          mountPath: /etc/postinstall
        command:
        - "/etc/postinstall/postInstall.sh"
      volumes:
      - name: post-install-resources-volume
        configMap:
          name: "{{ .Release.Name }}-post-install-resources"        
          defaultMode: 0777        
{{- with .Values.postInstall.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}

