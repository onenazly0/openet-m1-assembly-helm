{{- define "sigm.name" -}}
{{- default .Chart.Name .Values.chart_name_override | trunc 63 | replace "_" "-"  | trimSuffix "-" -}}
{{- end -}}

{{/* print namespace in format: "my/name/space/" (single slash only as suffix), or empty string if the value is empty */}}
{{- define "url.append_slash" -}}
{{- if . -}}
    {{.| trimAll "/"}}{{"/"}}
{{- end -}}
{{- end -}}

{{- define "sigm.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* append 'dnsPostfix' to rsyncd services when defined */}}
{{- define "appendDnsPostfix" -}}
{{- if $.Values.rsyncd.dnsPostfix }}.{{ $.Values.rsyncd.dnsPostfix }}{{ end -}}
{{- end -}}

