#!/bin/sh

NAMESPACE={{ .Release.Namespace }}
TOKEN={{.Values.serviceDiscovery.consulClient.aclToken}}
CONSUL_HOST={{.Values.serviceDiscovery.consulClient.hostName}}
{{- if .Values.serviceDiscovery.consulClient.port }}
CONSUL_PORT={{.Values.serviceDiscovery.consulClient.port}}
{{- else}}
CONSUL_PORT=8500
{{- end }}

DEPLOYMENT_TERMINATE_TIMEOUT={{.Values.postInstall.previousDeploymentTerminateTimeoutSeconds}}

CONSUL_ADDRESS=$CONSUL_HOST:$CONSUL_PORT
POD_PRE_4_2_0=$(kubectl get pods -n $NAMESPACE -o json | jq -r '.items[] | select(.spec.containers[0].image | test("registration-agent-server:(([0-3]\\.[0-9]+\\.[0-9]+)|(4\\.[0-1]\\.[0-9]+))(-SNAPSHOT)?$")).metadata.name')
if [ "$POD_PRE_4_2_0" == "" ] ; then
  echo "No NFRA pods with version prior to 4.2.0 found"
else
  echo "Waiting for termination of $POD_PRE_4_2_0"
  kubectl wait pods $POD_PRE_4_2_0 -n $NAMESPACE --for=delete --timeout=${DEPLOYMENT_TERMINATE_TIMEOUT}s
  if [ $? -gt 0 ] ; then
    echo "Timeout waiting for termination of $POD_PRE_4_2_0"
    exit 1
  fi
fi

NODE_IPS=$(curl -s -G -H "X-Consul-Token:$TOKEN" http://$CONSUL_ADDRESS/v1/catalog/nodes --data-urlencode 'filter=Node != "scc.external.node" and Node != "k8s-sync"'|jq -r .[].Address)
if [ $? -gt 0 ] ; then
  echo "Failed to obtain list of consul nodes from local agent"
  exit 1
fi

echo "Using Consul nodes: $NODE_IPS"


touch error.log
STATUS=0

for IP in $NODE_IPS 
do
  echo "Query agent $IP:$CONSUL_PORT for NFRA service in namespace $NAMESPACE"
  SERVICES=$(curl -s -G -H "X-Consul-Token:$TOKEN" "http://$IP:$CONSUL_PORT/v1/agent/services" --data-urlencode "filter=\"$NAMESPACE\" in Tags and NFRA in Tags"| jq -r '.[].ID')
  for SRV in $SERVICES
  do
    echo "Deregistering service: $SRV from node $IP:$CONSUL_PORT"
  	curl -f -s -X PUT -H "X-Consul-Token:$TOKEN" "http://$IP:$CONSUL_PORT/v1/agent/service/deregister/${SRV}";
    if [ $? -gt 0 ] ; then
      echo "Failed to deregister service $SRV from node $IP:$CONSUL_PORT" >> error.log
      STATUS=1
    fi
  done
done

cat error.log
exit $STATUS

