SBA SLC Helm Chart Packaging

## Overview

This module builds the helm chart that allows the SLC to be deployed to a kubernetes cluster

### Nchf-SpendingLimitControl OAPI specification
 `api.yaml` is an important artifact on a new release, as it contains API definitions of SpendingLimitControl Service .

## Configuration

The following helm values are used to alter a given deployment in the cluster

### General Configuration

Parameter | Description | Default
--- | --- | ---
`replicaCount` | The number of SLC pod instances | `1`
`pathPrefix` | Value for ambassador annotation of the service | `nchf-spendinglimitcontrol/v1`
`image.registry` | Docker registry | `artifactory.openet.com:5000`
`image.repository` | Docker repository | `/sba-microservice/slc-server`
`image.tag` | Docker image tag | the project version
`image.pullPolicy` | Docker image pull policy | `Always`
`filebeat.enabled` | Install the filebeat sidecar container if `true` | `true`
`installRsync` | Install the rsync init and sidecar containers if `true` | `true`

### Configuration Files

When the following values are set with a file location the file is used to override the respective functionality with custom configuration

__NOTE__ The ''Changes Require Restart'' column indicates whether changes to the file require the service process to be restarted or whether the changes are dynamically reloaded once updated on the filesystem

Parameter | Description | Default Value | Changes Require Restart
--- | --- | --- | ---
`customAlarmsConfigYaml`: | Alarms Config | uses file built into the chart | Yes
`customLoggingConfigXml`: | Log and Log Lavel Config | uses file built into the chart that can be controlled and extended by value definitions  | No
`customStatisticsConfigYaml` | Statistics Config | uses file built into the chart | No
`customRsyncConfigJson` | Rsync client configuration defining source and destination locations to be transferred | uses file built into the chart | No
`customRsyncClientPostSyncScript` | Script to provide custom operation on the microservice after the rsync operation is complete | uses file built into the chart | No
`customSBASessionManagerYaml` | SBA Session Manager configuration | uses file built into the chart | No
`customRecordWriterComponentNotifyEdrYaml` | Record Writer Component Operation for Notification Edr | uses file built into the chart | No
`customTimerClientYaml` | Timer Client configuration | uses file built into the chart | No
`customSpendingLimitManagerYaml` | Spending Limit Manager configuration | uses file built into the chart | No
`customNotifyEdrConfigXml` | Configuration defining the Notify EDR format | uses file built into the chart | No

### Log Levels
Parameter | Description | Default
--- | --- | ---
`logging.logLevel` | Sets the Log Level in the log file. Applied dynamically and changes log output without restart | `INFO`
`logging.extension` | Custom log file settings that are added in-place in the logback file. Yaml format. Default empty | ``

### Rsync Configuration

Parameter | Description | Default
--- | --- | ---
`rsync.name` | Name for rsync sidecar container | `rsync-client`
`rsync.image.registry` | Docker registry | `artifactory.openet.com:5000`
`rsync.image.repository` | Docker repository | `rsync/rsync-client`
`rsync.image.tag` | Rsync docker image tag | `2.0.1`
`rsync.image.pullPolicy` | Docker image pull policy | `Always`
`rsync.commandArgs` | Extra rsync command switches used by the client | ` -r --delete --password-file=/tmp/rsync/password `
`rsync.syncCheckPeriodSecs` | How often the client checks if a sync operation should be run, in seconds | `10`
`rsync.configVersion` | To be incremented to provoke a synchronisation operation to transfer latest files  | `0`
`rsync.livenessProbe.healthFile` | The location of the file used in the liveness check | `/mnt/health/liveness`
`rsync.readinessProbe.healthFile` | The location of the file used in the readiness check | `/mnt/health/readiness`
`rsyncd.service.name` | The rsync service name to be used in rsync operations | `rsyncd-svc`
`rsyncd.clusterDomain` | Cluster Domain name | `svc.cluster.local`

### voltClient Configuration

Parameter | Description | Default
--- | --- | ---
`voltClient.username` | Allow setting of username for VoltClient. If used it must match username of voltdb-pm | ` `
`voltClient.password` | Allow setting of password for VoltClient. If used it must match password of voltdb-pm | ` `
`voltClient.clientCount` | Allow setting of number of volt clients ` `
`voltClient.voltDBTimeout` | Allow setting of number of volt client timeout | ` `

## Deployment

# SLC Microservice in kubernetes

The SLC Microservice is deployed as part of the overall helm chart ecs-assembly-1.x.x.tgz into kubernetes using Helm refer to README under ecs-assembly helm
