global:
    config_version: 3
    retention_check_interval: 1s
input:
    type: file
    paths:
    -  /statistics/*_JPCF_high.stats.csv
    -  /statistics/*_JPCF_Compositehigh.stats.csv
    readall: false
    poll_interval_seconds: 1
imports:
  - type: grok_patterns
    dir: ./openet-patterns
grok_patterns:
  - 'STATISTIC_DURATION [0-9]*'

metrics:
    ### COUNTER METRICS

    - type: counter
      name: exporter_openet_counter_statistic_name_cte
      help: The total count of each statistic from the statistic catalog in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      labels:
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: counter
      name: exporter_openet_counter_category_cte
      help: The total count of each statistic category used in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      labels:
        category: '{{ "{{" }}.category{{ "}}" }}'
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    ### GAUGE METRICS
    - type: gauge
      name: exporter_openet_gauge_numeric_value_cte
      help: The latest count in a statistic of category type PC each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},PC,%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.numValue{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_numeric_value_cumulative_cte
      help: The cumulative number of a statistic of category type PC in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},PC,%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.numValue{{ "}}" }}'
      cumulative: true # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_tps_cte
      help: The last observed value of transactions/s in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.tps{{ "}}" }}'
      cumulative: true # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_single_latency_cte
      help: The duration of the last operation performed in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.sLatency{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_average_latency_cte
      help: The average latency calculated in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.avgLatency{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_max_latency_cte
      help: The highest latency calculated in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.maxLatency{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_min_latency_cte
      help: The lowest latency calculated in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.minLatency{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        statistic_name: '{{ "{{" }}.statName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_statistic_duration_cte
      help: The duration of the last statistic in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.statDuration{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_exception_count_cte
      help: The number of exceptions caught during the last statistic in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.exCount{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: gauge
      name: exporter_openet_gauge_total_transactions_cte
      help: The total number of statistics for a given period in each component
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.totalTransactions{{ "}}" }}'
      cumulative: false # DEFAULT. If true - gauge will return the sum of observed values.
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    ### SUMMARY METRICS
    - type: summary
      name: exporter_openet_summary_tps_cte
      help: The summary of tps in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.tps{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: summary
      name: exporter_openet_summary_single_latency_cte
      help: The summary of single latency in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.sLatency{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: summary
      name: exporter_openet_summary_average_latency_cte
      help: The summary of average latency in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.avgLatency{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: summary
      name: exporter_openet_summary_max_latency_cte
      help: The summary of max latency in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.maxLatency{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: summary
      name: exporter_openet_summary_min_latency_cte
      help: The summary of min latency in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.minLatency{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
         cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: summary
      name: exporter_openet_summary_statistic_duration_cte
      help: The summary of statistic duration in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.statDuration{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: summary
      name: exporter_openet_summary_exception_count_cte
      help: The summmary of exception counts in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.exCount{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

    - type: summary
      name: exporter_openet_summary_total_transactions_cte
      help: The summary of total transactions in each cte
      match: '%{ACTION},%{DISCRIMINATOR:disc}(,[0-9],|,,)%{INSTALLATION_ID:installId},%{NUMERIC_VALUE:numValue},%{STRING_VALUE:stringVal},%{COMPONENT_NAME:compName},%{TRANSACTION_ID:tId},%{STATISTIC_DURATION:statDuration},%{STATISTIC_NAME:statName},%{ASSOCIATED_NAME:aName},%{SINGLE_LATENCY:sLatency},%{AVERAGE_LATENCY:avgLatency},%{MAX_LATENCY:maxLatency},%{MIN_LATENCY:minLatency},%{TPS:tps},%{MAX_LATENCY_TRANSACTION_ID:maxLatTID},%{CREATED_ON_DATE:created},%{TOTAL_TRANSACTIONS:totalTransactions},%{PROCESS_NAME:processName},%{SLA1:sla1},%{SLA2:sla2},%{EXCEPTION_COUNT:exCount},%{CATEGORY:category},%{P95:p95},%{P99:p99},%{P999:p999},%{P9999:p9999},%{P99999:p99999}'
      value: '{{ "{{" }}.totalTransactions{{ "}}" }}'
      quantiles: {0.5: 0.05, 0.9: 0.01, 0.99: 0.001} # DEFAULT - These can be configured differently
      labels:
        cte_name: '{{ "{{" }}.processName{{ "}}" }}'

server:
    host: 0.0.0.0
    port: 9144