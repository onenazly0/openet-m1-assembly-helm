{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "slc.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "slc.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $defaultName := default .Chart.Name .Values.nameOverride -}}
{{- if contains $defaultName .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $defaultName | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "slc.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* append 'dnsPostfix' to rsyncd services when defined */}}
{{- define "appendDnsPostfix" -}}
{{- if $.Values.rsyncd.dnsPostfix }}.{{ $.Values.rsyncd.dnsPostfix }}{{ end -}}
{{- end -}}

{{/*
Get the service protocol (HTTP/HTTPS)
*/}}
{{- define "slc.service-protocol" -}}
{{- if eq (include "slc.is-tls-enabled" .) "true" }}
{{- printf "https" }}
{{- else }}
{{- printf "http" }}
{{- end -}}
{{- end -}}

{{/*
Get the pod's target port name
*/}}
{{- define "slc.sba-ms-port-name" -}}
sba-ms-{{ template "slc.service-protocol" . }}
{{- end -}}

{{/*
Check if TLS is enabled by checking both global and microservice Helm values. If microservice is configured, it takes precedence.
*/}}
{{- define "slc.is-tls-enabled" -}}
{{ ternary .Values.tls.enabled .Values.global.tls.enabled (hasKey .Values.tls "enabled") }}
{{- end -}}