{{- if .Values.metrics.enabled }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-metrics
  namespace: {{ $.Release.Namespace }}
  labels:
    name: {{ template "voltdb-operator.name" . }}-metrics
{{ include "voltdb-operator.labels" . | indent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-metrics
  template:
    metadata:
      labels:
        name: {{ template "voltdb-operator.name" . }}-metrics
{{ include "voltdb-operator.labels" . | indent 8 }}
    spec:
      serviceAccountName: {{ template "voltdb-operator.cluster.serviceAccountName" . }}
      automountServiceAccountToken: true
{{- if .Values.global.imagePullSecrets }}
      imagePullSecrets:
{{ toYaml .Values.global.imagePullSecrets | indent 6 }}
{{- end }}
      containers:
      - name: {{ template "voltdb-operator.name" . }}-metrics
        image: {{ with .Values.metrics.image }}{{ .registry }}/{{ .repository }}:{{ .tag }}{{ end }}
        imagePullPolicy: {{ .Values.metrics.image.pullPolicy }}
        args:
{{- if .Values.metrics.servers }}
        - "--servers={{ .Values.metrics.servers }}"
{{- else }}
        - "--servers={{ template "voltdb-operator.fullname" . }}-cluster-internal"
{{- end }}
{{- if .Values.metrics.port }}
        - "--port={{ .Values.metrics.port }}"
{{- end }}
{{- if .Values.metrics.stats }}
        - "--stats={{ .Values.metrics.stats }}"
{{- end }}
{{- if .Values.metrics.skipstats }}
        - "--skipstats={{ .Values.metrics.skipstats }}"
{{- end }}
        - "--webserverport=8484"
{{- if .Values.metrics.resources }}
        resources:
{{ toYaml .Values.metrics.resources | indent 10 }}
{{ else }}
        resources:
          requests:
            cpu: 10m
            memory: 50Mi
          limits:
            cpu: 1
            memory: 512Mi
{{- end }}
{{- if .Values.cluster.securityContext }}
        securityContext:
{{ toYaml .Values.cluster.securityContext | indent 10 }}
{{ else }}
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
{{- end }}
{{- if .Values.metrics.ssl.enabled }}
        volumeMounts:
        - mountPath: /etc/voltdb/ssl
          name: {{ template "voltdb-operator.fullname" . }}-metrics-ssl
{{- end }}
        env:
          - name: POD_NAME
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: POD_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          - name: POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          - name: HOST_IP
            valueFrom:
              fieldRef:
                fieldPath: status.hostIP
          - name: POD_SERVICE_ACCOUNT
            valueFrom:
              fieldRef:
                fieldPath: spec.serviceAccountName
{{- if .Values.metrics.user }}
          - name: VOLTDB_USERNAME
            valueFrom:
              secretKeyRef:
                name: {{ template "voltdb-operator.fullname" . }}-metrics-auth
                key: username
{{- end }}
{{- if .Values.metrics.password }}
          - name: VOLTDB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ template "voltdb-operator.fullname" . }}-metrics-auth
                key: password
{{- end }}
{{- if .Values.metrics.ssl.enabled }}
          - name: SSL_ENABLED
            valueFrom:
              secretKeyRef:
                name: {{ template "voltdb-operator.fullname" . }}-metrics-auth
                key: ssl_enabled
      volumes:
      - name: {{ template "voltdb-operator.fullname" . }}-metrics-ssl
        secret:
          defaultMode: 0644
          optional: true
          items:
          - key: truststore_data
            path: truststore.jks
          - key: certificate.txt
            path: certificate.txt
          secretName: {{ template "voltdb-operator.fullname" . }}-metrics-ssl
{{- end }}
{{- end }}
