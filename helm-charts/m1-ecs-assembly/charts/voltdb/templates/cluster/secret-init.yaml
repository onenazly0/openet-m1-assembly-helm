{{- if .Values.cluster.enabled }}
  {{- if .Values.cluster.clusterSpec.clusterInit.initSecretRefName }}
  {{- else }}
apiVersion: v1
kind: Secret
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-cluster-init
  namespace: {{ $.Release.Namespace }}
  labels:
    name: {{ template "voltdb-operator.name" . }}-cluster
  {{ include "voltdb-operator.labels" . | indent 4 }}
type: Opaque
stringData:
  {{- if .Values.cluster.config.deploymentXMLFile }}
  deployment.xml:
  {{ toYaml .Values.cluster.config.deploymentXMLFile | indent 4 }}
  {{- else }}
  deployment.xml: |
    <?xml version="1.0"?>
    <deployment>
      <cluster kfactor="{{ .Values.cluster.config.deployment.cluster.kfactor }}"
               sitesperhost="{{ .Values.cluster.config.deployment.cluster.sitesperhost }}"/>

      <heartbeat timeout="{{ .Values.cluster.config.deployment.heartbeat.timeout }}"/>
      <partition-detection enabled="{{ .Values.cluster.config.deployment.partitiondetection.enabled }}"/>

      <commandlog enabled="{{ .Values.cluster.config.deployment.commandlog.enabled }}"
        logsize="{{ .Values.cluster.config.deployment.commandlog.logsize }}"
        synchronous="{{ .Values.cluster.config.deployment.commandlog.synchronous }}">
        <frequency time="{{ .Values.cluster.config.deployment.commandlog.frequency.time }}"
          transactions="{{ .Values.cluster.config.deployment.commandlog.frequency.transactions }}"/>
      </commandlog>

    {{- if .Values.cluster.config.deployment.dr.connection.enabled }}
      <dr id="{{ .Values.cluster.config.deployment.dr.id }}"
        role="{{ .Values.cluster.config.deployment.dr.role }}">
        <connection enabled="{{ .Values.cluster.config.deployment.dr.connection.enabled }}"
          source="{{ .Values.cluster.config.deployment.dr.connection.source }}"
    {{- if .Values.cluster.config.deployment.dr.connection.preferredSource }}
          preferred-source="{{ .Values.cluster.config.deployment.dr.connection.preferredSource }}"
    {{- end }}
    {{- if and .Values.cluster.config.deployment.ssl.enabled .Values.cluster.config.deployment.ssl.dr }}
    {{- if .Values.cluster.config.deployment.ssl.truststore.file }}
          ssl="/etc/voltdb/ssl/certificate.txt"
    {{- else if .Values.cluster.config.deployment.dr.connection.ssl }}
          ssl="{{ .Values.cluster.config.deployment.dr.connection.ssl }}"
    {{- else }}
          ssl=""
    {{- end }}
    {{- end }}
          />
    {{- if .Values.cluster.config.deployment.dr.consumerlimit }}
    {{- if or .Values.cluster.config.deployment.dr.consumerlimit.maxsize .Values.cluster.config.deployment.dr.consumerlimit.maxbuffers }}
        <consumerlimit>
    {{- if .Values.cluster.config.deployment.dr.consumerlimit.maxsize }}
          <maxsize>{{ .Values.cluster.config.deployment.dr.consumerlimit.maxsize }}</maxsize>
    {{- else if .Values.cluster.config.deployment.dr.consumerlimit.maxbuffers }}
          <maxbuffers>{{ .Values.cluster.config.deployment.dr.consumerlimit.maxbuffers }}</maxbuffers>
    {{- end }}
        </consumerlimit>
    {{- end }}
    {{- end }}
      </dr>
    {{- end }}

    {{- if .Values.cluster.config.deployment.export }}
    {{- if .Values.cluster.config.deployment.export.configurations }}
      <export>
    {{- range .Values.cluster.config.deployment.export.configurations }}
    {{- if eq .type "custom" }}
    {{- if empty (get .properties "exportconnectorclass") }}
    {{- fail "exportconnectorclass must be specified for custom export." }}
    {{- end }}
        <configuration target="{{ .target }}" type="{{ .type }}" enabled="{{ .enabled }}" exportconnectorclass="{{ .exportconnectorclass }}">
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else if eq .type "file" }}
        <configuration target="{{ .target }}" type="{{ .type }}" enabled="{{ .enabled }}">
    {{- if empty (get .properties "nonce") }}
    {{- fail "nonce must be specified for file export." }}
    {{- end }}
    {{- if and (ne (upper (get .properties "type")) "CSV") (ne (upper (get .properties "type")) "TSV") }}
    {{- fail "type must be csv or tsv for file export." }}
    {{- end }}
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else if eq .type "jdbc" }}
        <configuration target="{{ .target }}" type="{{ .type }}" enabled="{{ .enabled }}">
    {{- if empty (get .properties "jdbcurl") }}
    {{- fail "jdbcurl must be specified for jdbc export." }}
    {{- end }}
    {{- if empty (get .properties "jdbcuser") }}
    {{- fail "jdbcuser must be specified for jdbc export." }}
    {{- end }}
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else if eq .type "kafka" }}
        <configuration target="{{ .target }}" type="{{ .type }}" enabled="{{ .enabled }}">
    {{- if and (empty (get .properties "bootstrap.servers") ) (empty (get .properties "metadata.broker.list") ) }}
    {{- fail "bootstrap.servers or metadata.broker.list must be specified" }}
    {{- end }}
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else if eq .type "elasticsearch" }}
        <configuration target="{{ .target }}" type="{{ .type }}" enabled="{{ .enabled }}">
    {{- if empty (get .properties "endpoint") }}
    {{- fail "endpoint must be specified for elasticsearch export." }}
    {{- end }}
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else }}
        <configuration target="{{ .target }}" type="{{ .type }}" enabled="{{ .enabled }}">
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- end }}
    {{- end }}
      </export>
    {{- end }}
    {{- end }}

    {{- if .Values.cluster.config.deployment.import }}
    {{- if .Values.cluster.config.deployment.import.configurations }}
      <import>
    {{- range .Values.cluster.config.deployment.import.configurations }}
    {{- if eq .type "custom" }}
        <configuration type="{{ .type }}" enabled="{{ .enabled }}" module="{{ .module }}">
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else if eq .type "kafka" }}
    {{- if empty (get .properties "brokers") }}
    {{- fail "brokers must be specified for kafka import." }}
    {{- end }}
    {{- if empty (get .properties "topics") }}
    {{- fail "topics must be specified for kafka import." }}
    {{- end }}
        <configuration type="{{ .type }}" enabled="{{ .enabled }}" module="{{ .module }}">
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else if eq .type "kinesis" }}
    {{- if empty (get .properties "region") }}
    {{- fail "region must be specified for kinesis import." }}
    {{- end }}
    {{- if empty (get .properties "secret.key") }}
    {{- fail "secret.key must be specified for kinesis import." }}
    {{- end }}
    {{- if empty (get .properties "access.key") }}
    {{- fail "access.key must be specified for kinesis import." }}
    {{- end }}
    {{- if empty (get .properties "stream.name") }}
    {{- fail "stream.name must be specified for kinesis import." }}
    {{- end }}
    {{- if empty (get .properties "stream.name") }}
    {{- fail "stream.name must be specified for kinesis import." }}
    {{- end }}
        <configuration type="{{ .type }}" enabled="{{ .enabled }}" module="{{ .module }}">
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- else }}
        <configuration type="{{ .type }}" enabled="{{ .enabled }}" format="{{ .format }}">
    {{- range $key, $val := .properties }}
          <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
        </configuration>
    {{- end }}
    {{- end }}
      </import>
    {{- end }}
    {{- end }}

    {{- if .Values.cluster.config.deployment.threadpools }}
      <threadpools>
    {{- range .Values.cluster.config.deployment.threadpools }}
    {{- if or (empty .name) (empty .size) }}
    {{- fail "name and size must be specified for threadpool." }}
    {{- end }}
        <pool name="{{ .name }}" size="{{ .size }}" />
    {{- end }}
      </threadpools>
    {{- end }}

    {{- if .Values.cluster.config.deployment.task }}
        <task
    {{- if .Values.cluster.config.deployment.task.mininterval }}
            mininterval="{{ .Values.cluster.config.deployment.task.mininterval }}"
    {{- end }} {{- if .Values.cluster.config.deployment.task.maxfrequency }}
            maxfrequency="{{ .Values.cluster.config.deployment.task.maxfrequency }}"
    {{- end }}
          >
          <threadpools>
    {{- if .Values.cluster.config.deployment.task.host }}
            <host size="{{ .Values.cluster.config.deployment.task.host.size }}" />
    {{- end }}
    {{- if .Values.cluster.config.deployment.task.partition }}
            <partition size="{{ .Values.cluster.config.deployment.task.partition.size }}" />
    {{- end }}
          </threadpools>
        </task>
    {{- end }}

      <httpd enabled="{{ .Values.cluster.config.deployment.httpd.enabled }}">
        <jsonapi enabled="{{ .Values.cluster.config.deployment.httpd.jsonapi.enabled }}"/>
      </httpd>

      <paths>
        <voltdbroot path="/pvc/voltdb"/>
    {{- if .Values.cluster.config.deployment.paths.commandlog.path }}
        <commandlog path="{{ .Values.cluster.config.deployment.paths.commandlog.path }}"/>
    {{- end }}
    {{- if .Values.cluster.config.deployment.paths.commandlogsnapshot.path }}
        <commandlogsnapshot path="{{ .Values.cluster.config.deployment.paths.commandlogsnapshot.path }}"/>
    {{- end }}
    {{- if .Values.cluster.config.deployment.paths.droverflow.path }}
        <droverflow path="{{ .Values.cluster.config.deployment.paths.droverflow.path }}"/>
    {{- end }}
    {{- if .Values.cluster.config.deployment.paths.exportoverflow.path }}
        <exportoverflow path="{{ .Values.cluster.config.deployment.paths.exportoverflow.path }}"/>
    {{- end }}
    {{- if .Values.cluster.config.deployment.paths.exportcursor.path }}
        <exportcursor path="{{ .Values.cluster.config.deployment.paths.exportcursor.path }}"/>
    {{- end }}
    {{- if .Values.cluster.config.deployment.paths.largequeryswap.path }}
        <largequeryswap path="{{ .Values.cluster.config.deployment.paths.largequeryswap.path }}"/>
    {{- end }}
    {{- if .Values.cluster.config.deployment.paths.snapshots.path }}
        <snapshots path="{{ .Values.cluster.config.deployment.paths.snapshots.path }}"/>
    {{- end }}
      </paths>

      <security enabled="{{ .Values.cluster.config.deployment.security.enabled }}"
        provider="{{ .Values.cluster.config.deployment.security.provider }}"/>

      <snapshot enabled="{{ .Values.cluster.config.deployment.snapshot.enabled }}"
        frequency="{{ .Values.cluster.config.deployment.snapshot.frequency }}"
        prefix="{{ .Values.cluster.config.deployment.snapshot.prefix }}"
        retain="{{ .Values.cluster.config.deployment.snapshot.retain }}"/>

    {{- if .Values.cluster.config.deployment.avro }}
    {{- if not .Values.cluster.config.deployment.avro.registry }}
    {{- fail "registry must be specified for avro configuration." }}
    {{- end }}
       <avro registry="{{ .Values.cluster.config.deployment.avro.registry }}"
    {{- if .Values.cluster.config.deployment.avro.namespace }}
             namespace="{{ .Values.cluster.config.deployment.avro.namespace }}"
    {{- end }}
    {{- if .Values.cluster.config.deployment.avro.prefix }}
             prefix="{{ .Values.cluster.config.deployment.avro.prefix }}"
    {{- end }}
       >
    {{- if .Values.cluster.config.deployment.avro.properties }}
         <properties>
    {{- range $key, $val := .Values.cluster.config.deployment.avro.properties }}
         <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
         </properties>
    {{- end }}
       </avro>
    {{- end }}

    {{- if .Values.cluster.config.deployment.topics }}
       <topics
    {{- if .Values.cluster.config.deployment.topics.enabled }}
         enabled="{{ .Values.cluster.config.deployment.topics.enabled }}"
    {{- end }}
    {{- if .Values.cluster.config.deployment.topics.threadpool }}
             threadpool="{{ .Values.cluster.config.deployment.topics.threadpool }}"
    {{- end }}
         >
    {{- if .Values.cluster.config.deployment.topics.properties }}
         <properties>
    {{- range $key, $val := .Values.cluster.config.deployment.topics.properties }}
           <property name="{{ $key }}">{{ $val }}</property>
    {{- end }}
         </properties>
    {{- end }}
    {{- if .Values.cluster.config.deployment.topics.profiles }}
         <profiles>
    {{- range .Values.cluster.config.deployment.topics.profiles }}
    {{- if not .name }}
    {{- fail "name must be specified for topics profile." }}
    {{- end }}
           <profile name="{{ .name }}">
             <retention policy="{{ .retention.policy }}" limit="{{ .retention.limit }}" />
           </profile>
    {{- end }}
         </profiles>
    {{- end }}
       </topics>
    {{- end }}

    {{- if .Values.cluster.config.deployment.snmp.enabled }}
      <snmp enabled="{{ .Values.cluster.config.deployment.snmp.enabled }}"
        target="{{ .Values.cluster.config.deployment.snmp.target }}"
        authkey="{{ .Values.cluster.config.deployment.snmp.authkey }}"
        authprotocol="{{ .Values.cluster.config.deployment.snmp.authprotocol }}"
        community="{{ .Values.cluster.config.deployment.snmp.community }}"
        privacykey="{{ .Values.cluster.config.deployment.snmp.privacykey }}"
        privacyprotocol="{{ .Values.cluster.config.deployment.snmp.privacyprotocol }}"
        username="{{ .Values.cluster.config.deployment.snmp.username }}"/>
    {{- end }}

    {{- if .Values.cluster.config.deployment.ssl.enabled }}
      <ssl enabled="{{ .Values.cluster.config.deployment.ssl.enabled }}"
        external="{{ .Values.cluster.config.deployment.ssl.external }}"
        internal="{{ .Values.cluster.config.deployment.ssl.internal }}"
        dr="{{ .Values.cluster.config.deployment.ssl.dr }}">

    {{- if .Values.cluster.config.deployment.ssl.keystore.file }}
        <keystore path="/etc/voltdb/ssl/keystore.jks"
          password="{{ .Values.cluster.config.deployment.ssl.keystore.password }}"
        />
    {{- end }}

    {{- if .Values.cluster.config.deployment.ssl.truststore.file }}
        <truststore path="/etc/voltdb/ssl/truststore.jks"
          password="{{ .Values.cluster.config.deployment.ssl.truststore.password }}"
        />
    {{- end }}

      </ssl>
    {{- end }}

      <systemsettings>
        <elastic duration="{{ .Values.cluster.config.deployment.systemsettings.elastic.duration }}"
          throughput="{{ .Values.cluster.config.deployment.systemsettings.elastic.throughput }}"/>
        <flushinterval minimum="{{ .Values.cluster.config.deployment.systemsettings.flushinterval.minimum }}">
          <dr interval="{{ .Values.cluster.config.deployment.systemsettings.flushinterval.dr.interval }}"/>
          <export interval="{{ .Values.cluster.config.deployment.systemsettings.flushinterval.export.interval }}"/>
        </flushinterval>
        <procedure loginfo="{{ .Values.cluster.config.deployment.systemsettings.procedure.loginfo }}"/>
        <query timeout="{{ .Values.cluster.config.deployment.systemsettings.query.timeout }}"/>
        <resourcemonitor frequency="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.frequency }}">
          <memorylimit size="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.memorylimit.size }}"
            alert="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.memorylimit.alert }}"/>
          <disklimit>
            <feature name="commandlog"
              size="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlog.size }}"
              alert="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlog.alert }}"/>
            <feature name="commandlogsnapshot"
              size="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlogsnapshot.size }}"
              alert="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlogsnapshot.alert }}"/>
            <feature name="droverflow"
              size="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.droverflow.size }}"
              alert="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.droverflow.alert }}"/>
            <feature name="exportoverflow"
              size="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.exportoverflow.size }}"
              alert="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.exportoverflow.alert }}"/>
            <feature name="snapshots"
              size="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.snapshots.size }}"
              alert="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.snapshots.alert }}"/>
            <feature name="topicsdata"
              size="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.topicsdata.size }}"
              alert="{{ .Values.cluster.config.deployment.systemsettings.resourcemonitor.disklimit.topicsdata.alert }}"/>
          </disklimit>
        </resourcemonitor>
        <snapshot priority="{{ .Values.cluster.config.deployment.systemsettings.snapshot.priority }}"/>
        <temptables maxsize="{{ .Values.cluster.config.deployment.systemsettings.temptables.maxsize }}"/>
      </systemsettings>

    {{- if .Values.cluster.config.deployment.users }}
      <users>
    {{- range .Values.cluster.config.deployment.users }}
        <user name="{{ .name }}"
              password="{{ .password }}"
              roles="{{ .roles }}" />
    {{- end }}
    {{- if .Values.cluster.config.deployment.security.enabled }}
    {{- if eq .Values.cluster.config.deployment.security.provider "hash" }}
        <user name="{{ .Values.cluster.config.auth.username }}"
              password="{{ .Values.cluster.config.auth.password }}"
              roles="administrator" />
    {{- end }}
    {{- end }}
      </users>
    {{- end }}
    </deployment>
    {{- end }}
  {{- if .Values.cluster.config.licenseXMLFile }}
  license.xml:
  {{ toYaml .Values.cluster.config.licenseXMLFile | indent 4 }}
  {{- end }}
  {{- if .Values.cluster.config.log4jcfgFile }}
  log4j.xml:
  {{ toYaml .Values.cluster.config.log4jcfgFile | indent 4 }}
  {{- end }}
  {{- end }}
  {{- end }}
