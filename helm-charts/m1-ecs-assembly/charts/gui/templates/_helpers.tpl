{{- define "gui.name" -}}
{{- default .Chart.Name .Values.chart_name_override | trunc 63 | replace "_" "-" | trimSuffix "-" -}}
{{- end -}}

{{/* print namespace in format: "my/name/space/" (single slash only as suffix), or empty string if the value is empty */}}
{{- define "url.append_slash" -}}
{{- if . -}}
    {{.| trimAll "/"}}{{"/"}}
{{- end -}}
{{- end -}}

{{/*
Get the jboss service port number (httpPort/httpsPort)
*/}}
{{- define "gui.jboss-port" -}}
{{- if eq (include "gui.is-tls-enabled" .) "true" }}
{{- .Values.httpsPort }}
{{- else }}
{{- .Values.httpPort }}
{{- end -}}
{{- end -}}

{{/*
Get the jboss service protocol (HTTP/HTTPS)
*/}}
{{- define "gui.service-protocol" -}}
{{- if eq (include "gui.is-tls-enabled" .) "true" }}
{{- printf "https" }}
{{- else }}
{{- printf "http" }}
{{- end -}}
{{- end -}}

{{/*
Check if TLS is enabled by checking both global and microservice Helm values. If microservice is configured, it takes precedence.
*/}}
{{- define "gui.is-tls-enabled" -}}
{{ ternary .Values.tls.enabled .Values.global.tls.enabled (hasKey .Values.tls "enabled") }}
{{- end -}}