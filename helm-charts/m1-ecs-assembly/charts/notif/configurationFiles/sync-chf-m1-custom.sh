#!/bin/bash
DEFAULT_IFS=$IFS
IFS=$'\n'

CONFIG_FILE=/tmp/config/rsync.json
TMP_CONFIG_FILE=/tmp/original.json
HEALTH_CHECK_FILE=${RSYNC_CLIENT_LIVENESS_FILE}-check
if [ ! -f $CONFIG_FILE ]; then
    echo "No Configuration file present at $CONFIG_FILE, exiting..."
    exit 1
fi

function updateTmpFile() {
    cp $CONFIG_FILE $TMP_CONFIG_FILE
}

function markReady() {
    echo "ready" > $RSYNC_CLIENT_READINESS_FILE
    echo "Marking as ready"
}

function markUnready() {
    rm -f $RSYNC_CLIENT_READINESS_FILE
    echo "Marking as unready"
}

function markHealthy() {
    echo "healthy" > $RSYNC_CLIENT_LIVENESS_FILE
    rm -f $HEALTH_CHECK_FILE
    echo "Marking as healthy"
}

function markUnHealthy() {
    rm -f $RSYNC_CLIENT_LIVENESS_FILE
    echo "unhealthy" > $HEALTH_CHECK_FILE
    echo "Marking as unhealthy"
}

function checkHealthy() {
    if [ -f "$HEALTH_CHECK_FILE" ]; then
        echo "Rsync client is unhealthy"
        return 1
    fi
}


function sync() {

    echo "Running sync $(date)"
    #Parse our json, extracting sourcePath and destinationPath into a single TSV separated line
    for i in $(cat $CONFIG_FILE | jq -r '.[] | [.sourcePath, .destinationPath] | @tsv'); do
        IFS=$DEFAULT_IFS
        #Split our TSV line into 2
        read -ra my_array <<< "$i"

        #Run rsync
        echo "Syncing from ${my_array[0]} to ${my_array[1]}"
        mkdir -p $(dirname ${my_array[1]})
        RSYNC_CMD=`echo rsync -v $RSYNC_COMMAND_ARGS ${my_array[0]} ${my_array[1]}`
        echo $RSYNC_CMD
        eval $RSYNC_CMD

        if [ $? -ne 0 ]; then
            IFS=$'\n'
            echo "Could not sync ${my_array[0]} to ${my_array[1]}"
            if [ ! -z "$INIT_STEPS_ONLY" ]; then
                exit 2
            fi
            markUnHealthy
            return 2
        else
            IFS=$'\n'
        fi
        echo "Sync Success"
    done

    if [ ! -z "$POST_SYNC_SCRIPT" ]; then
        $POST_SYNC_SCRIPT
        if [ $? -ne 0 ]; then
            echo "Failure in post sync script $POST_SYNC_SCRIPT"
            markUnHealthy
            return 2
        fi
        echo "Post sync script Success"
    fi

    # After all the above has worked mark the config file as handled
    if [ -z "$INIT_STEPS_ONLY" ]; then
        updateTmpFile
        markHealthy
    fi
}

#Running as an init container
if [ ! -z "$INIT_STEPS_ONLY" ]; then
    sync
    exit 0
fi

function watch() {
    #Running as a normal container, so never exit
    WATCH_DIR=/tmp/config
    echo "Watching ${WATCH_DIR} for updates"

    updateTmpFile

    # Catch the restart from unhealthy
    checkHealthy
    if [ $? -eq 0 ]; then
        markHealthy
    fi
    markReady

    rsync -a $SRC_DIR $RSYNC_WRITEOUT_USER@$RSYNC_WRITEOUT_DEST_HOST:$RSYNC_WRITEOUT_DEST_DIR/$HOSTNAME
    while true; do
        diff -q $CONFIG_FILE $TMP_CONFIG_FILE 2>&1 > /dev/null
        if [ $? -eq 1 ]; then
            echo "Detected config file update"
            sync
        else
            checkHealthy
            if [ $? -ne 0 ]; then
                sync
            else
                echo "No updates detected $(date)"
            fi
        fi
        #/usr/bin/rsync -v -r --delete -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' /opt/deploy/SBA/chf/logs azureuser@10.162.0.66:/home/azureuser/rsyncd/logs/chf
        /usr/bin/rsync -v -r --delete -e 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' $SRC_DIR $RSYNC_WRITEOUT_USER@$RSYNC_WRITEOUT_DEST_HOST:$RSYNC_WRITEOUT_DEST_DIR/$HOSTNAME
        sleep $RSYNC_CLIENT_SYNC_CHECK_PERIOD_SECS
    done
}

watch