{{- $tlsEnabled := ternary .Values.tls.enabled .Values.global.tls.enabled (hasKey .Values.tls "enabled") }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "notif.name" . }}
  labels:
    app: {{ template "notif.name" . }}
spec:
  replicas: {{ .Values.helm.replicas }}
  selector:
    matchLabels:
      app: {{ template "notif.name" . }}
  strategy:
    rollingUpdate:
      maxSurge: {{ .Values.helm.strategy.rollingUpdate.maxSurge }}
      maxUnavailable: {{ .Values.helm.strategy.rollingUpdate.maxUnavailable }}
    type: {{ .Values.helm.strategy.type }}
  minReadySeconds: {{ .Values.helm.minReadySeconds }}
  template:
    metadata:
      annotations:
        checksum_configmap: {{  .Files.Get "jinja_context.yaml" | sha256sum }}
      labels:
        app: {{ template "notif.name" . }}
        ums-containers: cte
        rollme: "{{ .Values.restartVersion }}" 
    spec:
{{- if or .Values.serviceAccount.name .Values.global.serviceAccountName }}
      serviceAccountName: {{ coalesce .Values.serviceAccount.name .Values.global.serviceAccountName }}
{{- end }}
{{- /* M1-Custom: CDR External Directory */}}
{{if eq .Values.writeRsyncOut.enabled "false"}}
      securityContext:
        {{- toYaml .Values.securityContext | nindent 8 }}
{{ end }}
{{- /* M1-Custom: End */}}
      containers:
      - name: cte
        image: "{{ .Values.helm.image.registry }}{{ .Values.helm.image.repository }}:{{ .Values.helm.image.tag }}"
        imagePullPolicy: {{ .Values.helm.image.pull_policy }}
        env:
        - name: FW_KUBE_ENTRY_COR
          value: "{{ .Values.cte_name }}"
        - name: HOST_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        - name: POD_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        - name: NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: CLUSTER_DOMAIN
          value: "{{ .Values.global.kubernetes.clusterDomain }}"
{{- if $tlsEnabled }}
        - name: KEYSTORE_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Values.tls.keystorePasswordSecret.name }}
              key: {{ .Values.tls.keystorePasswordSecret.key }}
        - name: TLS_ENABLED
          value: "true"
{{- if or .Values.global.tls.truststore.file .Values.global.tls.truststore.secretName }}
        - name: TRUSTSTORE_PASSWORD
          valueFrom:
            secretKeyRef:
              {{- if .Values.global.tls.truststore.file }}
              name: {{ template "notif.name" . }}-truststore
              {{- else }}
              name: {{ .Values.global.tls.truststore.secretName }}
              {{- end }}
              key: password
{{- end }}
{{- end }}
        livenessProbe:
          exec:
            command:
            - bash
            - -c
            - {{ .Values.helm.livenessProbe.exec.command }}
          initialDelaySeconds: {{ .Values.helm.livenessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.helm.livenessProbe.periodSeconds }}
          timeoutSeconds: {{ .Values.helm.livenessProbe.timeoutSeconds }}
          successThreshold: {{ .Values.helm.livenessProbe.successThreshold }}
          failureThreshold: {{ .Values.helm.livenessProbe.failureThreshold }}
        readinessProbe:
          exec:
            command:
            - bash
            - -c
            - {{ .Values.helm.readinessProbe.exec.command }}
          initialDelaySeconds: {{ .Values.helm.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.helm.readinessProbe.periodSeconds }}
          timeoutSeconds: {{ .Values.helm.readinessProbe.timeoutSeconds }}
          successThreshold: {{ .Values.helm.readinessProbe.successThreshold }}
          failureThreshold: {{ .Values.helm.readinessProbe.failureThreshold }}
        ports: 
        - name: cte-link
          containerPort: {{ .Values.context.cte_link.port }}
          protocol: TCP
        - name: notif-server
          containerPort: {{ .Values.context.notification_server.port }}
          protocol: TCP
        volumeMounts:
        - name: config-volume
          mountPath: /opt/forge/notif/home/config/parameters/kube-context

{{ range $key, $value := .Values.volumeMounts }}
        - mountPath: {{ $value }}
          name: {{ $key }}
{{ end }}
{{- if $tlsEnabled }}
        - name: tls-certificates-volume
          mountPath: {{ .Values.tls.mountPath }}
          readOnly: true
{{- if or .Values.global.tls.truststore.file .Values.global.tls.truststore.secretName }}
        - name: tls-external-truststore
          mountPath: "{{ .Values.tls.mountPath }}/../truststore"
{{- end }}
{{- end }}
{{if eq .Values.installRsync "true"}}
        - name: rsync-offer-catalog-data-volume
          mountPath: "/opt/forge/notif/home/config/runtime_config/business_rules/offercatalog/OCRuntime/CTE_Notif/config/"
        - name: rsync-tod-data-volume
          mountPath: "/opt/forge/notif/home/config/runtime_config/business_rules/timeofday/config"
        - name: rsync-notif-catalog-data-volume
          mountPath: "/opt/forge/notif/home/config/runtime_config/NotificationServer" 
        - name: rsync-stats-volume
          mountPath: "/opt/forge/notif/home/config/runtime_config/framework/Statistics_Management"
{{ end }}
        - name: ro-storage
          mountPath: "/usr/share/sftp"
        resources:
          # Commented line
{{ toYaml .Values.helm.resources | indent 11 }}
{{if eq .Values.installFilebeat "true"}}

      - name: {{ .Values.filebeat.name }}
        image: "{{ .Values.filebeat.image.registry }}{{ .Values.filebeat.image.repository }}:{{ .Values.filebeat.image.tag }}"
        imagePullPolicy: {{ .Values.filebeat.image.pullPolicy }}
        envFrom:
        - configMapRef:
            name: {{ template "notif.name" . }}-filebeat
        env:
        - name: NODE_IP
          valueFrom:
            fieldRef:
              fieldPath: status.hostIP
        - name: NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: POD_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: K8_LABEL_RELEASE
          valueFrom:
            fieldRef:
              fieldPath: metadata.labels['release']
        - name: K8_LABEL_APP
          valueFrom:
            fieldRef:
              fieldPath: metadata.labels['app']
{{- if .Values.filebeat.extraEnvs }}
{{ toYaml .Values.filebeat.extraEnvs | indent 8 }}
{{- end }}
        volumeMounts:

          - name: filebeat-app-config
            mountPath: /usr/share/filebeat/filebeat.yml
            subPath: filebeat.yml

{{ range $key, $value := .Values.volumeMounts }}
          - mountPath: {{ $value }}
            name: {{ $key }}
            readOnly: true
{{ end }}
        ports:
          - name: http
            containerPort: {{ .Values.filebeat.containerPort }}
            protocol: TCP
        resources:
{{ toYaml .Values.filebeat.resources | indent 11 }}

{{end}}

{{if eq .Values.housekeeping.enabled true}}
      - name: {{ index .Values "housekeeping" "name" }}
        image: "{{ index .Values "housekeeping" "image" "registry" }}/{{ index .Values "housekeeping" "image" "repository" }}:{{ index .Values "housekeeping" "image" "tag" }}"
        imagePullPolicy: {{ index .Values "housekeeping" "image" "pullPolicy" }}
        volumeMounts:
          - name: {{ .Chart.Name }}-housekeeping-logrotate-volume
            mountPath: /home/openet/logrotate/configFiles/

          - name: {{ .Chart.Name }}-housekeeping-cronjob-volume
            mountPath: /home/openet/logrotate/cronjobs/cronjob
            subPath: cronjob
{{ range $key, $value := .Values.housekeeping.volumeMounts }}
          - mountPath: {{ $value }}
            name: {{ $key }}
            readOnly: false
{{ end }}
{{ range $key, $value := .Values.volumeMounts }}
          - mountPath: {{ $value }}
            name: {{ $key }}
{{ end }}
        resources:
{{ toYaml .Values.housekeeping.resources | indent 11 }}
{{ end }}

{{if eq .Values.installRsync "true"}}
      - name: {{ .Values.rsync.name }}
        image: {{ .Values.rsync.image.registry }}/{{ .Values.rsync.image.repository }}:{{ .Values.rsync.image.tag }}
        imagePullPolicy: {{ .Values.rsync.image.pullPolicy }}
        env:
        - name: RSYNC_COMMAND_ARGS
          value: {{ .Values.rsync.commandArgs }}  
        - name: RSYNC_CLIENT_LIVENESS_FILE
          value: {{ .Values.rsync.livenessProbe.healthFile }}  
        - name: RSYNC_CLIENT_READINESS_FILE
          value: {{ .Values.rsync.readinessProbe.healthFile }}  
        - name: RSYNC_CLIENT_SYNC_CHECK_PERIOD_SECS
          value: {{ .Values.rsync.syncCheckPeriodSecs | quote }}
        - name: POST_SYNC_SCRIPT
          value: "/usr/bin/post-sync.sh"
        {{- /* M1-Custom: CDR External Directory */}}
        {{ if eq .Values.writeRsyncOut.enabled "true" }}
        - name: SRC_DIR
          value: {{ .Values.writeRsyncOut.source_dir }}
        - name: RSYNC_WRITEOUT_USER
          value: {{ .Values.writeRsyncOut.user }}
        - name: RSYNC_WRITEOUT_DEST_HOST
          value: {{ .Values.writeRsyncOut.dest_host }}
        - name: RSYNC_WRITEOUT_DEST_DIR
          value: {{ .Values.writeRsyncOut.dest_dir }}
        {{ end }}
      {{- /* M1-Custom: End */}}
        livenessProbe:
          exec:
            command:                                       
            - cat
            - {{ .Values.rsync.livenessProbe.healthFile }}
          initialDelaySeconds: {{ .Values.rsync.livenessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.rsync.livenessProbe.periodSeconds }}
          timeoutSeconds: {{ .Values.rsync.livenessProbe.timeoutSeconds }}
          successThreshold: {{ .Values.rsync.livenessProbe.successThreshold }}
          failureThreshold: {{ .Values.rsync.livenessProbe.failureThreshold }}
        readinessProbe:
          exec:
            command:
            - cat
            - {{ .Values.rsync.readinessProbe.healthFile }}
          initialDelaySeconds: {{ .Values.rsync.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.rsync.readinessProbe.periodSeconds }}
          timeoutSeconds: {{ .Values.rsync.readinessProbe.timeoutSeconds }}
          successThreshold: {{ .Values.rsync.readinessProbe.successThreshold }}
          failureThreshold: {{ .Values.rsync.readinessProbe.failureThreshold }}
        resources:
{{ toYaml .Values.rsync.resources | indent 11 }}
        volumeMounts:
          - name: rsync-config-volume
            mountPath: /tmp/config
          - name: rsync-post-sync-script-volume
            mountPath: "/usr/bin/post-sync.sh"
            readOnly: true
            subPath: post-sync.sh
          - name: rsyncd-credentials
            mountPath: "/tmp/rsync"
            readOnly: true
          - name: rsync-offer-catalog-data-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/business_rules/offercatalog/OCRuntime/CTE_Notif/config"
          - name: rsync-tod-data-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/business_rules/timeofday/config"
          - name: rsync-notif-catalog-data-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/NotificationServer"
          - name: rsync-stats-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/framework/Statistics_Management"
          - name: rsync-health-volume
            mountPath: "/mnt/health"
          - name: rsync-sync-data-volume
            mountPath: "/source_data"
           {{- /* M1-Custom: CDR External Directory */}}
            {{ if eq .Values.writeRsyncOut.enabled "true" }}
          - name: sync-chf-custom-script-volume
            mountPath: "/usr/bin/sync.sh"
            readOnly: true
            subPath: sync-notif.sh
          - name: rsync-sshkey-volume
            mountPath: "/root/.ssh/id_rsa"
            readOnly: true
            subPath: id_rsa
          - name: fw-base-dir
            mountPath: {{ .Values.fw_base_dir }}
            readOnly: true
          - name: ro-storage
            mountPath: "/usr/share/sftp"
        {{ end }}
       {{- /* M1-Custom: End */}}
      initContainers:
      - name: rsync-init
        image: {{ .Values.rsync.image.registry }}/{{ .Values.rsync.image.repository }}:{{ .Values.rsync.image.tag }}
        imagePullPolicy: {{ .Values.rsync.image.pullPolicy }}
        env:
        - name: INIT_STEPS_ONLY
          value: "true"
        - name: RSYNC_COMMAND_ARGS
          value: {{ .Values.rsync.commandArgs }}  
        volumeMounts:
          - name: rsync-config-volume
            mountPath: /tmp/config
          - name: rsyncd-credentials
            mountPath: "/tmp/rsync"
            readOnly: true
          - name: rsync-offer-catalog-data-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/business_rules/offercatalog/OCRuntime/CTE_Notif/config"
          - name: rsync-tod-data-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/business_rules/timeofday/config"
          - name: rsync-notif-catalog-data-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/NotificationServer"
          - name: rsync-stats-volume
            mountPath: "/opt/forge/notif/home/config/runtime_config/framework/Statistics_Management"
        resources:
{{ toYaml .Values.rsync.resources | indent 11 }}
{{end}}
      volumes:
      - name: config-volume
        configMap:
          name: notif-config-map
      - name: filebeat-app-config
        configMap:
          name: {{ .Values.filebeat.appFilebeatConfigMap }}
{{ if eq .Values.housekeeping.enabled true }}
      - name: {{ .Chart.Name }}-housekeeping-logrotate-volume
        configMap:
          name: {{ template "notif.name" . }}-housekeeping-logrotate
      - name: {{ .Chart.Name }}-housekeeping-cronjob-volume
        configMap:
{{ if and .Values.housekeeping (not .Values.global.housekeeping) }}
          name: {{ template "notif.name" . }}-housekeeping-cronjob
{{ else }}
          name: housekeeping-global-cronjob
{{ end }}
{{ end }}
{{ range $key, $value := .Values.housekeeping.volumeMounts }}
{{ if not (hasKey $.Values.volumeMounts $key) }}
      - name: {{ $key }}
        emptyDir: {}
{{ end }}
{{ end }}
{{if eq .Values.installRsync "true"}}
      - name: rsync-config-volume
        configMap:
          name: {{ template "notif.name" . }}-rsync-configuration
      - name: rsync-post-sync-script-volume
        configMap:
          defaultMode: 0755
          name: {{ template "notif.name" . }}-rsync-post-sync-configuration
      {{- /* M1-Custom: CDR External Directory */}}
      {{ if eq .Values.writeRsyncOut.enabled "true" }}
      - name: rsync-sshkey-volume
        configMap:
          defaultMode: 0600
          name: {{ template "notif.name" . }}-writersyncoutsshkey
      - name: sync-chf-custom-script-volume
        configMap:
          defaultMode: 0755
          name: {{ template "notif.name" . }}-sync-script
      - name: fw-base-dir
        emptyDir: {}
      {{ end }}
     {{- /* M1-Custom: End */}}
      - name: rsyncd-credentials
        secret:
          secretName: rsyncd-credentials
          defaultMode: 0660
      - name: rsync-offer-catalog-data-volume
        emptyDir: {}
      - name: rsync-tod-data-volume
        emptyDir: {}
      - name: rsync-notif-catalog-data-volume
        emptyDir: {}
      - name: rsync-stats-volume
        emptyDir: {}
      - name: rsync-health-volume
        emptyDir: {}
      - name: rsync-sync-data-volume
        emptyDir: {}

{{end}}
{{- if $tlsEnabled }}
      - name: tls-certificates-volume
        secret:
          secretName: {{ .Values.tls.certificateSecretName }}
{{- if or .Values.global.tls.truststore.file .Values.global.tls.truststore.secretName }}
      - name: tls-external-truststore
        secret:
          {{- if .Values.global.tls.truststore.file }}
          secretName: {{ template "notif.name" . }}-truststore
          {{- else }}
          secretName: {{ .Values.global.tls.truststore.secretName }}
          {{- end }}
{{- end }}
{{- end }}
{{ range $key, $value := .Values.volumeMounts }}
      - name: {{ $key }}
        emptyDir: {}
{{ end }}
      - name: ro-storage
        persistentVolumeClaim:
          claimName: ro-storage
{{- with .Values.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}
      affinity:
{{- range $key, $value := .Values.affinity }}
    {{- with . }}
{{ toYaml $key | trim | indent 8 }}:
{{ toYaml $value | trim | indent 10 }}
    {{- end }}
{{- end }}
{{- if kindIs "invalid" .Values.affinity.podAntiAffinity }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - {{ template "notif.name" . }}
              topologyKey: kubernetes.io/hostname
{{- end}}
{{- with .Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
{{- end }}
