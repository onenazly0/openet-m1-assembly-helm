{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "chart.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "chart.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Check if TLS is enabled by checking both global and microservice Helm values. If microservice is configured, it takes precedence.
*/}}
{{- define "chart.is-tls-enabled" -}}
{{ ternary .Values.tls.enabled .Values.global.tls.enabled (hasKey .Values.tls "enabled") }}
{{- end -}}

{{/*
Get the service protocol (HTTP/HTTPS)
*/}}
{{- define "chart.service-protocol" -}}
{{- if eq (include "chart.is-tls-enabled" .) "true" }}
{{- printf "https" }}
{{- else }}
{{- printf "http" }}
{{- end -}}
{{- end -}}

{{/*
The exposed service port can be overridden, but it probably does not make sense to do so.
*/}}
{{- define "chart.service-port" -}}
{{- if eq (include "chart.is-tls-enabled" .) "true" }}
{{- .Values.service.port | default 8443 }}
{{- else -}}
{{- .Values.service.port | default 8080 }}
{{- end -}}
{{- end -}}