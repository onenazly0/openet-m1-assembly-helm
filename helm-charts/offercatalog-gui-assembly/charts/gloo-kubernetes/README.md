# Gloo Helm Chart

This chart is a wrapper on Gloo to added Openet specific setup.

## Configuration

### Gloo Configuration

```yaml
gloo:
  settings:
    singleNamespace: true
    integrations:
      consul:
        httpAddress: consul-consul-server.consul.svc.cluster.local:8500
        token: c00d5ce4-2a5b-48d9-abbc-3e055e3c7de8
```

Consul address defaults to `consul-consul-server.consul.svc.cluster.local:8500`. You only need to change it if different in your cluster.
The Consul `token` needs to be provided only if you have ACLs enabled in Consul.

### Post Install Configuration

```yaml
postInstall:
  activeDeadlineSeconds: 240
  backoffLimit: 0
  image:
    registry: dependencies.openet.com:5000/alpine
    name: k8s
    tag: 1.18.2   
```

### Global Configuration

The following is an example of Global configuration.

```yaml
global:
  image:
    registry: dependencies.openet.com:5000/solo-io
  nfType:
  ingress:
    externalIPs:
      - 172.24.0.13
      - 172.18.0.6
    protocols:
      http:
        enabled: true
      https:
        enabled: false
        certificate:
          commonName: "My Ingress"
          additionalSANs:
            ipAddresses:
              - "127.0.0.1"
            dnsNames:
              - "SomeHostName"
          duration: 2160h # 90d
          renewBefore: 360h # 15d
          issuer: openet-ca
          subject:
            organizations:
              - Openet
              - CustomerName
```

With this configuration we can enabled one or both of HTTP and HTTPS.

#### HTTPS support

For HTTPS, a cert-manager Certificate resource will be created.
This will create a secret that must be used by any `VirtualService`s you create.

The default values will suffice to enabled TLS. However, a certificate with SAN entries is considered invalid. This means you need at least one IP addresss or one DNS Name.

The `externalIPs`, if present, will automatically be added to the SAN list of IP addresses. You may add additional ones if you require.

If the `commonName` is not set, one based on the release name will be used.

##### Certificate's private key algorithm

By default, `RSA` is being used as the private key algorithm for the certificate, it can be changed via `global.ingress.protocols.https.certificate.privateKey`.

Below is the possible combination of the key algorithm, encoding, and key size.

| Algorithm | Encoding | Size |
| --- | --- | --- |
| RSA | PKCS1, PKCS8 | Minimum 2048, Maximum 8192 |
| ECDSA | PKCS1, PKCS8 | 256, 384, 521 |

##### TLS/SSL Configuration

The TLS configuration on the listener proxy could be configured, such as: 
- The support of HTTP/2, HTTP/1.1
- Minimum TLS version and cipher suites to support
- SNI domains of the TLS connections

Full documentation of the parameters are available in [Gloo SSL configuration](https://docs.solo.io/gloo/latest/reference/api/github.com/solo-io/gloo/projects/gloo/api/v1/ssl.proto.sk).

###### Named template for TLS configuration in VirtualService
A Helm named template (`virtualService.sslConfig`) is provided by this Chart to auto configure the TLS configuration.

Do include this named template as below in the `VirtualService` manifest, within the `.Values.global.ingress.protocols` range's block.
The `$value` below is reference to one of the `.Values.global.ingress.protocols`.

```yaml
spec:
  {{- if eq $key "https" }}
{{ include "virtualService.sslConfig" (dict "root" $ "certificate" $value.certificate "sslConfig" $value.sslConfig) | indent 2 }}
  {{- end }}
```

The following is the default TLS configuration if the HTTPS is enabled. The certificate secret and the default minimum
TLS version are configured.
```yaml
global:
  ingress:
    protocols:
      https:
        sslConfig:
          secretRef:
            name: "{{ .certificate.secretName }}"
            namespace: {{ .Release.Namespace }}
          parameters:
            minimumProtocolVersion: TLSv1_2
```

To override the default TLS configuration, configure the `sslConfig` under `global.ingress.protocols.https` values.
