# VoltDB Operator Helm Chart

VoltDB Operator (https://github.com/voltdb/voltdb-operator) takes care of creating, deleting, updating and managing VoltDB.

## Prerequisites
  - Helm 3.1+
  - Access to the VoltDB Enterprise Docker Images
  - Permissions to create [the required Kubernetes objects](#rbac-permissions)
  - THP (transparent-hugepages) disabled, provided as a [separate VoltDB Helm chart](../transparent-hugepages/README.md)
  - Kubernetes hosts configured correctly, see requirements [here](https://docs.voltdb.com/AdminGuide/ServerConfigChap.php). Note that not all are applicable in a container environment.

```
kubectl create secret docker-registry dockerio-registry --docker-username=<USERNAME> --docker-password=<PASSWORD> --docker-email=<EMAIL>
```
> Note: Name is configurable using the Helm value `global.imagePullSecrets`

Add the Helm repo if you do not already have it:

```bash
$ helm repo add voltdb https://voltdb-kubernetes-charts.storage.googleapis.com
```

## Installing the Chart

Get the latest information about charts from the chart repositories.

```bash
$ helm repo update
```

To install the chart:

```bash
$ helm install myrelease voltdb/voltdb \
    --set global.image.credentials.username=dockerusername \
    --set global.image.credentials.password=dockerpassword \
```

Adding license and other configuration files:

```bash
$ helm install myrelease voltdb/voltdb [--values myvalues.yaml] \
    --set global.image.credentials.username=dockerusername \
    --set global.image.credentials.password=dockerpassword \
    --set-file cluster.config.licenseXMLFile='license.xml' \
    --set-file cluster.config.schemas.schema123_sql='schemas/schema123.sql' \
    --set-file cluster.config.schemas.schema456_sql='schemas/schema456.sql' \
    --set-file cluster.config.classes.classABC_jar='classes/classABC.jar' \
    --set-file cluster.config.classes.classXYZ_jar='classes/classXYZ.jar'
```

> Note: Use `[--set parameter] [--set-file file.ext] [--values myvalues.yaml]` for additional configuration, see options below

Installing a specific version of the chart:

```bash
$ helm install myrelease voltdb/voltdb --version 1.2.3...
```

## Configuration

The following tables list the configurable parameters of the chart and their default values.

#### cluster.*

The purpose of this section is to specify VoltDB cluster settings.

For full deployment config structure, see the [official VoltDB documentation](https://docs.voltdb.com/UsingVoltDB/ConfigStructure.php).

The following tables list the configurable parameters for the `cluster` option:

| Parameter                                                                                     | Description                                                                                                                                                                                  | Default                                                                                                     |
|-----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
| `cluster.enabled`                                                                             | Create VoltDB Cluster                                                                                                                                                                        | `true`                                                                                                      |
| `cluster.serviceAccount.create`                                                               | If true, create & use service account for VoltDB cluster node containers                                                                                                                     | `true`                                                                                                      |
| `cluster.serviceAccount.name`                                                                 | If not set and create is true, a name is generated using the fullname template                                                                                                               | `""`                                                                                                        |
| `cluster.clusterSpec.replicas`                                                                | Pod (VoltDB Node) replica count, scaling to 0 will shutdown the cluster gracefully                                                                                                           | `3`                                                                                                         |
| `cluster.clusterSpec.maxPodUnavailable`                                                       | Maximum pods unavailable in Pod Disruption Budget                                                                                                                                            | `kfactor`                                                                                                   |
| `cluster.clusterSpec.maintenanceMode`                                                         | VoltDB Cluster maintenance mode (pause all nodes)                                                                                                                                            | `false`                                                                                                     |
| `cluster.clusterSpec.takeSnapshotOnShutdown`                                                  | Takes a snapshot when cluster is shut down by scaling to 0. One of: NoCommandLogging (default), Always, Never. NoCommandLogging means 'snapshot only if command logging is disabled'.        | `""`                                                                                                        |
| `cluster.clusterSpec.initForce`                                                               | Always init --force on VoltDB node start/restart. WARNING: This will destroy VoltDB data on PVCs except snapshots.                                                                           | `false`                                                                                                     |
| `cluster.clusterSpec.deletePVC`                                                               | Delete and cleanup generated PVCs when VoltDBCluster is deleted, requires finalizers to be enabled (on by default)                                                                           | `false`                                                                                                     |
| `cluster.clusterSpec.allowRestartDuringUpdate`                                                | Allow VoltDB cluster restarts if necessary to apply user-requested configuration changes. May include automatic save and restore of database.                                                | `false`                                                                                                     |
| `cluster.clusterSpec.stoppedNodes`                                                            | User-specified list of stopped nodes based on StatefulSet # (e.g. `[ 2, 3 ]`)                                                                                                                | `[]`                                                                                                        |
| `cluster.clusterSpec.additionalXDCRReadiness`                                                 | Add additional readiness checks using XDCR to ensure both clusters are healthy (WARNING: May cause app downtime)                                                                             | `false`                                                                                                     |
| `cluster.clusterSpec.persistentVolume.size`                                                   | Persistent Volume size per Pod (VoltDB Node)                                                                                                                                                 | `1Gi`                                                                                                       |
| `cluster.clusterSpec.persistentVolume.storageClassName`                                       | Storage Class name to use, otherwise use default                                                                                                                                             | `""`                                                                                                        |
| `cluster.clusterSpec.persistentVolume.hostpath.enabled`                                       | Use HostPath volume for local storage of VoltDB. This node storage is often ephemeral and will not use PVC storage classes if enabled.                                                       | `false`                                                                                                     |
| `cluster.clusterSpec.persistentVolume.hostpath.path`                                          | HostPath mount point, defaults to `/data/voltdb/` if not specified.                                                                                                                          | `""`                                                                                                        |
| `cluster.clusterSpec.ssl.certificateFile`                                                     | PEM encoded certificate chain used by the VoltDB operator when SSL/TLS is enabled                                                                                                            | `""`                                                                                                        |
| `cluster.clusterSpec.ssl.insecure`                                                            | If true, skip certificate verification by the VoltDB operator when SSL/TLS is enabled                                                                                                        | `false`                                                                                                     |
| `cluster.clusterSpec.storageConfigs`                                                          | Optional storage configs for provisioning additional persistent volume claims automatically                                                                                                  | `[]`                                                                                                        |
| `cluster.clusterSpec.additionalVolumes`                                                       | Additional list of volumes that can be mounted by node containers                                                                                                                            | `[]`                                                                                                        |
| `cluster.clusterSpec.additionalVolumeMounts`                                                  | Pod volumes to mount into the container's filesystem, cannot be modified once set                                                                                                            | `[]`                                                                                                        |
| `cluster.clusterSpec.image.registry`                                                          | Image registry                                                                                                                                                                               | `docker.io`                                                                                                 |
| `cluster.clusterSpec.image.repository`                                                        | Image repository                                                                                                                                                                             | `voltdb/voltdb-enterprise`                                                                                  |
| `cluster.clusterSpec.image.tag`                                                               | Image tag                                                                                                                                                                                    | `10.0.0`                                                                                                    |
| `cluster.clusterSpec.image.pullPolicy`                                                        | Image pull policy                                                                                                                                                                            | `Always`                                                                                                    |
| `cluster.clusterSpec.additionalStartArgs`                                                     | Additional VoltDB start command args for the pod container                                                                                                                                   | `[]`                                                                                                        |
| `cluster.clusterSpec.priorityClassName`                                                       | Pod priority defined by an existing PriorityClass                                                                                                                                            | `""`                                                                                                        |
| `cluster.clusterSpec.additionalAnnotations`                                                   | Additional custom Pod annotations                                                                                                                                                            | `{}`                                                                                                        |
| `cluster.clusterSpec.additionalLabels`                                                        | Additional custom Pod labels                                                                                                                                                                 | `{}`                                                                                                        |
| `cluster.clusterSpec.resources`                                                               | CPU/Memory resource requests/limits                                                                                                                                                          | `{}`                                                                                                        |
| `cluster.clusterSpec.nodeSelector`                                                            | Node labels for pod assignment                                                                                                                                                               | `{}`                                                                                                        |
| `cluster.clusterSpec.tolerations`                                                             | Pod tolerations for Node assignment                                                                                                                                                          | `[]`                                                                                                        |
| `cluster.clusterSpec.affinity`                                                                | Node affinity                                                                                                                                                                                | `{}`                                                                                                        |
| `cluster.clusterSpec.podSecurityContext`                                                      | Pod security context                                                                                                                                                                         | `{"runAsNonRoot":true,"runAsUser":1001,"fsGroup":1001}`                                                     |
| `cluster.clusterSpec.securityContext`                                                         | Container security context. WARNING: Changing user or group ID may prevent VoltDB from operating.                                                                                            | `{"privileged":false,"runAsNonRoot":true,"runAsUser":1001,"runAsGroup":1001,"readOnlyRootFilesystem":true}` |
| `cluster.clusterSpec.clusterInit.initSecretRefName`                                           | Name of pre-created Kubernetes secret containining init configuration (deployment.xml, license.xml and log4j.xml), ignores init configuration if set                                         | `""`                                                                                                        |
| `cluster.clusterSpec.clusterInit.schemaConfigMapRefName`                                      | Name of pre-created Kubernetes configmap containining schema configuration                                                                                                                   | `""`                                                                                                        |
| `cluster.clusterSpec.clusterInit.classesConfigMapRefName`                                     | Name of pre-created Kubernetes configmap containining schema configuration                                                                                                                   | `""`                                                                                                        |
| `cluster.clusterSpec.podTerminationGracePeriodSeconds`                                        | Duration in seconds the Pod needs to terminate gracefully. Defaults to 30 seconds if not specified.                                                                                          | `30`                                                                                                        |
| `cluster.clusterSpec.livenessProbe.enabled`                                                   | Enable/disable livenessProbe                                                                                                                                                                 | `true`                                                                                                      |
| `cluster.clusterSpec.livenessProbe.initialDelaySeconds`                                       | Delay before liveness probe is initiated                                                                                                                                                     | `20`                                                                                                        |
| `cluster.clusterSpec.livenessProbe.periodSeconds`                                             | How often to perform the probe                                                                                                                                                               | `10`                                                                                                        |
| `cluster.clusterSpec.livenessProbe.timeoutSeconds`                                            | When the probe times out                                                                                                                                                                     | `1`                                                                                                         |
| `cluster.clusterSpec.livenessProbe.failureThreshold`                                          | Minimum consecutive failures for the probe                                                                                                                                                   | `10`                                                                                                        |
| `cluster.clusterSpec.livenessProbe.successThreshold`                                          | Minimum consecutive successes for the probe                                                                                                                                                  | `1`                                                                                                         |
| `cluster.clusterSpec.readinessProbe.enabled`                                                  | Enable/disable readinessProbe                                                                                                                                                                | `true`                                                                                                      |
| `cluster.clusterSpec.readinessProbe.initialDelaySeconds`                                      | Delay before readiness probe is initiated                                                                                                                                                    | `30`                                                                                                        |
| `cluster.clusterSpec.readinessProbe.periodSeconds`                                            | How often to perform the probe                                                                                                                                                               | `17`                                                                                                        |
| `cluster.clusterSpec.readinessProbe.timeoutSeconds`                                           | When the probe times out                                                                                                                                                                     | `2`                                                                                                         |
| `cluster.clusterSpec.readinessProbe.failureThreshold`                                         | Minimum consecutive failures for the probe                                                                                                                                                   | `6`                                                                                                         |
| `cluster.clusterSpec.readinessProbe.successThreshold`                                         | Minimum consecutive successes for the probe                                                                                                                                                  | `1`                                                                                                         |
| `cluster.clusterSpec.startupProbe.enabled`                                                    | Enable/disable startupProbe, feature flag must also be enabled at a cluster level (enabled by default in 1.18)                                                                               | `true`                                                                                                      |
| `cluster.clusterSpec.startupProbe.initialDelaySeconds`                                        | Delay before startup probe is initiated                                                                                                                                                      | `45`                                                                                                        |
| `cluster.clusterSpec.startupProbe.periodSeconds`                                              | How often to perform the probe                                                                                                                                                               | `10`                                                                                                        |
| `cluster.clusterSpec.startupProbe.timeoutSeconds`                                             | When the probe times out                                                                                                                                                                     | `1`                                                                                                         |
| `cluster.clusterSpec.startupProbe.failureThreshold`                                           | Minimum consecutive failures for the probe                                                                                                                                                   | `18`                                                                                                        |
| `cluster.clusterSpec.startupProbe.successThreshold`                                           | Minimum consecutive successes for the probe                                                                                                                                                  | `1`                                                                                                         |
| `cluster.clusterSpec.env.VOLTDB_OPTS`                                                         | VoltDB cluster additional java runtime options (VOLTDB_OPTS)                                                                                                                                 | `""`                                                                                                        |
| `cluster.clusterSpec.env.VOLTDB_HEAPMAX`                                                      | VoltDB cluster heap size, integer number of megabytes (VOLTDB_HEAPMAX)                                                                                                                       | `""`                                                                                                        |
| `cluster.clusterSpec.env.VOLTDB_HEAPCOMMIT`                                                   | Commit VoltDB cluster heap at startup, true/false (VOLTDB_HEAPCOMMIT)                                                                                                                        | `""`                                                                                                        |
| `cluster.clusterSpec.env.VOLTDB_K8S_LOG_CONFIG`                                               | VoltDB log4jcfg file path                                                                                                                                                                    | `""`                                                                                                        |
| `cluster.clusterSpec.customEnv`                                                               | Key-value map of additional envvars to set in all VoltDB node containers                                                                                                                     | `{}`                                                                                                        |
| `cluster.serviceSpec.type`                                                                    | VoltDB service type (options ClusterIP, NodePort, and LoadBalancer)                                                                                                                          | `ClusterIP`                                                                                                 |
| `cluster.serviceSpec.externalTrafficPolicy`                                                   | VoltDB service external traffic policy (options Cluster, Local)                                                                                                                              | `Cluster`                                                                                                   |
| `cluster.serviceSpec.vmcPort`                                                                 | VoltDB Management Center web interface Service port                                                                                                                                          | `8080`                                                                                                      |
| `cluster.serviceSpec.vmcNodePort`                                                             | Port to expose VoltDB Management Center service on each node, type NodePort only                                                                                                             | `31080`                                                                                                     |
| `cluster.serviceSpec.vmcSecurePort`                                                           | VoltDB Management Center secure web interface Service port                                                                                                                                   | `8443`                                                                                                      |
| `cluster.serviceSpec.vmcSecureNodePort`                                                       | Port to expose VoltDB Management Center secure service on each node, type NodePort only                                                                                                      | `31443`                                                                                                     |
| `cluster.serviceSpec.adminPortEnabled`                                                        | Enable exposing admin port with the VoltDB Service                                                                                                                                           | `true`                                                                                                      |
| `cluster.serviceSpec.adminPort`                                                               | VoltDB Admin exposed Service port                                                                                                                                                            | `21211`                                                                                                     |
| `cluster.serviceSpec.adminNodePort`                                                           | Port to expose VoltDB Admin service on each node, type NodePort only                                                                                                                         | `31211`                                                                                                     |
| `cluster.serviceSpec.clientPortEnabled`                                                       | Enable exposing client port with the VoltDB Service                                                                                                                                          | `true`                                                                                                      |
| `cluster.serviceSpec.clientPort`                                                              | VoltDB Client exposed service port                                                                                                                                                           | `21212`                                                                                                     |
| `cluster.serviceSpec.clientNodePort`                                                          | Port to expose VoltDB Client service on each node, type NodePort only                                                                                                                        | `31212`                                                                                                     |
| `cluster.serviceSpec.loadBalancerIP`                                                          | VoltDB Load Balancer IP                                                                                                                                                                      | `""`                                                                                                        |
| `cluster.serviceSpec.loadBalancerSourceRanges`                                                | VoltDB Load Balancer Source Ranges                                                                                                                                                           | `[]`                                                                                                        |
| `cluster.serviceSpec.externalIPs`                                                             | List of IP addresses at which the VoltDB service is available                                                                                                                                | `[]`                                                                                                        |
| `cluster.serviceSpec.http.sessionAffinity`                                                    | SessionAffinity override for the HTTP service                                                                                                                                                | `ClientIP`                                                                                                  |
| `cluster.serviceSpec.http.sessionAffinityConfig.clientIP.timeoutSeconds`                      | Timeout override for http.sessionAffinity=ClientIP                                                                                                                                           | `10800`                                                                                                     |
| `cluster.serviceSpec.dr.type`                                                                 | VoltDB DR service type, valid options are ClusterIP (default), LoadBalancer, or NodePort                                                                                                     | `""`                                                                                                        |
| `cluster.serviceSpec.dr.annotations`                                                          | Additional custom Service annotations                                                                                                                                                        | `{}`                                                                                                        |
| `cluster.serviceSpec.dr.externalTrafficPolicy`                                                | VoltDB DR service external traffic policy                                                                                                                                                    | `""`                                                                                                        |
| `cluster.serviceSpec.dr.replicationPort`                                                      | VoltDB DR replication exposed Service port                                                                                                                                                   | `5555`                                                                                                      |
| `cluster.serviceSpec.dr.replicationNodePort`                                                  | Voltdb DR port to expose VoltDB replication service on each node, type NodePort only                                                                                                         | `31555`                                                                                                     |
| `cluster.serviceSpec.dr.servicePerPod`                                                        | Allocates a DR service per VoltDB cluster pod                                                                                                                                                | `false`                                                                                                     |
| `cluster.serviceSpec.dr.publicIPFromService`                                                  | Operator will wait to get the public IP address from the service status set by Kubernetes                                                                                                    | `false`                                                                                                     |
| `cluster.serviceSpec.dr.override`                                                             | Allows per-pod-service overrides of serviceSpec                                                                                                                                              | `[]`                                                                                                        |
| `cluster.serviceSpec.dr.override[].podIndex`                                                  | Pod ordinal (0, 1. ...) this override applies to                                                                                                                                             | `""`                                                                                                        |
| `cluster.serviceSpec.dr.override[].annotations`                                               | Custom annotations for this pod's service                                                                                                                                                    | `""`                                                                                                        |
| `cluster.serviceSpec.dr.override[].publicIP`                                                  | Public IP for the DR service (--drpublic)                                                                                                                                                    | `""`                                                                                                        |
| `cluster.serviceSpec.dr.override[].spec`                                                      | Service spec for this pod                                                                                                                                                                    | `{}`                                                                                                        |
| `cluster.serviceSpec.dr.override[].spec.type`                                                 |  See cluster.serviceSpec.dr.type                                                                                                                                                             | `""`                                                                                                        |
| `cluster.serviceSpec.dr.override[].spec.loadBalancerIP`                                       |  Load balancer IP for this service                                                                                                                                                           | `""`                                                                                                        |
| `cluster.serviceSpec.dr.override[].spec.externalIPs`                                          |  External IPs for this service                                                                                                                                                               | `[]`                                                                                                        |
| `cluster.config.auth.username`                                                                | User added for operator VoltDB API communication when hash security is enabled                                                                                                               | `voltdb-operator`                                                                                           |
| `cluster.config.auth.password`                                                                | Password added for operator VoltDB API communication when hash security is enabled                                                                                                           | `""`                                                                                                        |
| `cluster.config.schemas`                                                                      | Map of optional schema files containing data definition statements                                                                                                                           | `{}`                                                                                                        |
| `cluster.config.classes`                                                                      | Map of optional jar files container stored procedures                                                                                                                                        | `{}`                                                                                                        |
| `cluster.config.licenseXMLFile`                                                               | VoltDB Enterprise license.xml                                                                                                                                                                | `{}`                                                                                                        |
| `cluster.config.log4jcfgFile`                                                                 | Custom Log4j configuration file                                                                                                                                                              | `{}`                                                                                                        |
| `cluster.config.deployment.cluster.kfactor`                                                   | K-factor to use for database durability and data safety replication                                                                                                                          | `1`                                                                                                         |
| `cluster.config.deployment.cluster.sitesperhost`                                              | SitesPerHost for VoltDB Cluster                                                                                                                                                              | `8`                                                                                                         |
| `cluster.config.deployment.heartbeat.timeout`                                                 | Internal VoltDB cluster verification of presence of other nodes (seconds)                                                                                                                    | `90`                                                                                                        |
| `cluster.config.deployment.partitiondetection.enabled`                                        | Controls detection of network partitioning                                                                                                                                                   | `true`                                                                                                      |
| `cluster.config.deployment.commandlog.enabled`                                                | Command logging for database durability (recommended)                                                                                                                                        | `true`                                                                                                      |
| `cluster.config.deployment.commandlog.logsize`                                                | Command logging allocated disk space (MB)                                                                                                                                                    | `1024`                                                                                                      |
| `cluster.config.deployment.commandlog.synchronous`                                            | Transactions do not complete until logged to disk                                                                                                                                            | `false`                                                                                                     |
| `cluster.config.deployment.commandlog.frequency.time`                                         | How often the command log is written, by time (milliseconds)                                                                                                                                 | `200`                                                                                                       |
| `cluster.config.deployment.commandlog.frequency.transactions`                                 | How often the commang log is written, by transaction command                                                                                                                                 | `2147483647`                                                                                                |
| `cluster.config.deployment.dr.id`                                                             | Unique cluster id, 0-127                                                                                                                                                                     | `0`                                                                                                         |
| `cluster.config.deployment.dr.role`                                                           | Role for this cluster, one of: master, replica, xdcr, none                                                                                                                                   | `master`                                                                                                    |
| `cluster.config.deployment.dr.connection.enabled`                                             | Specifies whether disaster recovery is enabled                                                                                                                                               | `false`                                                                                                     |
| `cluster.config.deployment.dr.connection.source`                                              | If role is replica or xdcr: list of host names or IP addresses of remote node(s)                                                                                                             | `""`                                                                                                        |
| `cluster.config.deployment.dr.connection.preferredSource`                                     | Cluster ID of preferred source                                                                                                                                                               | `""`                                                                                                        |
| `cluster.config.deployment.dr.connection.ssl`                                                 | Certificate file path for DR consumer (replica or xdcr mode), defaults to truststore.file location /etc/voltdb/ssl/certificate.txt when SSL is enabled, otherwise it must be specified       | `""`                                                                                                        |
| `cluster.config.deployment.dr.consumerlimit.maxsize`                                          | Enable DR consumer flow control either maxsize or maxbuffers must be specified  maxsize can be specified as 50m, 1g or just number for bytes                                                 | `""`                                                                                                        |
| `cluster.config.deployment.dr.consumerlimit.maxbuffers`                                       | Enable DR consumer flow control either maxsize or maxbuffers must be specified                                                                                                               | `""`                                                                                                        |
| `cluster.config.deployment.export.configurations`                                             | List of export configurations                                                                                                                                                                | `[]`                                                                                                        |
| `cluster.config.deployment.import.configurations`                                             | List of import configurations                                                                                                                                                                | `[]`                                                                                                        |
| `cluster.config.deployment.httpd.enabled`                                                     | Determines if HTTP API daemon is enabled                                                                                                                                                     | `true`                                                                                                      |
| `cluster.config.deployment.httpd.jsonapi.enabled`                                             | Determines if jSON over HTTP API is enabled                                                                                                                                                  | `true`                                                                                                      |
| `cluster.config.deployment.paths.commandlog.path`                                             | Directory path for command log                                                                                                                                                               | `/pvc/voltdb/voltdbroot/command_log`                                                                        |
| `cluster.config.deployment.paths.commandlogsnapshot.path`                                     | Directory path for command log snapshot                                                                                                                                                      | `/pvc/voltdb/voltdbroot/command_log_snapshot`                                                               |
| `cluster.config.deployment.paths.droverflow.path`                                             | Directory path for disaster recovery overflow                                                                                                                                                | `/pvc/voltdb/voltdbroot/dr_overflow`                                                                        |
| `cluster.config.deployment.paths.exportcursor.path`                                           | Directory path for export cursors                                                                                                                                                            | `/pvc/voltdb/voltdbroot/export_cursor`                                                                      |
| `cluster.config.deployment.paths.exportoverflow.path`                                         | Directory path for export overflow                                                                                                                                                           | `/pvc/voltdb/voltdbroot/export_overflow`                                                                    |
| `cluster.config.deployment.paths.largequeryswap.path`                                         | Directory path for large query swapping                                                                                                                                                      | `/pvc/voltdb/voltdbroot/large_query_swap`                                                                   |
| `cluster.config.deployment.paths.snapshots.path`                                              | Directory path for snapshots. Must not be located in a read-only root directory of mounted storage (as init --force will rename existing snapshot folder). Use a subdirectory.               | `/pvc/voltdb/voltdbroot/snapshots`                                                                          |
| `cluster.config.deployment.security.enabled`                                                  | Controls whether user-based authentication and authorization are used                                                                                                                        | `false`                                                                                                     |
| `cluster.config.deployment.security.provider`                                                 | Allows use of external Kerberos provider; one of: hash, kerberos                                                                                                                             | `hash`                                                                                                      |
| `cluster.config.deployment.snapshot.enabled`                                                  | Enable/disable periodic automatic snapshots                                                                                                                                                  | `true`                                                                                                      |
| `cluster.config.deployment.snapshot.frequency`                                                | Frequency of automatic snapshots (in s,m,h)                                                                                                                                                  | `24h`                                                                                                       |
| `cluster.config.deployment.snapshot.prefix`                                                   | Unique prefix for snapshot files                                                                                                                                                             | `AUTOSNAP`                                                                                                  |
| `cluster.config.deployment.snapshot.retain`                                                   | Number of snapshots to retain                                                                                                                                                                | `2`                                                                                                         |
| `cluster.config.deployment.snmp.enabled`                                                      | Enables or disables use of SNMP                                                                                                                                                              | `false`                                                                                                     |
| `cluster.config.deployment.snmp.target`                                                       | Host name or IP address, and optional port (default 162), for SNMP server                                                                                                                    | `""`                                                                                                        |
| `cluster.config.deployment.snmp.authkey`                                                      | SNMPv3 authentication key if protocol is not NoAuth                                                                                                                                          | `voltdbauthkey`                                                                                             |
| `cluster.config.deployment.snmp.authprotocol`                                                 | SNMPv3 authentication protocol. One of: SHA, MD5, NoAuth                                                                                                                                     | `SHA`                                                                                                       |
| `cluster.config.deployment.snmp.community`                                                    | Name of SNMP community                                                                                                                                                                       | `public`                                                                                                    |
| `cluster.config.deployment.snmp.privacykey`                                                   | SNMPv3 privacy key if protocol is not NoPriv                                                                                                                                                 | `voltdbprivacykey`                                                                                          |
| `cluster.config.deployment.snmp.privacyprotocol`                                              | SNMPv3 privacy protocol. One of: AES, DES, 3DES, AES192, AES256, NoPriv                                                                                                                      | `AES`                                                                                                       |
| `cluster.config.deployment.snmp.username`                                                     | Username for SNMPv3 authentication; else SNMPv2c is used                                                                                                                                     | `""`                                                                                                        |
| `cluster.config.deployment.ssl.enabled`                                                       | Enables TLS/SSL security for the HTTP port (default 8080, 8443)                                                                                                                              | `false`                                                                                                     |
| `cluster.config.deployment.ssl.external`                                                      | Extends TLS/SSL security to all external ports (default admin 21211, client 21212)                                                                                                           | `false`                                                                                                     |
| `cluster.config.deployment.ssl.internal`                                                      | Extends TLS/SSL security to the internal port (default 3021)                                                                                                                                 | `false`                                                                                                     |
| `cluster.config.deployment.ssl.dr`                                                            | Extends TLS/SSL security to the DR port (5555)                                                                                                                                               | `false`                                                                                                     |
| `cluster.config.deployment.ssl.keystore.file`                                                 | Keystore file to mount at the keystore path                                                                                                                                                  | `""`                                                                                                        |
| `cluster.config.deployment.ssl.keystore.password`                                             | Password for VoltDB keystore                                                                                                                                                                 | `""`                                                                                                        |
| `cluster.config.deployment.ssl.truststore.file`                                               | Truststore file to mount at the truststore path                                                                                                                                              | `""`                                                                                                        |
| `cluster.config.deployment.ssl.truststore.password`                                           | Password for VoltDB truststore                                                                                                                                                               | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.elastic.duration`                                   | Target value for the length of time each rebalance transaction will take (milliseconds)                                                                                                      | `50`                                                                                                        |
| `cluster.config.deployment.systemsettings.elastic.throughput`                                 | Target value for rate of data processing by rebalance transactions (MB)                                                                                                                      | `2`                                                                                                         |
| `cluster.config.deployment.systemsettings.flushinterval.minimum`                              | Interval between checking for need to flush (milliseconds)                                                                                                                                   | `1000`                                                                                                      |
| `cluster.config.deployment.systemsettings.flushinterval.dr.interval`                          | Interval for flushing DR data (milliseconds)                                                                                                                                                 | `1000`                                                                                                      |
| `cluster.config.deployment.systemsettings.flushinterval.export.interval`                      | Interval for flushing export data (milliseconds)                                                                                                                                             | `4000`                                                                                                      |
| `cluster.config.deployment.systemsettings.procedure.loginfo`                                  | Threshold for long-running task detection (milliseconds)                                                                                                                                     | `10000`                                                                                                     |
| `cluster.config.deployment.systemsettings.query.timeout`                                      | Timeout on SQL queries (milliseconds)                                                                                                                                                        | `10000`                                                                                                     |
| `cluster.config.deployment.systemsettings.resourcemonitor.frequency`                          | Resource Monitor interval between resource checks (seconds)                                                                                                                                  | `60`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.memorylimit.size`                   | Limit on memory use (in GB or as percentage)                                                                                                                                                 | `80%`                                                                                                       |
| `cluster.config.deployment.systemsettings.resourcemonitor.memorylimit.alert`                  | Alert level for memory use (in GB or as percentage)                                                                                                                                          | `70%`                                                                                                       |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlog.size`          | Resource Monitor disk limit on disk use (in GB or percentage, empty is unlimited)                                                                                                            | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlog.alert`         | Resource Monitor alert level for disk use (in GB or as percentage, empty is unlimited)                                                                                                       | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlogsnapshot.size`  | Resource Monitor disk limit on disk use (in GB or percentage, empty is unlimited)                                                                                                            | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.commandlogsnapshot.alert` | Resource Monitor alert level for disk use (in GB or as percentage, empty is unlimited)                                                                                                       | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.droverflow.size`          | Resource Monitor disk limit on disk use (in GB or percentage, empty is unlimited)                                                                                                            | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.droverflow.alert`         | Resource Monitor alert level for disk use (in GB or as percentage, empty is unlimited)                                                                                                       | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.exportoverflow.size`      | Resource Monitor disk limit on disk use (in GB or percentage, empty is unlimited)                                                                                                            | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.exportoverflow.alert`     | Resource Monitor alert level for disk use (in GB or as percentage, empty is unlimited)                                                                                                       | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.snapshots.size`           | Resource Monitor disk limit on disk use (in GB or percentage, empty is unlimited)                                                                                                            | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.snapshots.alert`          | Resource Monitor alert level for disk use (in GB or as percentage, empty is unlimited)                                                                                                       | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.topicsdata.size`          | Resource Monitor disk limit on disk use (in GB or percentage, empty is unlimited)                                                                                                            | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.resourcemonitor.disklimit.topicsdata.alert`         | Resource Monitor alert level for disk use (in GB or as percentage, empty is unlimited)                                                                                                       | `""`                                                                                                        |
| `cluster.config.deployment.systemsettings.snapshot.priority`                                  | Priority for snapshot work                                                                                                                                                                   | `6`                                                                                                         |
| `cluster.config.deployment.systemsettings.temptables.maxsize`                                 | Limit the size of temporary database tables (MB)                                                                                                                                             | `100`                                                                                                       |
| `cluster.config.deployment.users`                                                             | Define a list of VoltDB users to be added to the deployment                                                                                                                                  | `[]`                                                                                                        |

#### operator.*

The purpose of this section is to specify VoltDB Kubernetes operator settings.

The following tables list the configurable parameters for the `operator` option:

| Parameter                              | Description                                                                         | Default                                      |
| -------------------------------------- | ----------------------------------------------------------------------------------- | -------------------------------------------- |
| `operator.enabled`                     | Create VoltDB Operator to manage clusters                                           | `true`                                       |
| `operator.image.registry`              | Image registry                                                                      | `docker.io`                                  |
| `operator.image.repository`            | Image repository                                                                    | `voltdb/voltdb-operator`                     |
| `operator.image.tag`                   | Image tag                                                                           | `v0.1.0`                                     |
| `operator.image.pullPolicy`            | Image pull policy                                                                   | `Always`                                     |
| `operator.replicas`                    | Pod replica count                                                                   | `1`                                          |
| `operator.debug.enabled`               | Debug level logging                                                                 | `false`                                      |
| `operator.logformat`                   | Log encoding format for the operator (console or json)                              | `json`                                       |
| `operator.serviceAccount.create`       | If true, create & use service account for VoltDB operator containers                | `true`                                       |
| `operator.serviceAccount.name`         | If not set and create is true, a name is generated using the fullname template      | `""`                                         |
| `operator.cleanupCustomResource`       | Attempt to cleanup CRDs before installing the Helm chart (Helm 2 only)              | `false`                                      |
| `operator.cleanupNamespaceClusters`    | Delete ALL VoltDB clusters in the namespace when the operator Helm chart is deleted | `false`                                      |
| `operator.podLabels`                   | Additional custom Pod labels                                                        | `{}`                                         |
| `operator.podAnnotations`              | Additional custom Pod annotations                                                   | `{}`                                         |
| `operator.resources`                   | CPU/Memory resource requests/limits                                                 | `{}`                                         |
| `operator.nodeSelector`                | Node labels for pod assignment                                                      | `{}`                                         |
| `operator.tolerations`                 | Pod tolerations for Node assignment                                                 | `[]`                                         |
| `operator.affinity`                    | Node affinity                                                                       | `{}`                                         |
| `operator.securityContext`             | Container security context                                                          | `{"privileged":false,"runAsNonRoot":true,"runA

#### metrics.*

Deploy VoltDB metrics exporter agent for VoltDB cluster monitoring.

Install [kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack) in your cluster to create ServiceMonitor objects. 

An [example Prometheus file](examples/prometheus-values.yaml)

The following tables list the configurable parameters for the `metrics` option:

| Parameter                              | Description                                                                         | Default                                      |
| -------------------------------------- | ----------------------------------------------------------------------------------- | -------------------------------------------- |
| `metrics.enabled`                      | Create VoltDB metrics exporter agent to manage clusters                             | `false`                                      |
| `metrics.image.registry`               | Image registry                                                                      | `docker.io`                                  |
| `metrics.image.repository`             | Image repository                                                                    | `voltdb/voltdb-prometheus`                   |
| `metrics.image.tag`                    | Image tag                                                                           | `latest`                                     |
| `metrics.image.pullPolicy`             | Image pull policy                                                                   | `Always`                                     |
| `metrics.manageSecrets`             | Variable names for existing metric secrets                                             |                                     |
| `metrics.user`                         | Metric service user id used to access VoltDB. This value and the password value must exist in VoltDB. Superseded by `credSecretName` when provided. Required when security is enabled in VoltDB.  | `""`                                         |
| `metrics.password`                     | Metric service password used to access VoltDB. Superseded by `credSecretName` when provided. Required when security is enabled in VoltDB. | `""`                                         |
| `metrics.ssl`                          | SSL configuration settings for metrics.                                             |
| `metrics.manageSecrets.credSecretName` | Variable name for the secret containing metric username and password. This overrides user and password values below. |                                 |
| `metrics.ssl.enabled`                  | Enable SSL for metrics                                                              |  
| `metrics.ssl.keystore.password`        | Password for the keystore file                                                      |  
| `metrics.ssl.keystore.file`            | Contents of the keystore file                                                       |  
| `metrics.ssl.truststore.password`      | Password for the trust store file                                                   |  
| `metrics.ssl.truststore.file`          | Contents of the trust store file                                                    |  
| `metrics.port`                         | VoltDB API port, uses 21211 if not specified                                        | `""`                                         |
| `metrics.stats`                        | Comma-separated list of stats to collect from the VoltDB database                   | `""`                                         |
| `metrics.skipstats`                    | Stats to ignore when collecting all stats, cannot be used in conjunction with stats | `""`                                         |
| `metrics.resources`                    | CPU/Memory resource requests/limits                                                 | `{}`                                         |

#### global.*

The purpose of this section is to specify global settings.

The following tables list the configurable parameters for the `global` option:

| Parameter                                     | Description                                                                                    | Default               |
| --------------------------------------------- | ---------------------------------------------------------------------------------------------- | --------------------- |
| `global.rbac.create`                          | Create RBAC objects, ServiceAccount, Role and RoleBinding                                      | `true`                |
| `global.rbac.pspEnabled`                      | Create Pod Security Policies                                                                   | `true`                |
| `global.rbac.pspAnnotations`                  | Additional Pod Security Policy annotations                                                     | `{}`                  |
| `global.createNetworkPolicy`                  | Create Network Policies for VoltDB operator and cluster communication                          | `true`                |
| `global.image.credentials.username`           | Username for fetching VoltDB Enterprise container images from official registry                | `""`                  |
| `global.image.credentials.password`           | Password for fetching VoltDB Enterprise container images from official registry                | `""`                  |
| `global.image.credentials.servername`         | Server for fetching VoltDB Enterprise container images from official registry                  | `docker.io`           |
| `global.image.credentials.pullSecretOverride` | Pre-created pull secret for fetching VoltDB Enterprise container images from official registry | `dockerio-registry`   |

### Running

Verify the Helm chart:

```bash
$ helm install myrelease voltdb/voltdb --debug --dry-run
```

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example:

```bash
$ helm install myrelease --set cluster.clusterSpec.image.tag=10.0.0-dev voltdb/voltdb
```

Alternatively, a YAML file that specifies the values for the parameters can be provided. For example:

```bash
$ helm install myrelease -f values.yaml voltdb/voltdb
```

Check the deployment status:

```bash
helm status myrelease
```

## Upgrading

To upgrade an existing release:

```bash
$ helm upgrade myrelease voltdb/voltdb --version 1.2.3
```

This chart uses [semantic versioning](https://semver.org/). A major chart version change (like v1.2.3 -> v2.0.0) indicates that there is an incompatible breaking change needing manual actions. Breaking changes will be documented in the [release notes](https://github.com/voltdb/voltdb-operator/releases).

> Note: If updating VoltDB runtime values or deployment values after installting the Helm chart, upgrading the Helm chart may revert any manual changes you have done. To avoid this, always update via Helm values, or ensure Helm values reflect what changes you made manually.

Releases can also be rolled back, depending on what has changed in the latest release. Please see the [official docs](https://docs.voltdb.com/AdminGuide/MaintainUpgradeVoltdb.php) on downgrading, or falling back to a previous VoltDB version.

## Uninstalling the Chart

To uninstall/delete the chart:

```bash
$ helm delete myrelease
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

To also permanently delete the VoltDB data persistent volumes:

```bash
$ kubectl delete pvc -l release=myrelease
```

## RBAC Permissions

Typically Helm charts are installed by users with the Kubernetes `cluster-admin` ClusterRole. In secure environments this may not be the case.

The Helm chart created RBAC service accounts, roles and rolebindings that allows the operator to fully manage clusters without human intervention.

Users will need the following permissions to install the Helm chart:

### Cluster scoped permissions

**Verbs:** `*` or `get`, `list`, `create`, `update`, `patch`, `watch`, `delete`, `deleteCollection`

**API Group & Resource:**

- apiextensions.k8s.io/**CustomResourceDefinitions**
- policy/v1beta1/**PodSecurityPolicy** (Only when Pod Security Policies is enabled)
- rbac.authorization.k8s.io/v1/**ClusterRole** (Only when XDCR service type is NodePort)
- rbac.authorization.k8s.io/v1/**ClusterRoleBinding** (Only when XDCR service type is NodePort)

**Verbs:** `get`, `list`, `watch`

- v1/**Node** (Only when XDCR service type is NodePort)

### Namespace scoped permissions

> Note: Some of these permissions are optional if metrics, RBAC or security is disabled, which we do not recommend.

For information on how to check if you have the appropriate permission, see [the official Kubernetes documentation](https://kubernetes.io/docs/reference/access-authn-authz/authorization/#checking-api-access).

**Verbs:** `*` or `get`, `list`, `create`, `update`, `patch`, `watch`, `delete`, `deleteCollection`

**API Group & Resource:**

- voltdb.com/v1/**VoltDBCluster**
- v1/**ServiceAccount**
- v1/**Service**
- v1/**Endpoints**
- v1/**ConfigMap**
- v1/**Secret**
- apps/v1/**Deployment**
- batch/v1/**Job**
- rbac.authorization.k8s.io/v1/**Role**
- rbac.authorization.k8s.io/v1/**RoleBinding**
- networking.k8s.io/v1/**NetworkPolicy**
- policy/v1beta1/**PodDisruptionBudget**

- voltdb.com/v1/**RemoteVoltDBCluster** (XDCR only)

- monitoring.coreos.com/v1/**ServiceMonitor** (Only when Prometheus metrics agent is enabled)

```
K8S_NAMESPACE=replaceme
K8S_USER=replaceme

kubectl auth can-i create customresourcedefinition --as $K8S_USER --all-namespaces
kubectl auth can-i create clusterrole --as $K8S_USER --all-namespaces
kubectl auth can-i create clusterrolebinding --as $K8S_USER --all-namespaces
kubectl auth can-i create podsecuritypolicy --as $K8S_USER --all-namespaces

kubectl auth can-i create voltdbcluster --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create serviceaccount --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create service --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create endpoints --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create configmap --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create secret --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create deployment --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create job --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create role --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create rolebinding --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create networkpolicy --namespace $K8S_NAMESPACE --as $K8S_USER
kubectl auth can-i create poddisruptionbudget --namespace $K8S_NAMESPACE --as $K8S_USER

kubectl auth can-i create servicemonitor --namespace $K8S_NAMESPACE --as $K8S_USER # Prometheus Metrics Agent only

kubectl auth can-i get nodes --as $K8S_USER --all-namespaces #XDCR only
kubectl auth can-i create remotevoltdbcluster --namespace $K8S_NAMESPACE --as $K8S_USER # XDCR only
```

## Security Considerations

The Helm charts configuration options work on a secure-by-default approach. We apply the following best practices for container security:

- Image vulnerability scanning: All images released for the Helm chart are vulnerability scanned and use the latest stable trusted base Alpine Docker image. This provides a lower attack surface for vulnerabilites and has less dependencies than other operations systems. We provide the minimum components required to run VoltDB enterprise within our images.
- Kubernetes Secrets: All sensitive information is kept using native [secrets](https://kubernetes.io/docs/concepts/configuration/secret/), such as your license.xml. Kubernetes RBAC role bindings can restrict access to this sensitive data.
- Pod Security Policies: Control the security-related attributes of pods, including container privilege levels. This prevents processes running as root, privilege escalation, a read-only root filesystem, use of the default (masked) /proc filesystem mount, block access to the host network and process space, dropping unused and unnecessary Linux capabilities and use of SELinux for more fine-grained process controls.
- RBAC: Role-based Access Control is a standard method for controlling authorization to access the Kubernetes API server and associated objects. If RBAC is enabled, this will be configured automatically by the Helm chart.
- Kubernetes Service Accounts: A separate service account is created for each operator and VoltDB cluster with limited permissions through a minimal Kubernetes role and role binding. This can be used to audit the activities of the operator and cluster and how it can interact with other containers and Kubernetes API objects.
- Namespace Isolation: All resources required to run the VoltDB cluster are deployed inside a singular namespace security boundary (with the exception of PVs) which allows multi-tenancy with several teams or projects using VoltDB.
- Labels and Annotations: All resources belonging to a cluster can be easily identified through matching labels and annotations. Users can add their own common labels or annotations if necessary for compliance and audit. Proper use of labels and annotations can be used to easily alert teams and triage security issues.
- Network Policies: Are used to limit communication to between the operator, cluster and trusted applications.
- SSL/TLS: Secure communication is configurable when a truststore, keystore and PEM certificate is provided in the Helm values. This updates the deployment.xml respectively.

> Note: Users are responsible for securing their own defense-in-depth approach, including underlying hardware, infrastructure, platform (Kubernetes clusters), release engineering and application level components. Security should cover all three build, deploy and runtime phases. Kubernetes-native security controls can be used to reduce operational risk.
