# This file is part of VoltDB.
# Copyright (C) 2020 VoltDB Inc.

# Default values for VoltDB Operator.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

## Provide a name in place of voltdb-operator for `name:` labels
##
nameOverride: ""

## Provide a name to substitute for the full names of resources
##
fullnameOverride: ""

## Labels to apply to all resources
##
commonLabels: {}

##
global:
  image:
    ## Credentials used for pulling docker images
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    credentials:
      # username:
      # password:
      servername: "docker.io"
    ## Specify an existing image pull secret to add to all service accounts
    pullSecretOverride: "dockerio-registry"

  rbac:
    create: true
    pspEnabled: true
    pspAnnotations:
      ## Specify PodSecurityPolicy annotations
      ## Ref: https://kubernetes.io/docs/concepts/policy/pod-security-policy/#apparmor
      ## Ref: https://kubernetes.io/docs/concepts/policy/pod-security-policy/#seccomp
      ## Ref: https://kubernetes.io/docs/concepts/policy/pod-security-policy/#sysctl
      ##
      seccomp.security.alpha.kubernetes.io/allowedProfileNames: 'docker/default'
      seccomp.security.alpha.kubernetes.io/defaultProfileName:  'docker/default'
      apparmor.security.beta.kubernetes.io/defaultProfileName: 'runtime/default'
  createNetworkPolicy: true

## Operator to manage VoltDB Clusters
## ref: https://kubernetes.io/docs/concepts/extend-kubernetes/operator/
##
operator:

  enabled: true

  ## Details of the container image to use for the VoltDB Operator. Required.
  ##
  image:
    registry: "docker.io"
    repository: voltdb/voltdb-operator
    tag: 1.3.3
    pullPolicy: Always

  ## Amount of replicas. Required.
  ##
  replicas: 1

  ## Debug level logging for the VoltDB Operator. Required.
  ##
  debug:
    enabled: false

  ## Log encoding format for the operator (console or json). Required.
  ##
  logformat: "json"

  ## Namespaces to scope the interaction of the VoltDB Operator and the apiserver (allow list).
  ## This is mutually exclusive with denyNamespaces. Setting this to an empty object will disable the configuration.
  ##
  # namespaces: {}
    # releaseNamespace: true
    # additional:
    # - kube-system

  ## Namespaces not to scope the interaction of the VoltDB Operator (deny list). Optional.
  ##
  # denyNamespaces: []

  ## Service account for VoltDB operator to use. Optional.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
  ##
  serviceAccount:
    create: true
    name: ""

  ## Attempt to clean up CRDs created by the VoltDB Operator on Helm uninstall. Required.
  ## Note: Do not use with multiple VoltDB clusters, requires edit clusterrolebinding.
  ##
  cleanupCustomResource: false

  ## Delete ALL VoltDB clusters in the namespace when the operator Helm chart is deleted. Required.
  ##
  cleanupNamespaceClusters: false

  ## Labels to add to the VoltDB Operator Pod template. Optional.
  ##
  podLabels: {}

  ## Annotations to add to the VoltDB Operator Pod template. Optional.
  ## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
  ##
  podAnnotations: {}

  ## Resource requests/limits to specify for the VoltDB Operator. Optional.
  ## ref: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
  ##
  resources:
    requests:
      cpu: 100m
      memory: 100Mi
    limits:
      cpu: 200m
      memory: 200Mi

  ## Node selector to use for the VoltDB Operator. Optional
  ## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#nodeselector
  ##
  nodeSelector: {}

  ## Tolerations to use for the VoltDB Operator. Optional.
  ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
  ##
  tolerations: []
  # - key: "key"
  #   operator: "Equal"
  #   value: "value"
  #   effect: "NoSchedule"

  ## Affinity to use for the VoltDB Operator. Optional
  ## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
  ## In the below example, we will schedule nodes on availability zone 1 & 2 only and ensure nodes pods are on a different host name.
  ##
  affinity: {}
  # nodeAffinity:
  #   requiredDuringSchedulingIgnoredDuringExecution:
  #     nodeSelectorTerms:
  #     - matchExpressions:
  #       - key: kubernetes.io/e2e-az-name
  #         operator: In
  #         values:
  #         - e2e-az1
  #         - e2e-az2
  # podAntiAffinity:
  #   requiredDuringSchedulingIgnoredDuringExecution:
  #   - labelSelector:
  #       matchExpressions:
  #       - key: name
  #         operator: In
  #         values:
  #          - voltdb-cluster
  #       - key: release
  #         operator: In
  #         values:
  #          - mydb
  #       - key: voltdb-cluster-name
  #         operator: In
  #         values:
  #          - mydb-voltdb-cluster
  #     topologyKey: "kubernetes.io/hostname"

  ## SecurityContext to use for the VoltDB Operator container. Optional.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
  ##
  securityContext:
    privileged: false
    runAsNonRoot: true
    runAsUser: 1001
    runAsGroup: 1001
    readOnlyRootFilesystem: true

## Deploy a VoltDB Cluster
##
cluster:

  enabled: true

  ## Service account for VoltDB Cluster to use. Optional.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
  ##
  serviceAccount:
    create: true
    name: ""

  ## Configs (Secrets and ConfigMaps) for VoltDB Cluster to use. Optional.
  ##
  config:
    # Deployment.xml values set for initialization.
    deployment:
      cluster:
        ## K-factor to use for database durability when creating the database. Required.
        ## ref: https://docs.voltdb.com/UsingVoltDB/ChapKSafety.php
        ##
        kfactor: 1

        ## Sites Per Host. Required.
        ## ref: https://docs.voltdb.com/UsingVoltDB/RunClusterConfig.php
        ##
        sitesperhost: 8

      ## Internal VoltDB cluster verification of presence of other nodes (seconds). Required.
      ##
      heartbeat:
        timeout: 90

      ## Controls detector of network partitioning. Required.
      ##
      partitiondetection:
        enabled: true

      ## Command Logging for database durability and preserving a record of every transaction. Required.
      ## ref: https://docs.voltdb.com/UsingVoltDB/CmdLogConfig.php
      ##
      commandlog:
        # Enable command logging
        enabled: true
        # Allocated disk space (MB)
        logsize: 1024
        # Transactions do not complete until logged to disk
        synchronous: false
        # How often the command log is written, by time (milliseconds) and transaction count
        frequency:
          time: 200
          transactions: "2147483647"

      ## Disaster recovery. Optional.
      ## ref: https://docs.voltdb.com/UsingVoltDB/ChapReplication.php
      ##
      dr:
        # Unique cluster ID from 0-127
        id: 0
        # Role for this cluster, one of: 'xdcr' or 'none'
        role: none
        # Enable disaster recovery, if false, all DR fields are ignored
        connection:
          enabled: false
          source: ""
          preferredSource: ""
          ssl: ""
        consumerlimit:
          maxsize: ""
          maxbuffers: ""

      ## VoltDB Streaming Data Export/Import Configurations
      ## ref: https://docs.voltdb.com/UsingVoltDB/ChapExport.php
      ##
      export:
        configurations: []
        # - target: "systemlog"
        #   enabled: "true"
        #   type: "file"
        #   properties:
        #     type: "csv"
        #     nonce: "syslog"
        #     period: "60"
        #
        # - target: "myspecial"
        #   enabled: "true"
        #   type: "custom"
        #   exportconnectorclass: "org.voltdb.exportclient.MyExportClient"
        #   properties:
        #     filename: "myexportfile.txt"

      import:
        configurations: []
        # - type: "kafka"
        #   enabled: "true"
        #   format: "csv"
        #   properties:
        #     brokers: "kafkasvr1:9092,kafkasvr2:9092"
        #     topics: "nyse,nasdaq"
        #     procedure: "STOCKS.insert"
        #
        # - type: "custom"
        #   enabled: "true"
        #   module: "mycustomimporter.jar"
        #   properties:
        #     datasource: "my.data.source"
        #     timeout: "5m"

      # Enable HTTP API daemon. Required.
      httpd:
        enabled: true
        # Enable JSON over HTTP API. Optional.
        jsonapi:
          enabled: true

      ## Storage paths. Optional.
      ## ref: https://docs.voltdb.com/AdminGuide/HostConfigPathOpts.php
      ##
      paths:
        commandlog:
          path: ""
        commandlogsnapshot:
          path: ""
        droverflow:
          path: ""
        exportoverflow:
          path: ""
        snapshots:
          path: ""
        largequeryswap:
          path: ""
        exportcursor:
          path: ""

      ## VoltDB Security
      ## ref: https://docs.voltdb.com/UsingVoltDB/ChapSecurity.php
      ##
      security:
        enabled: false
        provider: hash

      ## VoltDB cluster snapshots
      ## ref: https://docs.voltdb.com/UsingVoltDB/ChapSaveRestore.php
      ##
      snapshot:
        enabled: true
        frequency: 24h
        prefix: AUTOSNAP
        retain: 2

      ## VoltDB SNMP Monitoring
      ## ref: https://docs.voltdb.com/AdminGuide/MonitorChap.php
      ##
      snmp:
        enabled: false
        target: 1.2.3.4
        authkey: voltdbauthkey
        authprotocol: SHA
        community: public
        privacykey: voltdbprivacykey
        privacyprotocol: AES
        username: ""

      ## VoltDB TLS/SSL
      ## ref: https://docs.voltdb.com/UsingVoltDB/SecuritySSL.php
      ##
      ## NOTE: ssl.enabled needs to be enabled, if external, internal or dr is enabled.
      ##
      ssl:
        # Enable for VoltDB Management Console (Web 8080 8443)
        enabled: false
        # Enable for VoltDB external ports (admin 21211, client 21212)
        external: false
        # Enable for VoltDB internal port (3021)
        internal: false
        # Enable for VoltDB DR port (5555)
        dr: false
        keystore:
          password: ""
          file:
        truststore:
          password: ""
          file:

      systemsettings:
        ## Elastic Scaling. Optional.
        ## ref: https://docs.voltdb.com/UsingVoltDB/UpdateElastic.php
        #
        elastic:
          ## Target value for the length of time each rebalance transaction will take (in milliseconds)
          duration: 50
          ## Target value for rate of data processing by rebalance transactions (in MB)
          throughput: 2

        ## Flush Interval for how frequently data is flushed from queues. Optional.
        ## ref: https://docs.voltdb.com/AdminGuide/HostConfigDBOpts.php
        #
        flushinterval:
          minimum: 1000
          dr:
            interval: 1000
          export:
            interval: 4000

        ## Theshold for log-running task detection (milliseconds). Required.
        ##
        procedure:
          loginfo: 10000

        ## Timeout on SQL queries (milliseconds). Required.
        ##
        query:
          timeout: 10000

        ## Resource Monitor configuration. Optional.
        ## ref: https://docs.voltdb.com/AdminGuide/MonitorSysResource.php
        ##
        resourcemonitor:
          # Interval between resource checks (seconds)
          frequency: 60
          # Limit and alert on memory use (in GB or as percentage)
          memorylimit:
            size: 80%
            alert: 70%
          # Limit and alert on disk use per component (in GB or as percentage)
          disklimit:
            commandlog:
              size: ""
              alert: ""
            commandlogsnapshot:
              size: ""
              alert: ""
            droverflow:
              size: ""
              alert: ""
            exportoverflow:
              size: ""
              alert: ""
            snapshots:
              size: ""
              alert: ""
            topicsdata:
              size: ""
              alert: ""

        ## Priority for snapshot work. Optional.
        ##
        snapshot:
          priority: 6

        ## Limit the size of temporary database tables (MB). Required
        ##
        temptables:
          maxsize: 100

      ## VoltDB Users
      ## ref: https://docs.voltdb.com/UsingVoltDB/SecurityUsersGroups.php
      ##
      users: []
      # - name: "operator"
      #   password: "mech"
      #   roles: "administrator, ops"
      # - name: "developer"
      #   password: "tech"
      #   roles: "ops, dbuser"
      # - name: "clientapp"
      #   password: "xyzzy"
      #   roles: "dbuser"


    ## Enterprise License XML config file, otherwise use trial. Optional.
    ##
    # licenseXMLFile: |
    #  <license>
    #     <permit version="1" scheme="0">
    #          <type>Enterprise Edition</type>
    #          <issuer>
    #              <company>VoltDB</company>
    #              <email>support@voltdb.com</email>
    #              <url>http://voltdb.com/</url>
    #          </issuer>
    #          <issuedate>2000-01-01</issuedate>
    #          <licensee>My Company</licensee>
    #          <expiration>2000-01-01</expiration>
    #          <hostcount max="12"/>
    #          <features trial="true">
    #              <wanreplication>true</wanreplication>
    #              <dractiveactive>true</dractiveactive>
    #          </features>
    #          <note>VoltDB Example License</note>
    #      </permit>
    #      <signature>
    #          1234567890ABCDEFGHIjKLMNOPQRSTUVWXYZ1234567890
    #          1234567890ABCDEFGHIjKLMNOPQRSTUVWXYZ1234567890
    #      </signature>
    #  </license>

    ## Log4jcfg config file. Optional.
    ##
    log4jcfgFile:

    ## Schema config file list. Optional.
    ##
    schemas: {}

    ## Classes config file list. Optional.
    ##
    classes: {}

    ## Authentication to communicate with the VoltDB API. Optional.
    ## Required when hash security is enabled. Password must be populated when
    ## enabled. A matching username and password entry will be automatically
    ## added to the users defined in the VoltDB deployment.
    ##
    auth:
      username: "voltdb-operator"
      password: ""

  ## Settings affecting clusterSpec. Required.
  ## ref: https://github.com/voltdb/voltdb-operator/blob/master/docs/api.md#clusterspec
  ##
  clusterSpec:
    ## Amount of replicas (VoltDB Node Pods). Required.
    ##
    replicas: 3

    ## Maximum unavailable pods (int) in the VoltDB cluster Pod Disruption Budget. Optional.
    ## If not specified, automatically set to kfactor (as recorded in the Config Secrets file).
    ## ref: https://kubernetes.io/docs/tasks/run-application/configure-pdb/
    ##
    maxPodUnavailable:

    ## VoltDB cluster maintenance mode (pause all nodes). Required.
    ## ref: https://docs.voltdb.com/UsingVoltDB/sysprocpause.php
    ##
    maintenanceMode: false

    ## VoltDB takes a terminal snapshot on cluster shutdown when scaling replicas to 0. Optional
    ## Valid options are: NoCommandLogging, Always, Never. Defaults to NoCommandLogging if empty.
    ##
    takeSnapshotOnShutdown:

    ## Always VoltDB init --force when nodes start or restart. Optional.
    ## WARNING: This will destroy all VoltDB data on PVCs except snapshots.
    ## Snapshots will be postfixed with .1 > .2 and so on.
    ## Used for temporary storage, cmdlog off and XDCR secondary clusters.
    ## USE WITH CAUTION.
    ##
    initForce: false

    ## List of nodes to stop. Optional.
    ## ref: https://docs.voltdb.com/UsingVoltDB/sysprocstopnode.php
    ##
    stoppedNodes: []
    # - "3"
    # - "7"

    ## disableFinalizers will ensure that finalizers aren't set for the CR
    ## so both operator, VoltDB and persistent volumes can be deleted at once
    ## with namespace deletion. WARNING: Finalizers control and implement
    ## asynchronous pre-delete hooks, when disabled some resources such as PVCs
    ## and PDBs may not get cleaned up when objects are deleted.
    ##
    disableFinalizers: false

    ## Delete and cleanup generated PVCs when VoltDBCluster is deleted
    ##
    deletePVC: false

    ## Allow VoltDB cluster restarts if necessary to apply user-requested configuration changes.
    ## May include automatic save and restore of the database.
    ##
    ## This will result in downtime; the time taken will depend on the size of the database.
    ##
    allowRestartDuringUpdate: false

    ## PEM encoded certificate chain used by VoltDB operator when SSL/TLS is enabled
    ## Required when SSL/TLS security is enabled.
    ##
    ssl:
      certificate: ""
      insecure: false

    ## Settings affecting persistent volumes. Required.
    ## These settings cannot be changed after the initial 'helm install'.
    ## ref: https://kubernetes.io/docs/concepts/storage/persistent-volumes
    ##
    persistentVolume:
      ## Persistent Volume storage size. Required.
      ##
      size: 1Gi
      ## Persistent Volume Storage Class name, otherwise use the default Storage Class. Optional.
      ##
      storageClassName: ""

      ## Use HostPath local node disk Volume for storage, otherwise use a storage class. Optional.
      ##
      hostPath:
        enabled: false
        ## HostPath storage path, defaults to /data/voltdb if not specified
        ##
        path: ""

    ## Storage Configs for provisioning additional PVCs. Optional.
    ## List of objects specifying name, mountPath and pvcSpec to use.
    ## These settings cannot be changed after the initial 'helm install'.
    ##
    storageConfigs: []
    # - mountPath: "/pvc/voltdb-backup/"
      # name: "voltdb-backup"
      # pvcSpec:
        # accessModes:
          # - ReadWriteOnce
        # storageClassName: default
        # resources:
          # requests:
            # storage: 10Gi

    ## Additional volumes and volume mount. Optional.
    ## ref: https://kubernetes.io/docs/concepts/storage/persistent-volumes/#types-of-persistent-volumes
    ##
    additionalVolumes: []
    # - name: hostpath-logs
      # hostPath:
        # # directory location on host
        # path: /data/voltdblogs
        # # the type field is optional
        # type: Directory
    # - name: tempcache-local
      # emptyDir: {}
    # - name: commandlog
      # persistentVolumeClaim:
        # claimName: commandlog-volume
    additionalVolumeMounts: []
    # - name: hostpath-logs
      # mountPath: /pvc/voltdblogs
    # - name: tempcache-local
      # mountPath: /pvc/local
    # - name: commandlog
      # mountPath: /pvc/commandlog

    # Set container environment variables
    env:
      ## VoltDB Java Runtime Options (VOLTDB_OPTS). Optional.
      ## ref: https://docs.voltdb.com/AdminGuide/HostConfigProcOpts.php
      ##
      VOLTDB_OPTS: ""

      ## VoltDB Maximum heap size. Optional.
      ## ref: https://docs.voltdb.com/PlanningGuide/MemSizeServers.php
      ##
      VOLTDB_HEAPMAX: ""

      ## VoltDB Flag to control whether full heap is commited at startup. Optional.
      ## Set to "true" or "false".
      ##
      VOLTDB_HEAPCOMMIT: ""

      ## VoltDB log4jcfg file path. Optional.
      ## ref: https://docs.voltdb.com/AdminGuide/LogConfig.php
      ##
      VOLTDB_K8S_LOG_CONFIG: ""

    # Set custom container environment variables
    customEnv: {}
      # FOO: BAR
      # MY_CUSTOM_ENVVAR: "TEST"

    ## VoltDB pod extra options for liveness and readiness probes
    ## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
    livenessProbe:
      enabled: true
      initialDelaySeconds: 20
      periodSeconds: 10
      timeoutSeconds: 1
      failureThreshold: 10 # Default 90 secs consistent failure
      successThreshold: 1
    readinessProbe:
      enabled: true
      initialDelaySeconds: 30
      periodSeconds: 17 # So it doesn't stay aligned with liveness probe
      timeoutSeconds: 2
      failureThreshold: 6 # Default 90 secs consistent failure
      successThreshold: 1
    startupProbe:
      enabled: true
      initialDelaySeconds: 45 # Modify for long startups
      periodSeconds: 10
      timeoutSeconds: 1
      failureThreshold: 18 # Default 3 mins consistent failure
      successThreshold: 1

    ## Details of the container image to use for the VoltDB Cluster. Required.
    ##
    image:
      registry: "docker.io"
      repository: voltdb/voltdb-enterprise
      tag: 10.2.3
      pullPolicy: Always

    ## Pod priority defined by an existing PriorityClass object. Optional.
    ## ref: https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/#how-to-use-priority-and-preemption
    ##
    priorityClassName: ""

    ## Additional VoltDB start command args for the pod container
    ##
    additionalStartArgs: []

    ## Resource requests/limits to specify for the VoltDB Cluster. Optional.
    ## ref: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
    ##
    resources: {}
    # requests:
    #   cpu: 100m
    #   memory: 100Mi
    # limits:
    #   cpu: 200m
    #   memory: 200Mi

    ## Additional Labels to add to the VoltDB Cluster Pod template. Optional.
    ## Warning: Key conflicts will overwrite original labels with the additional labels.
    ##
    additionalLabels: {}

    ## Additional Annotations to add to the VoltDB Cluster Pod template. Optional.
    ## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
    ##
    additionalAnnotations: {}

    ## Node selector to use for the VoltDB Cluster. Optional
    ## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#nodeselector
    ##
    nodeSelector: {}

    ## Tolerations to use for the VoltDB Cluster. Optional.
    ## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
    ##
    tolerations: []
    # - key: "key"
    #   operator: "Equal"
    #   value: "value"
    #   effect: "NoSchedule"

    ## Affinity to use for the VoltDB Cluster. Optional
    ## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
    ##
    affinity: {}
    # nodeAffinity:
    #   requiredDuringSchedulingIgnoredDuringExecution:
    #     nodeSelectorTerms:
    #     - matchExpressions:
    #       - key: kubernetes.io/e2e-az-name
    #         operator: In
    #         values:
    #         - e2e-az1
    #         - e2e-az2

    ## SecurityContext to use for the VoltDB Cluster pod. Optional.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
    ##
    podSecurityContext:
      runAsNonRoot: true
      runAsUser: 1001
      fsGroup: 1001

    ## SecurityContext to use for the VoltDB Cluster container. Optional.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
    ##
    securityContext:
      privileged: false
      runAsNonRoot: true
      runAsUser: 1001
      runAsGroup: 1001
      readOnlyRootFilesystem: true

    # ClusterInit is for pre-created existing secrets and configmaps to be used with the VoltDB cluster.
    # When specified, the VoltDB operator will look at these secrets and configmaps and ignore anything
    # the user specifies for deployment, license, log4j, schema or classes in the Helm chart.
    #
    clusterInit:
      initSecretRefName:
      schemaConfigMapRefName:
      classesConfigMapRefName:

    # Duration in seconds the pod needs to terminate gracefully
    # Used for kubectl delete pod and preStop hooks
    # ref: https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/#hook-handler-execution
    podTerminationGracePeriodSeconds: 45

  ## Settings affecting service spec. Optional.
  ## Typically configured for XDCR networking.
  ##
  serviceSpec:
    # type: ClusterIP
    # externalTrafficPolicy: Cluster
    adminPortEnabled: true
    # adminPort: 21211
    # adminNodePort: 31211

    clientPortEnabled: true
    # clientPort: 21212
    # clientNodePort: 31312

    # vmcPort: 8080
    # vmcNodePort: 31080
    # vmcSecurePort: 8443
    # vmcSecureNodePort: 31443

    # loadBalancerIP: ""
    # loadBalancerSourceRanges: [ ]

    # externalIPs: [ ]

    http: {}
      # sessionAffinity: ClientIP
      # sessionAffinityConfig:
      #   clientIP:
      #     timeoutSeconds: 10800

    dr: {}
      # # Note that changing type from LoadBalancer to NodePort (or from NodePort to LoadBalancer) will
      # # trigger a rolling update as StatefulSet in NodePort configuration relies on an init container
      # type: LoadBalancer
      # externalTrafficPolicy: Local
      # replicationPort: 5555
      # # replicationNodePort: 31555
      # servicePerPod: true
      # # availableIPs is a comma separated list of IP addresses and ranges
      # # that the operator can use
      # availableIPs:
      # # - 10.0.1.0
      # # - 10.0.1.2-10.0.1.6
      # # - 2001:db8::

## Add RemoteVoltDBCluster configuration
##
remoteclusters:
  ## A list of remote cluster CRs to create
  ## Uncomment and edit details
  # - name: "first-cluster"
  #   spec:
  #     remoteClusterVmcUrl: "http://first-cluster:8080"   # URL to connect to, example: http://example-operator.com:8443
  #     secretRefName: "voltdb-auth"                       # Secret name where username and password can be found
  #     insecure: false                                    # Optional value to set if the cluster uses self-signed certificates
  # - name: "second-cluster"
  #   spec:
  #     remoteClusterVmcUrl: "http://second-cluster:123123"
  #     secretRefName: "voltdb-auth"
  #     insecure: false

## Deploy a VoltDB Cluster Prometheus metrics exporter for VoltDB cluster monitoring
## Install kube-prometheus-stack in your cluster to create ServiceMonitor objects
## https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack
##
metrics:
  enabled: false

  image:
    registry: "docker.io"
    repository: voltdb/voltdb-prometheus
    tag: 10.2.3
    pullPolicy: Always

  resources:
    requests:
      cpu: 10m
      memory: 50Mi
    limits:
      cpu: 1
      memory: 512Mi

  # Credentials for connection to VoltDB. Provide user name and password, or
  # pre-created secrets. User/password ignored if credSecretName is present.
  manageSecrets:
    credSecretName:
  user:
  password:

  # TLS/SSL config; required iff SSL is enabled for 'external' connections
  # in the VoltDB deployment
  ssl:
    enabled: false
    truststore:
      password: ""
      file:

  # Port on VoltDB; defaults to admin port 21211; port 21212 is allowed
  # but means metrics cannot be reported when database is paused.
  # port: 21211

  # Stats to monitor; use 'stats' to explicitly list which stats are monitored,
  # or 'skipstats' to list only what is not required. Do not specify both 'stats'
  # and 'skipstats'. If neither is given, then all supported stats are monitored.
  # stats: "IOSTATS,LATENCY,MEMORY"
  # skipstats: "INDEX,INITIATOR"
