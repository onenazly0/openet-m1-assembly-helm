Thank you for installing VoltDB!

Now that you have deployed VoltDB, you should look over
the docs on using VoltDB with Kubernetes available here:

https://docs.voltdb.com/guides/kubernetes.php

Your release is named {{ $.Release.Name }}.

To learn more about the release, run:

  $ helm status {{ $.Release.Name }}
  $ helm get manifest {{ $.Release.Name }}
