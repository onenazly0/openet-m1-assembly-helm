{{- if .Values.global.createNetworkPolicy }}
---
{{/*
Access to DNS from any voltdb pod as selected by release name
*/}}
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-dns-allowed
  namespace: {{ $.Release.Namespace }}
spec:
  podSelector:
    matchLabels:
      release: {{ $.Release.Name }}
  egress:
  - ports:
    - protocol: TCP
      port: 53
    - protocol: UDP
      port: 53
{{- end }}

{{- if and .Values.operator.enabled .Values.global.createNetworkPolicy }}
---
{{/*
General operator pod communication
  - voltdb cluster (egress, selected ports, local namespace, any release)
  - prometheus (ingress, 8383, any namespace)
  - api server (egress 443/6443, to any pod/namespace/etc)
  TODO: RESTRICT API-SERVER
*/}}
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-operator
  namespace: {{ $.Release.Namespace }}
spec:
  podSelector: &podselector_nodes
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-operator
      release: {{ $.Release.Name }}
  ingress:
  - ports:
    - protocol: TCP
      port: 8383 # metrics
    from:
    - podSelector:
        matchLabels:
          app: prometheus
      namespaceSelector: {}
  egress:
  - ports:
    - protocol: TCP
      port: 8080 # web
    - protocol: TCP
      port: 8443 # secure web
    - protocol: TCP
      port: 11780 # status
    - protocol: TCP
      port: 11235 # boot
    to:
    - podSelector:
        matchLabels:
          name: {{ template "voltdb-operator.name" . }}-cluster
  - ports:
    - protocol: TCP
      port: 6443
    - protocol: TCP
      port: 443
{{/* no such pod as kube-apiserver
    to:
    - podSelector:
        matchLabels:
          component: kube-apiserver
    - namespaceSelector: {}
*/}}
{{- end }}

{{- if and .Values.cluster.enabled .Values.global.createNetworkPolicy }}
---
{{/*
Internal communication to voltdb cluster pods from other voltdb
cluster pods only (ingress, egress, all ports, local namespace, same release)
Replication port not included here, see -cluster-dr for that
*/}}
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-cluster-internal
  namespace: {{ $.Release.Namespace }}
spec:
  podSelector: &podselector_nodes
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-cluster
      release: {{ $.Release.Name }}
  ingress:
  - ports: &internal_ports
    - protocol: TCP
      port: 3021 # int
    - protocol: TCP
      port: 7181 # zk
    - protocol: TCP
      port: 8080 # web
    - protocol: TCP
      port: 8443 # secure web
    - protocol: TCP
      port: 9090 # jmx
    - protocol: TCP
      port: 21211 # admin
    - protocol: TCP
      port: 21212 # client
    from:
    - podSelector: *podselector_nodes
  egress:
  - ports: *internal_ports
    to:
    - podSelector: *podselector_nodes
---
{{/*
Access to voltdb cluster pods from operator and other management-related pods
 - operator (ingress, selected ports, local namespace, any release)
 - prometheus agent (ingress, client/admin ports, local namespace, any release)
 - prometheus (ingress, jmx ports, any namespace)
 */}}
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-cluster-control
  namespace: {{ $.Release.Namespace }}
spec:
  podSelector:
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-cluster
      release: {{ $.Release.Name }}
  ingress:
  - ports:
    - protocol: TCP
      port: 8080 # web
    - protocol: TCP
      port: 8443 # secure web
    - protocol: TCP
      port: 11780 # status
    - protocol: TCP
      port: 11235 # boot
    from:
    - podSelector:
        matchLabels:
          name: {{ template "voltdb-operator.name" . }}-operator
  - ports:
    - protocol: TCP
      port: 21211 # admin
    - protocol: TCP
      port: 21212 # client
    from:
    - podSelector:
        matchLabels:
          name: {{ template "voltdb-operator.name" . }}-metrics
  - ports:
    - protocol: TCP
      port: 9090 # jmx
    from:
    - podSelector:
        matchLabels:
          app: prometheus
      namespaceSelector: {}
---
{{/*
Access to voltdb from client software in the kubernetes cluster,
assumed customer-supplied. Ingress control is by IP block only.
*/}}
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-cluster-clients
  namespace: {{ $.Release.Namespace }}
spec:
  podSelector:
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-cluster
      release: {{ $.Release.Name }}
  ingress:
  - ports:
    - protocol: TCP
      port: 8080 # web
    - protocol: TCP
      port: 8443 # secure web
    - protocol: TCP
      port: 21211 # admin
    - protocol: TCP
      port: 21212 # client
    - protocol: TCP
      port: 9092 # kafka
    from: # any private network address
    - ipBlock:
        cidr: 10.0.0.0/8
    - ipBlock:
        cidr: 172.16.0.0/12
    - ipBlock:
        cidr: 192.168.0.0/16
{{- if .Values.cluster.config.deployment.dr.connection.enabled }}
---
{{/*
Replication traffic for XDCR
 - ingress/egress, replication port, any voltdb cluster pod
 - no restrictions on namespace, release, etc.
 TODO: WHAT ABOUT INTERMEDIARIES?
*/}}
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-cluster-dr
  namespace: {{ $.Release.Namespace }}
spec:
  podSelector:
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-cluster
      release: {{ $.Release.Name }}
  ingress:
  - ports:
    - protocol: TCP
      port: 5555 # replication
    from: []
  egress:
  - ports:
    - protocol: TCP
      port: 5555 # replication
    to: []
{{- end }}
{{- if .Values.metrics.enabled }}
---
{{/*
Prometheus agent support
 - prometheus (ingress, 8484, any namespace)
 - voltdb cluster pods (egress, client/admin ports, local namespace, same release)
*/}}
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-cluster-metrics
  namespace: {{ $.Release.Namespace }}
spec:
  podSelector:
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-metrics
      release: {{ $.Release.Name }}
  ingress:
  - ports:
    - protocol: TCP
      port: 8484 # metrics
    from:
    - podSelector:
        matchLabels:
          app: prometheus
      namespaceSelector: {}
  egress:
  - ports:
    - protocol: TCP
      port: 21211 # admin
    - protocol: TCP
      port: 21212 # client
    to:
    - podSelector:
        matchLabels:
          name: {{ template "voltdb-operator.name" . }}-cluster
          release: {{ $.Release.Name }}
{{- end }}
{{- end }}
