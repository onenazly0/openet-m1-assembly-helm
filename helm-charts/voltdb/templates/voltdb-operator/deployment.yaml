{{- if .Values.operator.enabled }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-operator
  namespace: {{ $.Release.Namespace }}
  labels:
    name: {{ template "voltdb-operator.name" . }}-operator
{{ include "voltdb-operator.labels" . | indent 4 }}
spec:
  replicas: {{ .Values.operator.replicas }}
  revisionHistoryLimit: 0
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 0
      maxUnavailable: 1
  selector:
    matchLabels:
      name: {{ template "voltdb-operator.name" . }}-operator
  template:
    metadata:
      labels:
        name: {{ template "voltdb-operator.name" . }}-operator
{{ include "voltdb-operator.labels" . | indent 8 }}
{{- if .Values.operator.podLabels }}
{{ toYaml .Values.operator.podLabels | indent 8 }}
{{- end }}
{{- if .Values.operator.podAnnotations }}
      annotations:
{{ toYaml .Values.operator.podAnnotations | indent 8 }}
{{- end }}
    spec:
      serviceAccountName: {{ template "voltdb-operator.operator.serviceAccountName" . }}
      automountServiceAccountToken: true
{{- if .Values.global.imagePullSecrets }}
      imagePullSecrets:
{{ toYaml .Values.global.imagePullSecrets | indent 6 }}
{{- end }}
      containers:
      - name: {{ template "voltdb-operator.name" . }}-operator
        image: {{ with .Values.operator.image }}{{ .registry }}/{{ .repository }}:{{ .tag }}{{ end }}
        imagePullPolicy: {{ .Values.operator.image.pullPolicy }}
        command:
        - voltdb-operator
        args:
        - "--zap-time-encoding"
        - "iso8601"
        - "--zap-encoder"
        - "{{ .Values.operator.logformat }}"
{{- if .Values.operator.debug.enabled }}
        - "--zap-level"
        - "debug"
{{- end }}
{{- if .Values.operator.resources }}
        resources:
{{ toYaml .Values.operator.resources | indent 10 }}
{{ else }}
        resources:
          requests:
            cpu: 10m
            memory: 50Mi
          limits:
            cpu: 1
            memory: 512Mi
{{- end }}
        livenessProbe:
          httpGet:
            path: /healthz
            port: 9494
            scheme: "HTTP"
          initialDelaySeconds: 10
          periodSeconds: 10
          timeoutSeconds: 1
          failureThreshold: 3
          successThreshold: 1
        readinessProbe:
          httpGet:
            path: /readyz
            port: 9494
            scheme: "HTTP"
          initialDelaySeconds: 10
          periodSeconds: 10
          timeoutSeconds: 1
          failureThreshold: 3
          successThreshold: 1
{{- if .Values.operator.securityContext }}
        securityContext:
{{ toYaml .Values.operator.securityContext | indent 10 }}
{{ else }}
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
{{- end }}
        env:
          - name: WATCH_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          - name: POD_NAME
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: OPERATOR_NAME
            value: "voltdb-operator"
{{- if .Values.operator.debug.enabled }}
          - name: LOG_LEVEL
            value: Debug
{{- end }}
{{- if and .Values.cluster.enabled .Values.cluster.config.deployment.security.enabled }}
{{- if eq .Values.cluster.config.deployment.security.provider "hash" }}
          - name: VOLTDB_API_AUTH_USERNAME
            valueFrom:
              secretKeyRef:
                name: {{ template "voltdb-operator.fullname" . }}-cluster-auth
                key: username
          - name: VOLTDB_API_AUTH_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ template "voltdb-operator.fullname" . }}-cluster-auth
                key: password
{{- end }}
{{- end }}
{{- if .Values.operator.nodeSelector }}
      terminationGracePeriodSeconds: 10
      nodeSelector:
{{ toYaml .Values.operator.nodeSelector | indent 8 }}
    {{- end }}
    {{- if .Values.operator.tolerations }}
      tolerations:
{{ toYaml .Values.operator.tolerations | indent 8 }}
    {{- end }}
    {{- if .Values.operator.affinity }}
      affinity:
{{ toYaml .Values.operator.affinity | indent 8 }}
    {{- end }}
{{- end }}
