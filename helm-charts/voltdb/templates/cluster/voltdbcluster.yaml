{{- if .Values.cluster.enabled }}
apiVersion: voltdb.com/v1
kind: VoltDBCluster
metadata:
  name: {{ template "voltdb-operator.fullname" . }}-cluster
  namespace: {{ $.Release.Namespace }}
  labels:
    name: {{ template "voltdb-operator.name" . }}-cluster
{{ include "voltdb-operator.labels" . | indent 4 }}
spec:
{{- if .Values.cluster.serviceSpec }}
{{- if .Values.cluster.serviceSpec.dr }}
{{- if .Values.cluster.serviceSpec.dr.type }}
{{- if eq .Values.cluster.serviceSpec.dr.type "NodePort" }}
  initContainers:
  - name: init
    image: {{ with .Values.cluster.clusterSpec.image }}{{ .registry }}/{{ .repository }}:{{ .tag }}{{ end }}
    imagePullPolicy: {{ .Values.cluster.clusterSpec.image.pullPolicy }}
    command:
    - sh
    - "-c"
    - |
      /bin/bash <<'EOF'
      TOKEN=`cat /var/run/secrets/kubernetes.io/serviceaccount/token`
      K8S_EXTERNAL_IP=$(curl -sk -H "Authorization: Bearer ${TOKEN}" https://kubernetes.default:443/api/v1/nodes/${NODE_NAME} 2>/dev/null | jq -r '.status.addresses[] | select(.type=="ExternalIP").address // empty')
      if [[ -n "${K8S_EXTERNAL_IP}" ]]; then
        echo "DRPUBLIC_IP=${K8S_EXTERNAL_IP}" > {{ template "voltdb-operator.cluster.mountPath" . }}/envvars
      fi
      RKE_EXTERNAL_IP=$(curl -sk -H "Authorization: Bearer ${TOKEN}" https://kubernetes.default:443/api/v1/nodes/${NODE_NAME} 2>/dev/null | jq -r '.metadata.annotations."rke.cattle.io/external-ip" // empty')
      if [[ -n "${RKE_EXTERNAL_IP}" ]]; then
        echo "DRPUBLIC_IP=${RKE_EXTERNAL_IP}" > {{ template "voltdb-operator.cluster.mountPath" . }}/envvars
      fi
      exit 0
      EOF
    env:
    - name: NODE_NAME
      valueFrom:
        fieldRef:
          fieldPath: spec.nodeName
    volumeMounts:
    - name: voltdb
      mountPath: {{ template "voltdb-operator.cluster.mountPath" . }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}
  container:
    image: {{ with .Values.cluster.clusterSpec.image }}{{ .registry }}/{{ .repository }}:{{ .tag }}{{ end }}
    imagePullPolicy: {{ .Values.cluster.clusterSpec.image.pullPolicy }}
{{- if .Values.cluster.clusterSpec.resources }}
    resources:
{{ toYaml .Values.cluster.clusterSpec.resources | indent 6 }}
{{ else }}
    resources:
      requests:
        memory: "4Gi"
        cpu: "1"
{{- end }}
{{- if .Values.cluster.clusterSpec.livenessProbe.enabled }}
    livenessProbe:
      initialDelaySeconds: {{ .Values.cluster.clusterSpec.livenessProbe.initialDelaySeconds }}
      periodSeconds: {{ .Values.cluster.clusterSpec.livenessProbe.periodSeconds }}
      timeoutSeconds: {{ .Values.cluster.clusterSpec.livenessProbe.timeoutSeconds }}
      failureThreshold: {{ .Values.cluster.clusterSpec.livenessProbe.failureThreshold }}
      successThreshold: {{ .Values.cluster.clusterSpec.livenessProbe.successThreshold }}
{{- end }}
{{- if .Values.cluster.clusterSpec.readinessProbe.enabled }}
    readinessProbe:
      initialDelaySeconds: {{ .Values.cluster.clusterSpec.readinessProbe.initialDelaySeconds }}
      periodSeconds: {{ .Values.cluster.clusterSpec.readinessProbe.periodSeconds }}
      timeoutSeconds: {{ .Values.cluster.clusterSpec.readinessProbe.timeoutSeconds }}
      failureThreshold: {{ .Values.cluster.clusterSpec.readinessProbe.failureThreshold }}
      successThreshold: {{ .Values.cluster.clusterSpec.readinessProbe.successThreshold }}
{{- end }}
{{- if .Values.cluster.clusterSpec.startupProbe.enabled }}
    startupProbe:
      initialDelaySeconds: {{ .Values.cluster.clusterSpec.startupProbe.initialDelaySeconds }}
      periodSeconds: {{ .Values.cluster.clusterSpec.startupProbe.periodSeconds }}
      timeoutSeconds: {{ .Values.cluster.clusterSpec.startupProbe.timeoutSeconds }}
      failureThreshold: {{ .Values.cluster.clusterSpec.startupProbe.failureThreshold }}
      successThreshold: {{ .Values.cluster.clusterSpec.startupProbe.successThreshold }}
{{- end }}
{{- if .Values.cluster.clusterSpec.securityContext }}
    securityContext:
{{ toYaml .Values.cluster.clusterSpec.securityContext | indent 6 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.additionalStartArgs }}
    additionalArgs:
{{ toYaml .Values.cluster.clusterSpec.additionalStartArgs | indent 6 }}
{{- end }}
  replicas: {{ .Values.cluster.clusterSpec.replicas }}
{{- if kindIs "invalid" .Values.cluster.clusterSpec.maxPodUnavailable }}
  maxPodUnavailable: {{ .Values.cluster.config.deployment.cluster.kfactor }}
{{ else }}
  {{- if lt .Values.cluster.clusterSpec.maxPodUnavailable 0.0 }}
  maxPodUnavailable: 0
  {{ else }}
  maxPodUnavailable: {{ .Values.cluster.clusterSpec.maxPodUnavailable }}
  {{- end }}
{{- end }}
  persistentVolume:
    size: {{ .Values.cluster.clusterSpec.persistentVolume.size }}
{{- if .Values.cluster.clusterSpec.persistentVolume.storageClassName }}
    storageClassName: {{ .Values.cluster.clusterSpec.persistentVolume.storageClassName }}
{{- end }}
{{- if .Values.cluster.clusterSpec.persistentVolume.hostPath.enabled }}
    hostPath:
      enabled: true
{{- if .Values.cluster.clusterSpec.persistentVolume.hostPath.path }}
      path: {{ .Values.cluster.clusterSpec.persistentVolume.hostPath.path }}
{{- end }}
{{- end }}
{{- if .Values.cluster.clusterSpec.maintenanceMode }}
  maintenanceMode: {{ .Values.cluster.clusterSpec.maintenanceMode }}
{{- end }}
{{- if .Values.cluster.clusterSpec.takeSnapshotOnShutdown }}
  takeSnapshotOnShutdown: {{ .Values.cluster.clusterSpec.takeSnapshotOnShutdown }}
{{- end }}
{{- if .Values.cluster.clusterSpec.initForce }}
  initForce: {{ .Values.cluster.clusterSpec.initForce }}
{{- end }}
{{- if .Values.cluster.config.deployment.security.enabled }}
  auth:
    secretRefName: {{ template "voltdb-operator.fullname" . }}-cluster-auth
{{- end }}
{{- if .Values.cluster.clusterSpec.deletePVC }}
  deletePVC: {{ .Values.cluster.clusterSpec.deletePVC }}
{{- end }}
{{- if .Values.cluster.clusterSpec.allowRestartDuringUpdate }}
  allowRestartDuringUpdate: {{ .Values.cluster.clusterSpec.allowRestartDuringUpdate }}
{{- end }}
{{- if .Values.cluster.clusterSpec.disableFinalizers }}
  disableFinalizers: {{ .Values.cluster.clusterSpec.disableFinalizers }}
{{- end }}
{{- if .Values.cluster.clusterSpec.livenessProbe.enabled }}
  disableLivenessChecks: false
{{ else }}
  disableLivenessChecks: true
{{- end }}
{{- if .Values.cluster.clusterSpec.readinessProbe.enabled }}
  disableReadinessChecks: false
{{ else }}
  disableReadinessChecks: true
{{- end }}
{{- if .Values.cluster.clusterSpec.startupProbe.enabled }}
  disableStartupChecks: false
{{ else }}
  disableStartupChecks: true
{{- end }}
{{- if .Values.cluster.clusterSpec.stoppedNodes }}
  stoppedNodes:
{{ toYaml .Values.cluster.clusterSpec.stoppedNodes | indent 4 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.storageConfigs }}
  storageConfigs:
{{ toYaml .Values.cluster.clusterSpec.storageConfigs | indent 4 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.additionalVolumes }}
  additionalVolumes:
{{ toYaml .Values.cluster.clusterSpec.additionalVolumes | indent 4 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.additionalVolumeMounts }}
  additionalVolumeMounts:
{{ toYaml .Values.cluster.clusterSpec.additionalVolumeMounts | indent 4 }}
{{- end }}
  env:
    - name: DRPUBLIC_IP
      valueFrom:
        fieldRef:
          fieldPath: status.hostIP
    - name: POD_NAMESPACE
      valueFrom:
        fieldRef:
          fieldPath: metadata.namespace
    - name: VOLTDB_K8S_ADAPTER_PVVOLTDBROOT
      value: {{ template "voltdb-operator.cluster.mountPath" . }}
{{- if .Values.cluster.clusterSpec.env.VOLTDB_OPTS }}
    - name: VOLTDB_OPTS
      value: {{ .Values.cluster.clusterSpec.env.VOLTDB_OPTS | quote }}
{{- end }}
{{- if .Values.cluster.clusterSpec.env.VOLTDB_HEAPMAX }}
    - name: VOLTDB_HEAPMAX
      value: {{ .Values.cluster.clusterSpec.env.VOLTDB_HEAPMAX | quote }}
{{- end }}
{{- if .Values.cluster.clusterSpec.env.VOLTDB_HEAPCOMMIT }}
    - name: VOLTDB_HEAPCOMMIT
      value: {{ .Values.cluster.clusterSpec.env.VOLTDB_HEAPCOMMIT | quote }}
{{- end }}
{{- if .Values.cluster.clusterSpec.env.VOLTDB_K8S_LOG_CONFIG }}
    - name: VOLTDB_K8S_LOG_CONFIG
      value: {{ .Values.cluster.clusterSpec.env.VOLTDB_K8S_LOG_CONFIG | quote }}
{{ else }}
    - name: VOLTDB_K8S_LOG_CONFIG
      value: '/etc/voltdb/log4j.xml'
{{- end }}
{{- if and .Values.cluster.config.deployment.dr.role .Values.cluster.config.deployment.dr.connection.enabled }}
    - name: VOLTDB_DR_ROLE
      value: {{ .Values.cluster.config.deployment.dr.role }}
{{- end }}
{{- if .Values.cluster.clusterSpec.additionalXDCRReadiness }}
    - name: VOLTDB_K8S_READY_CHECK_WITH_XDCR
      value: 'true'
{{- end }}
{{- if .Values.cluster.clusterSpec.customEnv }}
{{- range $key, $value := .Values.cluster.clusterSpec.customEnv }}
    - name: {{ default "none" $key | quote }}
      value: {{ default "none" $value | quote }}
{{- end }}
{{- end }}
{{- if .Values.cluster.config.deployment.ssl.enabled }}
  ssl:
    enabled: true
    insecure: {{ .Values.cluster.clusterSpec.ssl.insecure }}
    secretRefName: {{ template "voltdb-operator.fullname" . }}-cluster-ssl
{{- end }}
  pod:
    serviceAccountName: {{ template "voltdb-operator.cluster.serviceAccountName" . }}
    automountServiceAccountToken: true
{{- if .Values.cluster.clusterSpec.podTerminationGracePeriodSeconds }}
    terminationGracePeriodSeconds: {{ .Values.cluster.clusterSpec.podTerminationGracePeriodSeconds }}
{{- end }}
{{- if .Values.cluster.clusterSpec.priorityClassName }}
    priorityClassName: {{ .Values.cluster.clusterSpec.priorityClassName }}
{{- end }}
{{- if .Values.cluster.clusterSpec.additionalLabels }}
    additionalLabels:
{{ toYaml .Values.cluster.clusterSpec.additionalLabels | indent 6 }}
{{- else }}
    additionalLabels: {} # Hack to ensure pod is object {} if all optional values are empty
{{- end }}
{{- if .Values.cluster.clusterSpec.affinity }}
    affinity:
{{ toYaml .Values.cluster.clusterSpec.affinity | indent 6 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.additionalAnnotations }}
    annotations:
{{ toYaml .Values.cluster.clusterSpec.additionalAnnotations | indent 6 }}
{{- end }}
{{- if .Values.global.imagePullSecrets }}
    imagePullSecrets:
{{ toYaml .Values.global.imagePullSecrets | indent 4 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.nodeSelector }}
    nodeSelector:
{{ toYaml .Values.cluster.clusterSpec.nodeSelector | indent 6 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.tolerations }}
    tolerations:
{{ toYaml .Values.cluster.clusterSpec.tolerations | indent 6 }}
{{- end }}
{{- if .Values.cluster.clusterSpec.podSecurityContext }}
    securityContext:
{{ toYaml .Values.cluster.clusterSpec.podSecurityContext | indent 6 }}
{{- end }}
{{- if or .Values.cluster.clusterSpec.clusterInit.initSecretRefName .Values.cluster.clusterSpec.clusterInit.schemaConfigMapRefName .Values.cluster.clusterSpec.clusterInit.classesConfigMapRefName }}
  clusterInit:
{{- if .Values.cluster.clusterSpec.clusterInit.initSecretRefName }}
    initSecretRefName: {{ .Values.cluster.clusterSpec.clusterInit.initSecretRefName }}
{{- else }}
    initSecretRefName:
{{- end }}
{{- if .Values.cluster.clusterSpec.clusterInit.schemaConfigMapRefName }}
    schemaConfigMapRefName: {{ .Values.cluster.clusterSpec.clusterInit.schemaConfigMapRefName }}
{{- else }}
    schemaConfigMapRefName:
{{- end }}
{{- if .Values.cluster.clusterSpec.clusterInit.classesConfigMapRefName }}
    classesConfigMapRefName: {{ .Values.cluster.clusterSpec.clusterInit.classesConfigMapRefName }}
{{- else }}
    classesConfigMapRefName:
{{- end }}
{{- end }}
{{- if .Values.cluster.serviceSpec }}
  service:
{{- if .Values.cluster.serviceSpec.type }}
    type: {{ .Values.cluster.serviceSpec.type }}
{{- end }}
{{- if .Values.cluster.serviceSpec.externalTrafficPolicy }}
    externalTrafficPolicy: {{ .Values.cluster.serviceSpec.externalTrafficPolicy }}
{{- end }}
{{- if .Values.cluster.serviceSpec.vmcPort }}
    vmcPort: {{ .Values.cluster.serviceSpec.vmcPort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.vmcNodePort }}
    vmcNodePort: {{ .Values.cluster.serviceSpec.vmcNodePort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.vmcSecurePort }}
    vmcSecurePort: {{ .Values.cluster.serviceSpec.vmcSecurePort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.vmcSecureNodePort }}
    vmcSecureNodePort: {{ .Values.cluster.serviceSpec.vmcSecureNodePort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.adminPortEnabled }}
    adminPortEnabled: {{ .Values.cluster.serviceSpec.adminPortEnabled }}
{{- if .Values.cluster.serviceSpec.adminPort }}
    adminPort: {{ .Values.cluster.serviceSpec.adminPort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.adminNodePort }}
    adminNodePort: {{ .Values.cluster.serviceSpec.adminNodePort }}
{{- end }}
{{- end }} # End adminPortEnabled
{{- if .Values.cluster.serviceSpec.clientPortEnabled }}
    clientPortEnabled: {{ .Values.cluster.serviceSpec.clientPortEnabled }}
{{- if .Values.cluster.serviceSpec.clientPort }}
    clientPort: {{ .Values.cluster.serviceSpec.clientPort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.clientNodePort }}
    clientNodePort: {{ .Values.cluster.serviceSpec.clientNodePort }}
{{- end }}
{{- end }} # End clientPortEnabled
{{- if .Values.cluster.serviceSpec.loadBalancerIP }}
    loadBalancerIP: {{ .Values.cluster.serviceSpec.loadBalancerIP }}
{{- end }}
{{- if .Values.cluster.serviceSpec.loadBalancerSourceRanges }}
    loadBalancerSourceRanges:
{{ toYaml .Values.cluster.serviceSpec.loadBalancerSourceRanges | indent 6 }}
{{- end }}
{{- if .Values.cluster.serviceSpec.externalIPs }}
    externalIPs:
{{ toYaml .Values.cluster.serviceSpec.externalIPs | indent 6 }}
{{- end }}
    {{- if .Values.cluster.serviceSpec.http }}
    http:
      {{- if .Values.cluster.serviceSpec.http.sessionAffinity }}
      sessionAffinity: {{ .Values.cluster.serviceSpec.http.sessionAffinity }}
      {{- end }}
      {{- if .Values.cluster.serviceSpec.http.sessionAffinityConfig }}
      sessionAffinityConfig:
{{ toYaml .Values.cluster.serviceSpec.http.sessionAffinityConfig | indent 8 }}
      {{- end }}
    {{- end }}
{{- if .Values.cluster.serviceSpec.dr }}
{{- if .Values.cluster.serviceSpec.dr.type }}
    dr:
      type: {{ .Values.cluster.serviceSpec.dr.type }}
{{- if .Values.cluster.serviceSpec.dr.servicePerPod }}
      servicePerPod: {{ .Values.cluster.serviceSpec.dr.servicePerPod }}
{{- end }}
{{- if .Values.cluster.serviceSpec.dr.annotations }}
      annotations:    
{{ toYaml .Values.cluster.serviceSpec.dr.annotations | indent 8 }}
{{- end }}
{{- if .Values.cluster.serviceSpec.dr.availableIPs }}
      availableIPs:
{{ toYaml .Values.cluster.serviceSpec.dr.availableIPs | indent 8 }}
{{- end }}
{{- if .Values.cluster.serviceSpec.dr.externalTrafficPolicy }}
      externalTrafficPolicy: {{ .Values.cluster.serviceSpec.dr.externalTrafficPolicy }}
{{- end }}
{{- if .Values.cluster.serviceSpec.dr.replicationPort }}
      replicationPort: {{ .Values.cluster.serviceSpec.dr.replicationPort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.dr.replicationNodePort }}
      replicationNodePort: {{ .Values.cluster.serviceSpec.dr.replicationNodePort }}
{{- end }}
{{- if .Values.cluster.serviceSpec.dr.publicIPFromService }}
      publicIPFromService: {{ .Values.cluster.serviceSpec.dr.publicIPFromService }}
{{- end }}
{{- if .Values.cluster.serviceSpec.dr.override }}
      override:
{{ toYaml .Values.cluster.serviceSpec.dr.override | indent 8 }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}
{{- end }}
